FROM tomcat:8.0.20-jre8

#RUN mkdir /usr/local/tomcat/webapps/myapp
COPY ./target/micro-service-maintenance.war /usr/local/tomcat/webapps/micro-service-maintenance.war
#ADD ./target/db-service.war /usr/local/tomcat/webapps/myapp/db-service.war



# Setting up environment variable
ENV cdns_log_server="10.10.80.38:5000"
ENV cdns_redis_ip="10.10.80.43"
ENV cdns_redis_port="6379"
ENV cdns_auth_enabled="false"
ENV cdns_datasource_password="123456"
ENV cdns_datasource_url="jdbc:oracle:thin:@10.10.30.16:1521:orcl"
ENV cdns_datasource_username="cdnstest_3"
ENV cdns_eureka_ip="localhost"
ENV cdns_eureka_port="8761"
ENV cdns_pusher_host="10.10.80.43:4567"
ENV cdns_pusher_key="h866mohlb9aue8vsajwg"
ENV cdns_pusher_secret="Lt774B7FuRvUdjzVvupeFEksz3Z3QDPU"

#Command to run docker file

#docker build --rm=true --tag=jboss/wildfly .
#docker run -p 8080:8080 -p 9990:9990 -it jboss/wildfly /opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0