package com.paysys.micro.services;

import com.paysys.micro.services.maintenance.bl.MaintenanceFactory;
import com.paysys.micro.services.maintenance.bl.CustomerMaintenanceBL;
import com.paysys.micro.services.maintenance.bl.MaintenanceOperationsBL;
import com.paysys.micro.services.maintenance.bl.ProductMaintenanceBL;
import com.paysys.service.common.enm.ServiceNames;
import com.paysys.service.interceptor.RequestInterceptor;
import com.paysys.service.utility.APICaller;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = {"com.paysys.service", "com.paysys.micro.services"})
public class MicroServiceMaintenanceApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MicroServiceMaintenanceApplication.class, args);
    }
}

@Configuration
class Config {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .setConnectTimeout(15000)
                .additionalInterceptors(this.requestInterceptor())
                .setReadTimeout(60000).build();
    }

    @Bean
    RequestInterceptor requestInterceptor() {
        RequestInterceptor interceptor = new RequestInterceptor();
        return interceptor;
    }


    @Bean(name = "apiCaller")
    @Scope("prototype")
    public APICaller apiCaller(ServiceNames serviceName) {
        return new APICaller(serviceName);
    }


    @Bean(name = "maintenanceFactory")
    @Scope("prototype")
    public MaintenanceFactory maintenanceFactory(String moduleId) {
        return new MaintenanceFactory(moduleId);
    }

    @Bean(name = "customerMaintenanceBL")
    @Scope("prototype")
    public CustomerMaintenanceBL customerMaintenanceBL(String moduleId, String apiKey) {
        return new CustomerMaintenanceBL(moduleId, apiKey);
    }

    @Bean(name = "productMaintenanceBL")
    @Scope("prototype")
    public ProductMaintenanceBL productMaintenanceBL(String moduleId, String apiKey) {
        return new ProductMaintenanceBL(moduleId, apiKey);
    }

    @Bean(name = "maintenanceOperationsBL")
    @Scope("prototype")
    public MaintenanceOperationsBL maintenanceOperationsBL(String moduleId, String apiKey) {
        return new MaintenanceOperationsBL(moduleId, apiKey);
    }

    @Bean
    @Scope("prototype")
    public String moduleId(String moduleId) {
        return moduleId;
    }
}
