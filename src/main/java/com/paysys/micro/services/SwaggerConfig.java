package com.paysys.micro.services;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;

//import com.paysys.services.common.BaseController;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				// .apis(RequestHandlerSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.paysys.micro.services"))
				// .paths(PathSelectors.ant("/rest/entity/*"))
				//.paths(regex(BaseController.BASE_PATH_TRAN + ".*"))
				// .paths(PathSelectors.any())
				.build();

	}

	// @Bean
	// public Docket api() {
	// return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
	// .apis(RequestHandlerSelectors.basePackage("com.paysys.micro.services"))
	// .paths(regex(BaseController.BASE_PATH_USER + ".*")).build();
	//
	// }

	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo("Business Process API", "This API is used to Manage Business Process Modal",
				"1.0", "Terms of services URL", "Paysys Labs", "License of API", "API license URL");
		return apiInfo;
	}
}
