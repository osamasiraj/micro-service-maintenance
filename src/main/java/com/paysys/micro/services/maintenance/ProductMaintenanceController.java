package com.paysys.micro.services.maintenance;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.paysys.entity.common.RegistrationRequestDTO;
import com.paysys.micro.services.maintenance.bl.APIInterface;
import com.paysys.micro.services.maintenance.bl.MaintenanceFactory;
import com.paysys.requests.customer.CustomerAccountRequestDTO;
import com.paysys.service.common.APIKeyConstants;
import com.paysys.service.common.BaseController;
import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import com.paysys.service.response.ServiceResponse;
import com.paysys.service.utility.APICallerDetail;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Product Maintenance Operations")
@Validated
@RestController
@RequestMapping(BaseController.BASE_PATH_MAINTENANCE + ModuleKeyConstants.MODULE_KEY_PRODUCT)
public class ProductMaintenanceController {

    private final static Logger logger = Logger.getLogger(ProductMaintenanceController.class);

    @Autowired
    private ApplicationContext context;


    @ApiOperation(value = "Get Product By Identifier")
    @PostMapping(value = "/getProductByIdentifier")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getProductByIdentifier(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getProductByIdentifier");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_BY_IDENTIFIER);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get WSB By Identifier")
    @PostMapping(value = "/getInventoryByIdentifier")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getInventoryByIdentifier(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getInventoryByIdentifier");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_INVENTORY_BY_IDENTIFIER);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get WSB Details")
    @PostMapping(value = "/getInventoryDetails")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getInventoryDetails(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getInventoryDetails");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_INVENTORY_DETAILS);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }


    @ApiOperation(value = "Get Product Nominees")
    @PostMapping(value = "/getProductNominees")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getProductNominees(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getProductNominees");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_PRODUCT_NOMINEES);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Customer SSCard")
    @PostMapping(value = "/getProductSigCard")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getProductSigCard(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getProductSigCard");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_SIG_CARD_BY_IDENTIFIER);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Product Details")
    @PostMapping(value = "/getProductDetails")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getProductDetails(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getProductDetails");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_PRODUCT_DETAILS);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Product KYC Set")
    @PostMapping(value = "/getProductKycSet")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getProductKycSet(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getProductKycSet");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_PRODUCT_KYC_SET);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get All Customer Accounts By Product Code")
    @PostMapping(value = "/getCustAccByProdCode")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getCustAccByProdCode(@RequestBody CustomerAccountRequestDTO accountRequestDTO) {

        logger.info("Function Called: getCustAccByProdCode");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_BY_CUST_PROD_CODE);
            return serviceResponse.getBykey(accountRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Allowed Status Change List")
    @GetMapping(value = "getAllowedStatusFiller/{status}/{type}")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getStatusChangeList(@PathVariable("status") String status,
                                               @PathVariable("type") String type) {

        logger.info("Function Called: getAllowedStatusFiller");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_ALLOWED_STATUS_FILLER);
            return serviceResponse.getBykey(new APICallerDetail().add(status).add(type));

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Certificate Inventory For Re-Issuance")
    @PostMapping(value = "/getCertForReIssuance")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getCertForReIssuance(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getCertForReIssuance");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_CERT_FOR_REISSUANCE);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Certificate Inventory For Re-Issuance")
    @PostMapping(value = "/getProductAttachments")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getProductAttachments(
            @ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getCertForReIssuance");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE))
                    .processTransaction(APIKeyConstants.API_KEY_GET_PRODUCT_ATTACHMENTS);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

}
