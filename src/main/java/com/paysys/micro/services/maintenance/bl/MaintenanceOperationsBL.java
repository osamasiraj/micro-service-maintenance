package com.paysys.micro.services.maintenance.bl;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;

import com.paysys.entity.common.Filler;
import com.paysys.entity.common.RegistrationRequestDTO;
import com.paysys.entity.common.RegistrationRequestDetailDTO;
import com.paysys.micro.services.maintenance.bl.common.GeneralBL;
import com.paysys.requests.customer.CustomerAccountUnitIssuanceRequestDTO;
import com.paysys.requests.customer.CustomerAccountUnitRequestDTO;
import com.paysys.requests.customer.KYCFillerRequestDTO;
import com.paysys.requests.customer.ProductProfitDetailRequestDTO;
import com.paysys.requests.product.ProductIdentTypeRequestDTO;
import com.paysys.responses.customer.CustomerAccountUnitResponseDTO;
import com.paysys.responses.entity.BranchResponseDTO;
import com.paysys.responses.product.AccountIssuanceResponseDTO;
import com.paysys.responses.product.AccountResponseDTO;
import com.paysys.responses.product.NomineePayDetailsResponseDTO;
import com.paysys.responses.product.ReinvestmentDetailsResponseDTO;
import com.paysys.service.common.APIKeyConstants;
import com.paysys.service.common.Constants;
import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.common.enm.BPMProcess;
import com.paysys.service.common.enm.BPMTransactionKey;
import com.paysys.service.common.enm.BPMWorkFlowStatus;
import com.paysys.service.common.enm.CustomerIdentityKey;
import com.paysys.service.common.enm.KYCEnum;
import com.paysys.service.common.enm.RequestType;
import com.paysys.service.common.enm.ServiceNames;
import com.paysys.service.exception.PaySysException;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import com.paysys.service.response.ServiceResponse;
import com.paysys.service.utility.APICaller;
import com.paysys.service.utility.APICallerDetail;
import com.paysys.service.utility.HelperFunctions;
import com.paysys.service.utility.MappingUtility;

public class MaintenanceOperationsBL extends GeneralBL implements APIInterface {

	private final String apiKey;
	private final String moduleId;
	private final static Logger logger = Logger.getLogger(MaintenanceOperationsBL.class);

	public MaintenanceOperationsBL(String moduleId, String apiKey) {
		this.moduleId = moduleId;
		this.apiKey = apiKey;
	}

	@Override
	public ServiceResponse getAll() throws PaySysException, Exception {
		ServiceResponse response;
		switch (apiKey) {
		case APIKeyConstants.API_KEY_GET_IDENTIFIER_TYPE_FILLER:
			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_IDENTIFIER_TYPE,
					APIKeyConstants.API_KEY_GET_FILLER, new APICallerDetail());

			return response;
		default:
			return new ServiceResponse(ResponseCode.ERR_INVALID_API_KEY);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponse getBykey(Object key) throws PaySysException, Exception {
		ServiceResponse response = null;

		switch (apiKey) {

		case APIKeyConstants.API_VALIDATE_TRANSFER_IN_REQUEST:
			boolean isValid = this.validateTransferInLimits(((RegistrationRequestDTO) key));

			if (isValid)
				response = new ServiceResponse(ResponseCode.SUCCESS);
			else
				response = new ServiceResponse(ResponseCode.FAILURE, ResponseDesc.ERR_DESC_DATA_VALIDATION_FAILED);

			return response;

		case APIKeyConstants.API_KEY_GET_IDENTIFIER_FILLER:

			ProductIdentTypeRequestDTO productIdentTypeRequestDTO = ((ProductIdentTypeRequestDTO) key);
			List<Filler> validators = null;

			if (productIdentTypeRequestDTO.getHideIdentifiers() != 1) {
				KYCFillerRequestDTO kycFiller = new KYCFillerRequestDTO();
				kycFiller.setKycCode(KYCEnum.KYC_CUST_IDENT_CODE.toString());

				response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_KYC_ATTR_LIST,
						APIKeyConstants.API_KEY_FILLER_BY_KYC_CODE, new APICallerDetail().setObj(kycFiller));

				Type listType = new TypeToken<List<Filler>>() {
				}.getType();

				validators = (List<Filler>) new MappingUtility<>().JsonToDto(response.getData(), listType);
			} else {

				validators = new ArrayList<>();

			}

			if (productIdentTypeRequestDTO.getIsAccount() == 1) {

				Filler accountFiller = new Filler();
				accountFiller.setLabel("Account Number");
				accountFiller.setValue(CustomerIdentityKey.ACCOUNT_NUMBER.toString());
				validators.add(accountFiller);

			}

			if (productIdentTypeRequestDTO.getIsCertificate() == 1) {
				Filler registrationFiller = new Filler();
				registrationFiller.setLabel("Registration Code");
				registrationFiller.setValue(CustomerIdentityKey.REGISTRATION_CODE.toString());
				validators.add(registrationFiller);
			}

			response = new ServiceResponse(ResponseCode.SUCCESS, validators);

			return response;

		case APIKeyConstants.API_KEY_GET_REGEX_BY_IDEN_NAME:

			KYCFillerRequestDTO kycFillerRequest = (KYCFillerRequestDTO) key;

			if (kycFillerRequest.getKycName().equalsIgnoreCase(CustomerIdentityKey.ACCOUNT_NUMBER.toString())
					|| kycFillerRequest.getKycName()
							.equalsIgnoreCase(CustomerIdentityKey.REGISTRATION_CODE.toString())) {

				Filler validator = new Filler();
				validator.setLabel(kycFillerRequest.getKycName());
				if (kycFillerRequest.getKycName().equalsIgnoreCase("Account Number") || kycFillerRequest.getKycName()
						.equalsIgnoreCase(CustomerIdentityKey.ACCOUNT_NUMBER.toString()))
					validator.setValue("^(\\d){15}$");
				else
					validator.setValue("^(\\d){15}$");

				response = new ServiceResponse(ResponseCode.SUCCESS, Collections.singletonList(validator));

			} else {

				response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_VALIDATOR, apiKey,
						new APICallerDetail().setObj(key));
			}
			return response;

		case APIKeyConstants.API_KEY_GET_PROD_TRANSFER_DETAILS:
			RegistrationRequestDTO request = ((RegistrationRequestDTO) key);
			String requestType = this.getInnerRequestType(request);

			if (requestType.equalsIgnoreCase(BPMProcess.P2PTransfer.toString()))

				request = this.getP2PTransferDetails(request);

			else if (requestType.equalsIgnoreCase(BPMProcess.C2CTransferOut.toString()))

				request = this.getC2CTransferOutDetails(request);

			else if (requestType.equalsIgnoreCase(BPMProcess.C2CTransferIn.toString()))

				request = this.getC2CTransferInDetails(request);

			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			return response;

		case APIKeyConstants.API_KEY_GET_PROD_REINVESTMENT_DETAILS:
			request = ((RegistrationRequestDTO) key);

			request = this.getReinvestmentDetails(request);
			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			return response;

		case APIKeyConstants.API_KEY_GET_LAPSED_REVIVAL_DETAILS:
			request = ((RegistrationRequestDTO) key);

			request = this.getLapsedRevivalDetails(request);
			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			return response;

		case APIKeyConstants.API_KEY_GET_LAPSED_DETAILS:
			request = ((RegistrationRequestDTO) key);

			request = this.getLapsedDetails(request);
			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			return response;

		case APIKeyConstants.API_KEY_RUN_LAPSED_MARKING_JOB:

			APICaller jobCaller = (APICaller) context.getBean(APICaller.class, ServiceNames.TRANSACTION_ENGINE);
			response = jobCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_TRAN_ENGINE,
					APIKeyConstants.API_KEY_JOB,
					new APICallerDetail().setObj(key).add(APIKeyConstants.API_KEY_RUN_LAPSED_MARKING_JOB));

			return response;

		case APIKeyConstants.API_KEY_GET_NOMINEE_PAYMENT_DETAILS:
			request = ((RegistrationRequestDTO) key);

			request = this.getNomineePaymentDetails(request);
			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			return response;

		case APIKeyConstants.API_KEY_GET_MATURITY_AND_PROFIT_COUNT:
			ProductProfitDetailRequestDTO productProfitDetailRequestDTO = ((ProductProfitDetailRequestDTO) key);

			response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_PRODUCT,
					APIKeyConstants.API_KEY_GET_MATURITY_AND_PROFIT_COUNT,
					new APICallerDetail().setObj(productProfitDetailRequestDTO));

			return response;

		// Get account for correlation
		case APIKeyConstants.API_KEY_GET_BY_CUST_PROD_CODE:
			HashMap<String, String> data = ((HashMap<String, String>) key);
			String productCode = (String) data.get("productCode");
			String userTypeKey = (String) data.get("userTypeKey");

			response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT, apiKey,
					new APICallerDetail().setObj(key));

			if (response.getCode() != ResponseCode.SUCCESS) {
				AccountResponseDTO newAccount = this.generateAccountNumber(userTypeKey, productCode,
						BPMTransactionKey.ACCOUNT_OPENING_REQUEST);

				response = new ServiceResponse(ResponseCode.SUCCESS, Collections
						.singletonList(new Filler(newAccount.getAccountNumber(), newAccount.getAccountNumber())));
			}

			return response;

		default:
			return new ServiceResponse(ResponseCode.ERR_INVALID_API_KEY);

		}
	}

	@Override
	public ServiceResponse add(Object genObj) throws PaySysException, Exception {
		RegistrationRequestDTO request = (RegistrationRequestDTO) genObj;
		ServiceResponse response = null;
		RegistrationRequestDTO data = null;
		if (genObj instanceof RegistrationRequestDTO) {
			data = (RegistrationRequestDTO) genObj;
			if(data.getPaymentMode() != null && data.getPaymentMode().equals("0005"))
				this.validateInLueGovtChequeNumber(data);
			else if(data.getAdditionalInfo().containsKey("paymentMode") && data.getAdditionalInfo().get("paymentMode").equals("0005"))
				this.validateInLueGovtChequeNumber(data);
			else if (data.getAdditionalInfo().containsKey("inlue") || data.getAdditionalInfo().containsKey("oldChequeNumber"))
				throw new PaySysException(ResponseCode.ERR_CODE_CHEQUE_INLUE_NOT_ALLOWED,ResponseDesc.ERR_CODE_CHEQUE_INLUE_NOT_ALLOWED);
		}
		if (this.validateData(request)) {
			this.apiCaller = (APICaller) this.context.getBean(APICaller.class, ServiceNames.BPM_SERVICE);
			response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_BPM_COMMON_CONTROLLER,
					APIKeyConstants.API_KEY_BPM_CREATE, new APICallerDetail().setObj(this.updateUserTypeKey(request)));
		} else
			response = new ServiceResponse(ResponseCode.FAILURE, ResponseDesc.ERR_DESC_DATA_VALIDATION_FAILED);

		return response;
	}

	@Override
	public ServiceResponse update(Object genObj) throws PaySysException, Exception {
		RegistrationRequestDTO request = (RegistrationRequestDTO) genObj;
		ServiceResponse response = null;

		if (this.validateData(request)) {
			this.apiCaller = (APICaller) this.context.getBean(APICaller.class, ServiceNames.BPM_SERVICE);
			response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_BPM_COMMON_CONTROLLER,
					APIKeyConstants.API_KEY_UPDATE, new APICallerDetail().setObj(this.updateUserTypeKey(request)));
		} else
			response = new ServiceResponse(ResponseCode.FAILURE, ResponseDesc.ERR_DESC_DATA_VALIDATION_FAILED);

		return response;
	}

	@SuppressWarnings("unchecked")
	private boolean validateData(RegistrationRequestDTO request) throws Exception {
		boolean isValid = false;
		String innerRequestType = this.getInnerRequestType(request);

		switch (BPMProcess.valueOf(innerRequestType)) {
		case customerDemographics:
			isValid = this.validateCustomerDemographics(request);
			break;
		case changeSignature:
			isValid = this.validateCustomerAttachment(request);
			break;
		case changeNominee:
			isValid = this.validateNomineeShare(request);
			break;
		case minorToMajor:
			isValid = this.validateCustomerDocuments(request);
			break;
		case certificateReIssue:
			if (StringUtils.isEmpty(request.getWorkFlowStatus()) || request.getWorkFlowStatus()
					.equalsIgnoreCase(BPMWorkFlowStatus.DEALING_OFFICER_APPROVE.toString())) {
				isValid = this.validateReIssueAmount(request);
				isValid = this.validateCertificateInventory(request);
			} else {
				isValid = true;
			}
			break;
		case accountConversion:
			this.validateWSBInventory(request);
			isValid = true;
			break;
		case certificateConversion:
			isValid = this.validateCustomerDocuments(request);
			break;
		case zakatExemption:
			isValid = this.validateCustomerAttachment(request);
			break;
		case updateAccount:
			isValid = this.validateAccountStatusUpdate(request);
			break;
		case WSBReIssue:
			this.validateWSBInventory(request);
			isValid = true;
			break;
		case P2PTransfer:
			// isValid = this.validateWSBInventory(request);
			isValid = true;
			break;
		case C2CTransferIn:
			isValid = this.validateTransferInLimits(request);
			isValid = this.validateC2CTransferIn(request);
			break;
		case C2CTransferOut:
			isValid = this.validateC2CTransferOut(request);
			break;
		case updateCertificate:
			isValid = this.validateCertificateStatusUpdate(request);
			break;
		case accountCorelation:
			isValid = true;
			break;
		case changeWSBStatus:
			isValid = true;
			break;
		case reInvestment:
		case lapsedReInvestment:

			if (request.getWorkFlowStatus() != null
					&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_APPROVE.toString())) {

				List<ReinvestmentDetailsResponseDTO> reinvestmentDetails = (List<ReinvestmentDetailsResponseDTO>) this
						.parse(request, BPMTransactionKey.REINVESTMENT_DETAILS);

				BigDecimal nonInvestAmount = BigDecimal.ZERO;

				for (ReinvestmentDetailsResponseDTO reinvestmentDetail : reinvestmentDetails) {
					nonInvestAmount = nonInvestAmount.add(new BigDecimal(reinvestmentDetail.getNonInvestableAmount()));
				}

				if (!nonInvestAmount.equals(BigDecimal.ZERO) && request.getAdditionalInfo().get("chequeNum") == null) {
					request.setWorkFlowStatus(BPMWorkFlowStatus.INCHARGE_APPROVED.toString());
				}
			}

			isValid = true;
			break;
		case lapsedEncashment:
			isValid = this.validateLapsedEncashment(request);
			break;
		case lapsedInquiry:
			isValid = true;
			break;
		case lapsedMarking:
			isValid = true;
			break;
		case nomineePaymentEncashment:
			isValid = this.validateNomineePayEncashment(request);
			break;
		case nomineePaymentEncashmentNBP:
			isValid = this.validateNomineePayEncashment(request);
			break;
		case nomineePaymentReinvestment:

			if (request.getWorkFlowStatus() != null
					&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_APPROVE.toString())) {

				List<NomineePayDetailsResponseDTO> nomineePayDetails = (List<NomineePayDetailsResponseDTO>) this
						.parse(request, BPMTransactionKey.NOMINEE_PAYMENT_DETAILS);

				BigDecimal nonInvestAmount = BigDecimal.ZERO;

				for (NomineePayDetailsResponseDTO nomineePayDetail : nomineePayDetails) {
					nonInvestAmount = nonInvestAmount.add(new BigDecimal(nomineePayDetail.getNonInvestableAmount()));
				}

				if (!nonInvestAmount.equals(BigDecimal.ZERO)) {
					request.setWorkFlowStatus(BPMWorkFlowStatus.INCHARGE_APPROVED.toString());
				}
			}

			isValid = true;
			break;
		case uploadAttachment:
			isValid = true;
			break;
		case accountClosure:
			isValid = this.validateAccountStatusUpdate(request);
			break;
		case changeIssuanceDate:
			isValid = this.changeIssuanceDateValidation(request);
			break;
		case PCBReIssue:
			isValid = this.validatePCBReIssue(request);
			break;
		
		case changePCBStatus:
		case lapsedEncashmentByCash:
		case lapsedReInvestmentByCash:
		case restriction:
		case discardRegOrAcc:
		case profitAccural:
		case changeCertificateInventory:
		case changeEffectiveDate:
		case changeProfitSchedule:
		case identificationTypeUpdate:
		case singleToJoint:
		case jointToSingle:
		case guiUserProcess:
		case guiRoleProcess:
		case changeCertificateStatus:
		case recoveryTransaction:
		case inventoryUpload:

			isValid = true;
			break;

		}

		if (request.getAdditionalInfo() != null && request.getAdditionalInfo().containsKey("chequeDate"))
			this.validateChequeDate(request);

		return isValid;
	}

	public boolean changeIssuanceDateValidation(RegistrationRequestDTO request) throws Exception {

		boolean isSuc = false;
		try {
			String branchCode = request.getAdditionalInfo().get("branchCode");
			BranchResponseDTO branchResponse = this.getBranchByCode(branchCode);
			String effectiveDate = "";
			String issuanceDate = "";
			if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.REGISTRATION_CODE.toString())) {
				effectiveDate = request.getAdditionalInfo().get("newEffDate");
				issuanceDate = request.getAdditionalInfo().get("newIssuanceDate");
				Date effDate = new Date(Long.parseLong(effectiveDate));
				Date issDate = new Date(Long.parseLong(issuanceDate));

				if (branchResponse != null && branchResponse.getIsOnline().equalsIgnoreCase(Constants.YES))

					if (branchResponse.getCreatedDate() != null && (!effDate.before(branchResponse.getCreatedDate())
							|| !issDate.before(branchResponse.getCreatedDate()))) {

						throw new PaySysException(ResponseCode.ERR_CODE_ISSUANCE_EFFECTIVE_DATE,
								ResponseDesc.ERR_DESC_ISSUANCE_EFFECTIVE_DATE);
					}
			} else if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.ACCOUNT_NUMBER.toString())) {
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO) != null) {

					AccountIssuanceResponseDTO account = new MappingUtility<AccountIssuanceResponseDTO>(true).JsonToDto(
							this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO).getContent(),
							AccountIssuanceResponseDTO.class);

					for (CustomerAccountUnitIssuanceRequestDTO action : account.getCustomerAccountUnits()) {
						Date effDate = action.getNewEffectiveDate();
						Date issDate = action.getNewIssuanceDate();
						if (branchResponse != null && branchResponse.getIsOnline().equalsIgnoreCase(Constants.YES))

							if (branchResponse.getCreatedDate() != null
									&& (!effDate.before(branchResponse.getCreatedDate())
											|| !issDate.before(branchResponse.getCreatedDate()))) {
								throw new PaySysException(ResponseCode.ERR_CODE_ISSUANCE_EFFECTIVE_DATE,
										ResponseDesc.ERR_DESC_ISSUANCE_EFFECTIVE_DATE);
							}

					}

				}
			}
			isSuc = true;

		} catch (Exception ex) {
			logger.info("Error: " + ex);
			throw ex;
		}
		return isSuc;

	}

	@Override
	public ServiceResponse inquiry(Object genObj) throws PaySysException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}