package com.paysys.micro.services.maintenance.bl;

import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.exception.PaySysException;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class MaintenanceFactory {

    private final String moduleId;

    @Autowired
    private ApplicationContext context;

    public MaintenanceFactory(String moduleId) {
        this.moduleId = moduleId;
    }

    public APIInterface processTransaction(String apiKey) throws PaySysException, Exception {
        if (apiKey == null) {
            return null;
        }

        switch (this.moduleId) {

            case ModuleKeyConstants.MODULE_KEY_CUSTOMER_MAINTENANCE: {
                return (CustomerMaintenanceBL) context.getBean(CustomerMaintenanceBL.class, this.moduleId, apiKey);
            }
            case ModuleKeyConstants.MODULE_KEY_PRODUCT_MAINTENANCE: {
                return (ProductMaintenanceBL) context.getBean(ProductMaintenanceBL.class, this.moduleId, apiKey);
            }
            case ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS: {
                return (MaintenanceOperationsBL) context.getBean(MaintenanceOperationsBL.class, this.moduleId, apiKey);
            }

        }
        throw new PaySysException(ResponseCode.ERR_INVALID_API_KEY, ResponseDesc.ERR_DESC_INVALID_API_KEY);
    }

}
