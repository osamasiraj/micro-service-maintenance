package com.paysys.micro.services.maintenance;

import com.paysys.entity.common.RegistrationRequestDTO;
import com.paysys.micro.services.maintenance.bl.APIInterface;
import com.paysys.micro.services.maintenance.bl.MaintenanceFactory;
import com.paysys.service.common.APIKeyConstants;
import com.paysys.service.common.BaseController;
import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import com.paysys.service.response.ServiceResponse;
import com.paysys.service.utility.HelperFunctions;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Customer Maintenance Operations")
@Validated
@RestController
@RequestMapping(BaseController.BASE_PATH_MAINTENANCE + ModuleKeyConstants.MODULE_KEY_CUSTOMER)
public class CustomerMaintenanceController {

	private final static Logger logger = Logger.getLogger(CustomerMaintenanceController.class);

	@Autowired
	private ApplicationContext context;

	@ApiOperation(value = "Get Customer By Identifier")
	@PostMapping(value = "/getCustomerByIdentifier")
	@ApiResponses(value = { @ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
			@ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
			@ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
			@ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND) })

	public ServiceResponse getCustomerByIdentifier(
			@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

		logger.info("Function Called: getCustomerByIdentifier");
		logger.debug("Function Called: getCustomerByIdentifier");

		try {
			APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
					ModuleKeyConstants.MODULE_KEY_CUSTOMER_MAINTENANCE))
							.processTransaction(APIKeyConstants.API_KEY_GET_BY_IDENTIFIER);
			return serviceResponse.getBykey(registrationRequestDTO);

		} catch (Exception ex) {
			logger.info(ex);
			return new ServiceResponse(ResponseCode.FAILURE, ex);
		}
	}

	@ApiOperation(value = "Get Customer Details")
	@PostMapping(value = "/getCustomerDetails")
	@ApiResponses(value = { @ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
			@ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
			@ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
			@ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND) })

	public ServiceResponse getCustomerDetails(
			@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

		logger.info("Function Called: getCustomerDetails");

		try {
			APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
					ModuleKeyConstants.MODULE_KEY_CUSTOMER_MAINTENANCE))
							.processTransaction(APIKeyConstants.API_KEY_GET_CUSTOMER_DETAILS);
			return serviceResponse.getBykey(registrationRequestDTO);

		} catch (Exception ex) {
			logger.info(ex);
			return new ServiceResponse(ResponseCode.FAILURE, ex);
		}
	}
	
	@ApiOperation(value = "Get Customer Account/Certificate Summary")
	@PostMapping(value = "/getSummary")
	@ApiResponses(value = { @ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
			@ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
			@ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
			@ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND) })

	public ServiceResponse getSummary(
			@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

		logger.info("Function Called: inquiry");

		try {
			APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
					ModuleKeyConstants.MODULE_KEY_CUSTOMER_MAINTENANCE))
							.processTransaction(APIKeyConstants.API_KEY_SUMMARY);
			return serviceResponse.inquiry(registrationRequestDTO);

		} catch (Exception ex) {
			logger.info(ex);
			return new ServiceResponse(ResponseCode.FAILURE, ex);
		}
	}

	@ApiOperation(value = "Get Customer Account/Certificate inquiry")
	@PostMapping(value = "/inquiry")
	@ApiResponses(value = { @ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
			@ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
			@ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
			@ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND) })

	public ServiceResponse inquiry(
			@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

		logger.info("Function Called: inquiry");

		try {
			APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
					ModuleKeyConstants.MODULE_KEY_CUSTOMER_MAINTENANCE))
							.processTransaction(APIKeyConstants.API_KEY_INQUIRE);
			return serviceResponse.inquiry(registrationRequestDTO);

		} catch (Exception ex) {
			logger.info(ex);
			return new ServiceResponse(ResponseCode.FAILURE, ex);
		}
	}

}
