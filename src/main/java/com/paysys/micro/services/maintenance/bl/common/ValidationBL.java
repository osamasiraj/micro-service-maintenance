package com.paysys.micro.services.maintenance.bl.common;

import com.paysys.entity.common.CustomerDTO;
import com.paysys.entity.common.Filler;
import com.paysys.entity.common.RegistrationRequestDTO;
import com.paysys.entity.common.RegistrationRequestDetailDTO;
import com.paysys.requests.customer.CustomerAssociateDTO;
import com.paysys.requests.customer.ProductProfitDetailRequestDTO;
import com.paysys.requests.inventory.CertificateInventoryDTO;
import com.paysys.requests.inventory.InventoryRequestDTO;
import com.paysys.requests.product.AccountRequestDTO;
import com.paysys.responses.config.DocumentTypeResponseDTO;
import com.paysys.responses.customer.*;
import com.paysys.responses.inventory.InventoryDetailResponseDTO;
import com.paysys.responses.inventory.InventoryResponseDTO;
import com.paysys.responses.maintenance.BasicInfoResponseDTO;
import com.paysys.responses.product.*;
import com.paysys.responses.servicelog.AuditLogResponseDTO;
import com.paysys.service.common.APIKeyConstants;
import com.paysys.service.common.CertificateEnum;
import com.paysys.service.common.Constants;
import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.common.bl.BaseBL;
import com.paysys.service.common.enm.*;
import com.paysys.service.exception.PaySysException;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import com.paysys.service.response.ServiceResponse;
import com.paysys.service.utility.APICaller;
import com.paysys.service.utility.APICallerDetail;
import com.paysys.service.utility.HelperFunctions;
import com.paysys.service.utility.MappingUtility;
import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.PayloadApplicationEvent;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

@SuppressWarnings("unchecked")
public class ValidationBL extends BaseBL {

	private final static Logger logger = Logger.getLogger(ValidationBL.class);

	protected APICaller apiCaller;

	@Autowired
	protected ApplicationContext context;

	@PostConstruct
	public void postConstruct() {
		this.apiCaller = context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
	}

	protected CustomerDTO validateSearchResult(CustomerDTO product, RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: validateSearchResult");

		switch (BPMProcess.valueOf(this.getInnerRequestType(request))) {

		case customerDemographics:
			product.getProducts().removeIf(p -> p.getStatusCode().equalsIgnoreCase(StatusCodes.Decesed.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.NOT_FOUND, ResponseDesc.NOT_FOUND);

			break;

		case restriction:
		case changeSignature:
			// No validations
			break;
		case changeNominee:

			// product.getProducts().removeIf(p ->
			// !p.getStatusCode().equalsIgnoreCase(StatusCodes.Normal.toString())
			// && !p.getStatusCode().equalsIgnoreCase(StatusCodes.Decesed.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_NO_PRODUCT_FOR_NOMINEE_CHANGE,
						ResponseDesc.ERR_DESC_NO_PRODUCT_FOR_NOMINEE_CHANGE);

			break;
		case accountClosure:
			break;
		case minorToMajor:

			// Validate minor dob before showing search results
			CustomerKycResponseDTO minorKyc = this.getCustomerKYCByCode(product.getCustomerCode(),
					KycAttributeCode.KYC_DATE_OF_BIRTH);

			String minorDOB = minorKyc.getAttributeValue();
			if (new Date().getTime() - this.parseDate(minorDOB).getTime() < Constants.MILLIS_IN_18_YEARS)
				throw new PaySysException(ResponseCode.ERR_CODE_MINOR_NOT_FOUND, ResponseDesc.ERR_DESC_MINOR_NOT_FOUND);

			// Validate investor type on product
			product.getProducts()
					.removeIf(p -> !p.getInvestorTypeCode().equalsIgnoreCase(InvestorType.Individual_Minor.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_MINOR_NOT_FOUND, ResponseDesc.ERR_DESC_MINOR_NOT_FOUND);

			break;
		case certificateReIssue:
			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.NOT_FOUND, ResponseDesc.NOT_FOUND);
			break;
		case updateAccount:
			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_ACCOUNT_NOT_FOUND,
						ResponseDesc.ERR_DESC_ACCOUNT_NOT_FOUND);
			break;
		case WSBReIssue:
			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_ACCOUNT_NOT_FOUND,
						ResponseDesc.ERR_DESC_ACCOUNT_NOT_FOUND);
			break;
		case updateCertificate:
		case changeCertificateStatus:
		case singleToJoint:
		case jointToSingle:
			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_CERT_NOT_FOUND, ResponseDesc.ERR_DESC_CERT_NOT_FOUND);
			break;
		case certificateConversion:
		case accountConversion:

			// Validate account type as single
			product.getProducts().removeIf(p -> !p.getOperatingCode().equalsIgnoreCase(Constants.OP_MODE_SINGLE)
					|| p.getInvestorTypeCode().equalsIgnoreCase(InvestorType.Individual_Minor.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_CONVERSION_NOT_AVAILABLE,
						ResponseDesc.ERR_DESC_CONVERSION_NOT_AVAILABLE);

			break;
		case accountCorelation:

			product.getProducts().removeIf(
					p -> (p.getCorelatedAccountId() != null && !p.getCorelatedAccountId().equalsIgnoreCase("0")) // Migration
																													// Data
																													// Issue
							|| p.getCategoryCode().equalsIgnoreCase(ProductCategory.ACCOUNT.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_CORELATION_NOT_AVAILABLE,
						ResponseDesc.ERR_DESC_CORELATION_NOT_AVAILABLE);

			break;

		case changeWSBStatus:
			break;

		case lapsedRevival:
			// Filter out products that are not marked as lapsed by the job
			product.getProducts().removeIf(p -> !p.getStatusCode().equalsIgnoreCase(StatusCodes.Lasped.toString()));
			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_PRODUCT_NOT_FOUND_OR_INVALID_STATUS,
						ResponseDesc.ERR_DESC_FPRODUCT_NOT_FOUND_OR_INVALID_STATUS);
			break;

		case reInvestment:

			product.getProducts().removeIf(p -> !p.getStatusCode().equalsIgnoreCase(StatusCodes.Normal.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_PRODUCT_NOT_FOUND_OR_INVALID_STATUS,
						ResponseDesc.ERR_DESC_FPRODUCT_NOT_FOUND_OR_INVALID_STATUS);

			break;

		case P2PTransfer:

			product.getProducts()
					.removeIf(p -> !p.getCategoryCode().equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())
							|| !p.getStatusCode().equalsIgnoreCase(StatusCodes.Normal.toString())
							|| p.getCode().equalsIgnoreCase(Products.BSC.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_PRODUCT_NOT_FOUND_OR_INVALID_STATUS,
						ResponseDesc.ERR_DESC_FPRODUCT_NOT_FOUND_OR_INVALID_STATUS);

			break;

		case C2CTransferOut:

			product.getProducts().removeIf(
					p -> p.getHasPledged() || !p.getStatusCode().equalsIgnoreCase(StatusCodes.Normal.toString())
							|| p.getCategoryCode().equalsIgnoreCase(ProductCategory.ACCOUNT.toString())
							|| p.getCode().equalsIgnoreCase(Products.STSC3.toString())
							|| p.getCode().equalsIgnoreCase(Products.STSC6.toString())
							|| p.getCode().equalsIgnoreCase(Products.STSC12.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_PRODUCT_NOT_FOUND_OR_INVALID_STATUS,
						ResponseDesc.ERR_DESC_FPRODUCT_NOT_FOUND_OR_INVALID_STATUS);
			break;

		case nomineePayment:

			product.getProducts().removeIf(p -> !p.getStatusCode().equals(StatusCodes.Decesed.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_NO_PRODUCT_FOR_NOMINEE_PAY,
						ResponseDesc.ERR_DESC_NO_PRODUCT_FOR_NOMINEE_PAY);

			break;

		case uploadAttachment:
			break;

		case PCBReIssue:

			// product.getProducts().removeIf(p ->
			// !p.getStatusCode().equalsIgnoreCase(StatusCodes.Normal.toString())
			// && !p.getStatusCode().equalsIgnoreCase(StatusCodes.Block.toString()));

			if (product.getProducts().isEmpty())
				throw new PaySysException(ResponseCode.ERR_CODE_PRODUCT_NOT_FOUND_OR_INVALID_STATUS,
						ResponseDesc.ERR_DESC_FPRODUCT_NOT_FOUND_OR_INVALID_STATUS);

			break;

		default:
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
		}

		return product;
	}

	protected List<CustomerDocumentResponseDTO> validateCustomerDocsGlobal(
			List<CustomerDocumentResponseDTO> customerDocs, String customerCode) throws Exception {
		logger.debug("Function called: validateCustomerDocsGlobal");

		ServiceResponse response;

		response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_DOCUMENT,
				APIKeyConstants.API_KEY_GET_BY_SCOPE_AND_TYPE,
				new APICallerDetail().add(Constants.GLOBAL).add(Constants.DOC_TYPE_ISSUEABLE));

		if (response.getCode() == ResponseCode.SUCCESS) {

			List<DocumentTypeResponseDTO> documents = new MappingUtility<List<DocumentTypeResponseDTO>>()
					.JsonToDto(response.getData(), new TypeToken<List<DocumentTypeResponseDTO>>() {
					}.getType());

			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_DOCUMENT,
					APIKeyConstants.API_KEY_GET_BY_CUST_TYPE_AND_SCOPE,
					new APICallerDetail().add(customerCode).add(Constants.DOC_TYPE_ISSUEABLE).add(Constants.GLOBAL));

			List<CustomerDocumentResponseDTO> customerDocumentResponseDTOs = new ArrayList<>();

			if (response.getCode() == ResponseCode.SUCCESS) {
				customerDocumentResponseDTOs = new MappingUtility<List<CustomerDocumentResponseDTO>>()
						.JsonToDto(response.getData(), new TypeToken<List<CustomerDocumentResponseDTO>>() {
						}.getType());
			}

			boolean exists = false;
			for (DocumentTypeResponseDTO document : documents) {
				for (CustomerDocumentResponseDTO custDoc : customerDocumentResponseDTOs) {
					if (custDoc.getDocumentType().getCode().equalsIgnoreCase(document.getCode()))
						exists = true;
				}
				if (!exists) {
					CustomerDocumentResponseDTO custDoc = new CustomerDocumentResponseDTO();
					custDoc.setCustomerCode(customerCode);
					custDoc.setDocumentType(document);
					custDoc.setCustomerDocumentAttachment(new CustomerDocAttachmentResponseDTO());
					customerDocumentResponseDTOs.add(custDoc);
				}
			}
			return customerDocumentResponseDTOs;

		} else
			throw new PaySysException(ResponseCode.FAILURE, ResponseDesc.DATA_NOT_FOUND);
		// investorDocumentDetailResponseDTOs.forEach(investorDoc ->
		// investorDoc.setIdentifierValue(identifierValue));
	}

	protected boolean validateCustomerDemographics(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: validateCustomerDemographics");

		boolean isValid = false;

		CustomerResponseDTO customer = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);
		List<KycAttributeResponseDTO> customerKyc = (List<KycAttributeResponseDTO>) this.parse(request,
				BPMTransactionKey.CUSTOMER_KYC);

		// Validate customer deceased date
		if (customer.getLastStatusUpdate() != null && customer.getLastStatusUpdate().getTime() > new Date().getTime())
			throw new PaySysException(ResponseCode.ERR_CODE_INVALID_DECEASED_DATE,
					ResponseDesc.ERR_DESC_INVALID_DECEASED_DATE);

		// Get required KYC attributes for validation
		String customerDOB = null;
		String idenTypeExpiry = null;
		for (KycAttributeResponseDTO kyc : customerKyc) {
			if (kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_DATE_OF_BIRTH.toString())) {
				customerDOB = kyc.getAttributeValue();
			} else if (kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_EXPIRY_DATE.toString())) {
				idenTypeExpiry = kyc.getAttributeValue();
			}
		}

		// Validate Customer DOB By Investor Type
		isValid = this.validateInvestorDOB(customer.getCode(), customerDOB);

		// Validate Customer Identification Type Expiry Date
		if (isValid) {
			if (idenTypeExpiry != null && this.validateIdenTypeExpiry(idenTypeExpiry))
				isValid = true;
			else if (idenTypeExpiry != null)
				throw new PaySysException(ResponseCode.ERR_CODE_IDENT_EXPIRED, ResponseDesc.ERR_DESC_IDENT_EXPIRED);

		} else
			throw new PaySysException(ResponseCode.FAILURE, ResponseDesc.ERR_DESC_INVALID_DOB);

		return isValid;

	}

	private boolean validateIdenTypeExpiry(String identTypeExpiry) throws Exception {
		logger.debug("Function called: validateIdenTypeExpiry");

		return this.parseDate(identTypeExpiry).after(new Date());
	}

	private boolean validateInvestorDOB(String customerCode, String customerDOB) throws Exception {
		logger.debug("Function called: validateInvestorDOB");

		boolean isValid = true;
		if (customerDOB != null && !customerDOB.isEmpty()) {
			ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVESTOR,
					APIKeyConstants.API_KEY_VALIDATE_DOB_BY_CUST_CODE,
					new APICallerDetail().add(customerCode).add(customerDOB));

			if (response.getCode() != ResponseCode.SUCCESS) {

				throw new PaySysException(ResponseCode.ERR_CODE_WRONG_INVESTMENT_TYPE,
						ResponseDesc.ERR_DESC_WRONG_INVESTMENT_TYPE);
			}
		}
		return isValid;
	}

	protected boolean validateCustomerAttachment(RegistrationRequestDTO request) throws Exception {
		List<CustomerDocumentResponseDTO> attachedDocs = (List<CustomerDocumentResponseDTO>) this.parse(request,
				BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT);

		if (attachedDocs == null || attachedDocs.size() == 0)
			throw new PaySysException(ResponseCode.ERR_CODE_DOCUMENT_IS_MANDATORY,
					ResponseDesc.ERR_DESC_DOCUMENT_IS_MANDATORY);

		BasicInfoResponseDTO customer = (BasicInfoResponseDTO) this.parse(request, BPMTransactionKey.BASIC_INFO);
		CustomerResponseDTO customerResponse = null;
		List<CustomerResponseDTO> customers = null;
		String investorTypeCode = null;
		boolean isValid = false;

		switch (CustomerIdentityKey.valueOf(request.getCustIdentityKey().toUpperCase())) {
		case ACCOUNT_NUMBER:
			customers = this.getCustomerByIdentificationKey(request, CustomerIdentityKey.ACCOUNT_NUMBER);

			AccountResponseDTO account = this.getAccountByNumber(request.getCustIdentityValue());

			customerResponse = customers.stream()
					.filter(cust -> cust.getCode().equalsIgnoreCase(customer.getCustomers().get(0).getCustomerCode()))
					.findFirst().orElse(null);

			CustomerAccountRelResponseDTO custRel = account.getCustomerAccountRels().stream()
					.filter(acc -> acc.getProduct().getCode().equalsIgnoreCase(account.getProductCode())
							&& acc.getCustomerCode().equalsIgnoreCase(customer.getCustomers().get(0).getCustomerCode()))
					.findFirst().orElse(null);

			if (custRel != null) {
				investorTypeCode = custRel.getInvestorType().getType();
			} else
				throw new PaySysException(ResponseCode.ERR_CODE_ACCOUNT_NOT_FOUND,
						ResponseDesc.ERR_DESC_ACCOUNT_NOT_FOUND);

			break;
		case REGISTRATION_CODE:
			customers = this.getCustomerByIdentificationKey(request, CustomerIdentityKey.REGISTRATION_CODE);

			List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCodeAndBranch(request);

			customerResponse = customers.stream()
					.filter(cust -> cust.getCode().equalsIgnoreCase(customer.getCustomers().get(0).getCustomerCode()))
					.findFirst().orElse(null);

			custRel = certificates.get(0).getCustomerAccountRels().stream().filter(
					cert -> cert.getProduct().getCode().equalsIgnoreCase(certificates.get(0).getProductCode()) && cert
							.getCustomerCode().equalsIgnoreCase(customer.getCustomers().get(0).getCustomerCode()))
					.findFirst().orElse(null);

			if (custRel != null) {
				investorTypeCode = custRel.getInvestorType().getType();

			} else
				throw new PaySysException(ResponseCode.ERR_CODE_CERT_NOT_FOUND, ResponseDesc.ERR_DESC_CERT_NOT_FOUND);

			break;
		case CUSTOMER_CODE:

			customerResponse = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);

			break;

		default:
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
		}

		if (customerResponse != null && investorTypeCode != null) {
			CustomerDTO customerDTO = this.getCustomerWithBasicInfo(Collections.singletonList(customerResponse)).get(0);
			this.validateCustomerAttachableDocs(request, customerDTO, attachedDocs);
			isValid = true;

		} else if (investorTypeCode == null) {
			isValid = true;

		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);

		return isValid;

	}

	protected boolean validateCustomerDocuments(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: validateRequiredDocuments");
		boolean isValid = false;
		List<InvestorDocumentDetailResponseDTO> investorDocs = (List<InvestorDocumentDetailResponseDTO>) this
				.parse(request, BPMTransactionKey.CUSTOMER_DOCUMENTS);

		for (InvestorDocumentDetailResponseDTO doc : investorDocs) {
			if (doc.getIsMandatory().equalsIgnoreCase(Constants.YES) && !doc.getCheckFlag())
				throw new PaySysException(ResponseCode.ERR_CODE_VERIFY_MANDATORY_DOCUMENTS,
						ResponseDesc.ERR_DESC_VERIFY_MANDATORY_DOCUMENTS);
		}
		return true;

	}

	private boolean validateSeniorCitizen(String invTypeCode, String customerDOB) throws Exception {
		logger.debug("Function called: validateSeniorCitizen");

		return new Date().getTime() - this.parseDate(customerDOB).getTime() >= (Constants.MILLIS_IN_60_YEARS);
	}

	protected boolean validateReIssueAmount(RegistrationRequestDTO request) throws PaySysException {
		logger.debug("Function called: validateReIssueAmount");

		boolean isValid = false;
		BigDecimal certificatesSum = BigDecimal.ZERO;
		BigDecimal totalInvestmentAmount = new BigDecimal(request.getAmount());

		RegistrationRequestDetailDTO requestDetails = request.getRegistrationRequestDetailDTO().stream()
				.filter(d -> d.getTransactionKey()
						.equalsIgnoreCase(BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY.toString()))
				.findFirst().orElse(null);

		if (requestDetails != null) {

			List<CustomerCertificateResponseDTO> certificates = (List<CustomerCertificateResponseDTO>) new MappingUtility<>(
					false).JsonToDto(requestDetails.getContent(),
							new TypeToken<List<CustomerCertificateResponseDTO>>() {
							}.getType());

			for (CustomerCertificateResponseDTO cert : certificates) {
				if (cert.getInventoryDetail().getInventoryStatus().getCode()
						.equalsIgnoreCase(InventoryStatuses.ISSUED.toString())
						|| cert.getInventoryDetail().getInventoryStatus().getCode()
								.equalsIgnoreCase(InventoryStatuses.READY.toString()))
					certificatesSum = certificatesSum.add(new BigDecimal(cert.getDenomination().getValue()));
			}
		}
		if (certificatesSum.equals(totalInvestmentAmount))
			isValid = true;
		else
			throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CERT_DENOM,
					ResponseDesc.ERR_DESC_INVALID_CERT_DENOM);

		return isValid;
	}

	protected boolean validateCertificateInventory(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: validateCertificateInventory");
		boolean isValid = false;

		RegistrationRequestDetailDTO requestDetails = request.getRegistrationRequestDetailDTO().stream()
				.filter(d -> d.getTransactionKey()
						.equalsIgnoreCase(BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY.toString()))
				.findFirst().orElse(null);

		if (requestDetails != null) {

			List<CustomerCertificateResponseDTO> certificates = (List<CustomerCertificateResponseDTO>) new MappingUtility<>(
					false).JsonToDto(requestDetails.getContent(),
							new TypeToken<List<CustomerCertificateResponseDTO>>() {
							}.getType());

			List<Long> certInvIds = new ArrayList<>();

			for (CustomerCertificateResponseDTO certificate : certificates) {
				if (certificate.getInventoryDetail().getInventoryStatus().getCode()
						.equalsIgnoreCase(InventoryStatuses.READY.toString()))
					certInvIds.add(Long.valueOf(certificate.getId()));
			}

			if (request.getWorkFlowStatus() != null
					&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_REJECT.toString()))

				isValid = this.discardCertificate(certInvIds);
			else if (request.getWorkFlowStatus() == null
					|| !request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_APPROVE.toString())) {
				isValid = reserveCertificateInventory(certInvIds);
			} else {
				isValid = true;
			}

		} else
			throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
					ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);

		return isValid;
	}

	protected boolean validateNomineeShare(RegistrationRequestDTO request) throws PaySysException {
		logger.debug("Function called: validateNomineeShare");

		boolean isValid = false;
		int totalShare = 0;

		List<CustomerAssociateDTO> nominees = (List<CustomerAssociateDTO>) this.parse(request,
				BPMTransactionKey.CUSTOMER_ASSOCIATE);

		// Set Customer Nominee Display Name
		Object obj = this.parse(request, BPMTransactionKey.CUSTOMER_ASSOCIATE);
		BasicInfoResponseDTO basicInfo = (BasicInfoResponseDTO) this.parse(request, BPMTransactionKey.BASIC_INFO);

		if (obj instanceof ArrayList) {

			List<CustomerAssociateDTO> associates = ((ArrayList) obj);
			associates.forEach(nom -> {
				nom.getKycAttributeResponseDTOs().stream()
						.filter(kyc -> kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_FIRST_NAME.toString()))
						.findFirst().ifPresent(kycAttr -> nom.setDisplayName(kycAttr.getAttributeValue()));
			});

			request.getRegistrationRequestDetailDTO().clear();

			RegistrationRequestDetailDTO assoDet = new RegistrationRequestDetailDTO();
			assoDet.setContent(associates);
			assoDet.setTransactionKey(BPMTransactionKey.CUSTOMER_ASSOCIATE.toString());

			request.getRegistrationRequestDetailDTO().add(assoDet);

			RegistrationRequestDetailDTO basicDet = new RegistrationRequestDetailDTO();
			basicDet.setContent(basicInfo);
			basicDet.setTransactionKey(BPMTransactionKey.BASIC_INFO.toString());

			request.getRegistrationRequestDetailDTO().add(basicDet);

		}

		if (nominees != null) {

			for (CustomerAssociateDTO nominee : nominees) {
				totalShare = totalShare + Integer.parseInt(nominee.getNomineeShare());
			}
		}
		if (totalShare == 100)
			isValid = true;
		else
			throw new PaySysException(ResponseCode.ERR_CODE_INVALID_SHARE, ResponseDesc.ERR_DESC_INVALID_SHARE);

		return isValid;
	}

	protected boolean validateAccountStatusUpdate(RegistrationRequestDTO request) throws Exception {
		boolean isValid = true;
		HashMap<String, String> additionalInfo = (HashMap<String, String>) request.getAdditionalInfo();
		if (additionalInfo.containsKey("updatedAccountStatus")) {

			RegistrationRequestDetailDTO requestDetail = this.getRegistrationRequestDetailByKey(request,
					BPMTransactionKey.ACCOUNT_INFO);

			if (requestDetail == null) {
				throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
						ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);
			}

			AccountResponseDTO account = (AccountResponseDTO) new MappingUtility<>(false)
					.JsonToDto(requestDetail.getContent(), AccountResponseDTO.class);

			// Closed status
			if (additionalInfo.get("updatedAccountStatus").equalsIgnoreCase(AccountStatus.CLOSED.toString())) {

				if (!account.getCurrentBalance().equals(BigDecimal.ZERO)
						|| !account.getAvaliableBalance().equals(BigDecimal.ZERO))
					throw new PaySysException(ResponseCode.ERR_CODE_ACCOUNT_NOT_EMPTY,
							ResponseDesc.ERR_DESC_ACCOUNT_NOT_EMPTY);

			} else {

				String oldStatus = account.getAccountStatus().getCode();
				String newStatus = additionalInfo.get("updatedAccountStatus");

				ServiceResponse response = apiCaller.execute(RequestType.GET,
						ModuleKeyConstants.MODULE_KEY_ACCOUNT_STATUS, APIKeyConstants.API_KEY_GET_ALLOWED_STATUS_FILLER,
						new APICallerDetail().add(oldStatus).add("ACCOUNT_MAINT"));

				List<Filler> allowedStatuses = new MappingUtility<List<Filler>>(false).JsonToDto(response.getData(),
						new TypeToken<List<Filler>>() {
						}.getType());

				Filler filler = allowedStatuses.stream().filter(status -> status.getValue().equalsIgnoreCase(newStatus))
						.findFirst().orElseThrow(() -> new PaySysException(ResponseCode.ERR_CODE_INVALID_ACCOUNT_STATUS,
								ResponseDesc.ERR_DESC_INVALID_ACCOUNT_STATUS));

				logger.debug(String.format("Account Status Old: %1$s New: %2$s", oldStatus, newStatus));

			}

		} else {
			throw new PaySysException(ResponseCode.ERR_CODE_INVALID_ACCOUNT_STATUS,
					ResponseDesc.ERR_DESC_INVALID_ACCOUNT_STATUS);
		}
		return isValid;
	}

	protected boolean validateLapsedEncashment(RegistrationRequestDTO request) throws Exception {
		RegistrationRequestDetailDTO lapsedDetails = this.getRequestDetailsByKey(request,
				BPMTransactionKey.LAPSED_REVIVAL_REQUEST);
		RegistrationRequestDetailDTO jointDetails = this.getRequestDetailsByKey(request,
				BPMTransactionKey.JOINT_REQUEST);

		if (lapsedDetails == null) {
			throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
					ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);
		}

		CustomerDTO primaryCustomer = new MappingUtility<CustomerDTO>(false).JsonToDto(lapsedDetails.getContent(),
				CustomerDTO.class);
		StringBuilder chequeTitle = new StringBuilder().append(primaryCustomer.getDisplayName());

		if (jointDetails != null) {
			CustomerDTO jointCustomer = new MappingUtility<CustomerDTO>(false).JsonToDto(jointDetails.getContent(),
					CustomerDTO.class);
			chequeTitle.append("/").append(jointCustomer.getDisplayName());
		}

		request.getAdditionalInfo().put("chequeTitle", chequeTitle.toString());

		return true;
	}

	protected boolean validateNomineePayEncashment(RegistrationRequestDTO request) throws Exception {
		CustomerAssociateDTO nominee = (CustomerAssociateDTO) this.parse(request, BPMTransactionKey.NOMINEE_DETAILS);
		request.getAdditionalInfo().put("chequeTitle", nominee.getDisplayName());
		return true;
	}

	protected boolean validateC2CTransferOut(RegistrationRequestDTO request) throws Exception {
		boolean isValid;
		if (request.getWorkFlowStatus() != null
				&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.DEALING_OFFICER_APPROVE.toString())
				&& !request.getCategory().equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())) {
			isValid = this.validateWSBInventory(request);
		} else
			isValid = true;

		return isValid;
	}

	protected boolean validateC2CTransferIn(RegistrationRequestDTO request) throws Exception {
		boolean isValid;

		if (!request.getCategory().equalsIgnoreCase(ProductCategory.CERTIFICATE.toString()))
			isValid = this.validateUnitProfitPaid(request);
		else
			isValid = this.validateCertProfitPaid(request);

		if (request.getWorkFlowStatus() != null
				&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.DEALING_OFFICER_APPROVE.toString())
				&& !request.getCategory().equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())) {

			isValid = this.validateWSBInventory(request);

		} else if (request.getWorkFlowStatus() == null && request.getAdditionalInfo() != null
				&& request.getCategory().equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())) {

			ProductProfitDetailRequestDTO profitDetails = new ProductProfitDetailRequestDTO();
			profitDetails.setEffectiveDate(new Date(Long.parseLong(request.getAdditionalInfo().get("effectiveDate"))));
			profitDetails.setProductCode(request.getProductCode());

			ServiceResponse response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_PRODUCT,
					APIKeyConstants.API_KEY_GET_MATURITY_AND_PROFIT_COUNT, new APICallerDetail().setObj(profitDetails));

			profitDetails = new MappingUtility<ProductProfitDetailRequestDTO>().JsonToDto(response.getData(),
					ProductProfitDetailRequestDTO.class);

			request.getAdditionalInfo().put("maturityDate", String.valueOf(profitDetails.getMaturityDate().getTime()));

			isValid = true;

		} else
			isValid = true;

		return isValid;

	}

	protected boolean validateUnitProfitPaid(RegistrationRequestDTO request) throws Exception {
		boolean isValid = true;

		RegistrationRequestDetailDTO unitDetails = this.getRequestDetailsByKey(request,
				BPMTransactionKey.TRANSFERER_ACC_INFO);

		CustomerAccountDTO customerAccount = new MappingUtility<CustomerAccountDTO>(false)
				.JsonToDto(unitDetails.getContent(), CustomerAccountDTO.class);

		ProductResponseDTO productResponse = this.getProduct(request.getProductCode());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		for (ProfitDetailResponseDTO unit : customerAccount.getCustomerAccountUnits()) {

			if (Integer.parseInt(unit.getProfitsPaid()) > Integer.parseInt(unit.getProfitCount()))
				throw new PaySysException(ResponseCode.ERR_CODE_PROFIT_PAID_GREATER_THAN_PROFIT_COUNT,
						ResponseDesc.ERR_DESC_PROFIT_PAID_GREATER_THAN_PROFIT_COUNT);

			long months = ChronoUnit.MONTHS.between(LocalDate.parse(sdf.format(unit.getEffectiveDate())),
					LocalDate.parse(sdf.format(new Date())));

			long maxProfitsDue = months / productResponse.getProfitPaymentCycle().intValue();

			if (Long.parseLong(unit.getProfitsPaid()) > maxProfitsDue)
				throw new PaySysException(ResponseCode.ERR_CODE_PROFITS_PAID_GREATE_THAN_DUE,
						ResponseDesc.ERR_DESC_PROFITS_PAID_GREATE_THAN_DUE + " Effective Date: "
								+ new SimpleDateFormat("dd-MM-yyyy").format(unit.getEffectiveDate()));
		}

		return isValid;
	}

	protected boolean validateCertProfitPaid(RegistrationRequestDTO request) throws Exception {
		boolean isValid = true;

		Date effectiveDate = new Date(Long.parseLong(request.getAdditionalInfo().get("effectiveDate")));

		ProductResponseDTO productResponse = this.getProduct(request.getProductCode());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		if (Integer.parseInt(request.getAdditionalInfo().get("profitsPaid")) > Integer
				.parseInt(request.getAdditionalInfo().get("profitCount")))
			throw new PaySysException(ResponseCode.ERR_CODE_PROFIT_PAID_GREATER_THAN_PROFIT_COUNT,
					ResponseDesc.ERR_DESC_PROFIT_PAID_GREATER_THAN_PROFIT_COUNT);

		long months = ChronoUnit.MONTHS.between(LocalDate.parse(sdf.format(effectiveDate)),
				LocalDate.parse(sdf.format(new Date())));

		long maxProfitsDue = months / productResponse.getProfitPaymentCycle().intValue();

		if (Long.parseLong(request.getAdditionalInfo().get("profitsPaid")) > maxProfitsDue)
			throw new PaySysException(ResponseCode.ERR_CODE_PROFITS_PAID_GREATE_THAN_DUE,
					ResponseDesc.ERR_DESC_PROFITS_PAID_GREATE_THAN_DUE + " Effective Date: "
							+ new SimpleDateFormat("dd-MM-yyyy").format(effectiveDate));

		return isValid;
	}

	protected boolean validateCertificateStatusUpdate(RegistrationRequestDTO request) throws Exception {
		boolean isValid = false;

		List<CertificateInventoryDTO> certificates = (List<CertificateInventoryDTO>) this.parse(request,
				BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY);

		if (certificates.stream().anyMatch(cert -> cert.getProductCode().equalsIgnoreCase(Products.BSC.toString())
				&& cert.getNewStatusCode().equalsIgnoreCase(AccountStatus.PLEDGE.toString()))) {

			throw new PaySysException(ResponseCode.ERR_CODE_PLEDGE_NOT_ALLOWED,
					ResponseDesc.ERR_DESC_PLEDGE_NOT_ALLOWED);

		} else {

			for (CertificateInventoryDTO certificate : certificates) {

				String oldStatus = certificate.getStatusCode();
				String newStatus = certificate.getNewStatusCode();

				if (oldStatus.equalsIgnoreCase(newStatus))
					continue;

				ServiceResponse response = apiCaller.execute(RequestType.GET,
						ModuleKeyConstants.MODULE_KEY_ACCOUNT_STATUS, APIKeyConstants.API_KEY_GET_ALLOWED_STATUS_FILLER,
						new APICallerDetail().add(oldStatus).add("CERTIFICATE_MAINT"));

				List<Filler> allowedStatuses = new MappingUtility<List<Filler>>(false).JsonToDto(response.getData(),
						new TypeToken<List<Filler>>() {
						}.getType());

				logger.info(String.format("Certificate Status Old: %1$s New: %2$s", oldStatus, newStatus));

				Filler filler = allowedStatuses.stream().filter(status -> status.getValue().equalsIgnoreCase(newStatus))
						.findFirst().orElseThrow(() -> new PaySysException(ResponseCode.ERR_CODE_INVALID_CERT_STATUS,
								ResponseDesc.ERR_DESC_INVALID_CERT_STATUS));

				isValid = true;
			}
		}
		return isValid;
	}

	protected boolean validateWSBInventory(RegistrationRequestDTO request) throws Exception {

		if (request.getAdditionalInfo().containsKey("insertWSB"))
			return true;

		ServiceResponse response = null;
		boolean isValid = false;

		String wsbStartingNumber;
		String wsbEndingNumber;
		String wsbQuantity;
		String wsbInventoryId;

		String bpmWsbStartingNumber;
		String bpmWsbEndingNumber;
		String bpmWsbQuantity;
		String bpmWsbInventoryId;

		if (request.getAdditionalInfo() == null)
			throw new PaySysException(ResponseCode.ERR_CODE_WSB_MISSING_OR_INVALID,
					ResponseDesc.ERR_DESC_WSB_MISSING_OR_INVALID);

		AccountRequestDTO accountDetails = (AccountRequestDTO) this.parse(request, BPMTransactionKey.ACCOUNT_INFO);

		if (request.getAdditionalInfo().containsKey("AccountExists")) {

			isValid = true;

		} else if (accountDetails != null && request.getAdditionalInfo().containsKey("WSBInventoryIdAccount")) {

			response = this.getResponseFromBPM(request, BPMTransactionKey.ACCOUNT_INFO);

			RegistrationRequestDTO bpmData = new MappingUtility<RegistrationRequestDTO>(true)
					.JsonToDto(response.getData(), RegistrationRequestDTO.class);

			wsbStartingNumber = request.getAdditionalInfo().get("WSBStartingNumAccount");
			wsbEndingNumber = request.getAdditionalInfo().get("WSBEndingNumAccount");
			wsbQuantity = request.getAdditionalInfo().get("WSBQuantityAccount");
			wsbInventoryId = request.getAdditionalInfo().get("WSBInventoryIdAccount");

			bpmWsbStartingNumber = bpmData.getAdditionalInfo().get("WSBStartingNumAccount");
			bpmWsbEndingNumber = bpmData.getAdditionalInfo().get("WSBEndingNumAccount");
			bpmWsbQuantity = bpmData.getAdditionalInfo().get("WSBQuantityAccount");
			bpmWsbInventoryId = bpmData.getAdditionalInfo().get("WSBInventoryIdAccount");

			if (wsbStartingNumber.equals(bpmWsbStartingNumber) && wsbEndingNumber.equals(bpmWsbEndingNumber)
					&& wsbQuantity.equals(bpmWsbQuantity) && wsbInventoryId.equals(bpmWsbInventoryId))
				isValid = true;
			else
				throw new PaySysException(ResponseCode.ERR_CODE_INVALID_WSLIP_BOOK_NUMBER,
						ResponseDesc.ERR_DESC_INVALID_WSLIP_BOOK_NUMBER);

		} else if (accountDetails != null && (request.getWorkFlowStatus() == null
				|| !request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_REJECT.toString()))) {

			Map<String, String> wslipBookStartingNum = HelperFunctions
					.separateDigitsAndAlphabets(request.getAdditionalInfo().get("WSBStartingNumAccount"));
			Map<String, String> wslipBookEndingNum = HelperFunctions
					.separateDigitsAndAlphabets(request.getAdditionalInfo().get("WSBEndingNumAccount"));

			wsbInventoryId = this.reserveWSBInventory(wslipBookStartingNum.get("alphabets"),
					wslipBookStartingNum.get("numbers"), wslipBookEndingNum.get("numbers"), request.getUserTypeKey(),
					accountDetails.getProductCode());

			request.getAdditionalInfo().put("WSBInventoryIdAccount", wsbInventoryId);

			isValid = true;

		} else if (accountDetails != null && request.getWorkFlowStatus() != null
				&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_REJECT.toString())) {

			this.releaseInventory(request.getAdditionalInfo().get("WSBInventoryIdAccount"));
			request.getAdditionalInfo().remove("WSBInventoryIdAccount");

			isValid = true;

		}

		AccountRequestDTO corrAccDetails = (AccountRequestDTO) this.parse(request, BPMTransactionKey.CUSTOMER_ACCOUNT);

		if (request.getAdditionalInfo().containsKey("CorelatedAccountExists")) {

			isValid = true;

		} else if (corrAccDetails != null && request.getAdditionalInfo().containsKey("WSBInventoryIdCorrelatedAcc")) {

			response = this.getResponseFromBPM(request, BPMTransactionKey.CUSTOMER_ACCOUNT);

			RegistrationRequestDTO bpmData = new MappingUtility<RegistrationRequestDTO>(false)
					.JsonToDto(response.getData(), RegistrationRequestDTO.class);

			wsbStartingNumber = request.getAdditionalInfo().get("WSBStartingNumCorrelatedAcc");
			wsbEndingNumber = request.getAdditionalInfo().get("WSBEndingNumCorrelatedAcc");
			wsbQuantity = request.getAdditionalInfo().get("WSBQuantityCorrelatedAcc");
			wsbInventoryId = request.getAdditionalInfo().get("WSBInventoryIdCorrelatedAcc");

			bpmWsbStartingNumber = bpmData.getAdditionalInfo().get("WSBStartingNumCorrelatedAcc");
			bpmWsbEndingNumber = bpmData.getAdditionalInfo().get("WSBEndingNumCorrelatedAcc");
			bpmWsbQuantity = bpmData.getAdditionalInfo().get("WSBQuantityCorrelatedAcc");
			bpmWsbInventoryId = bpmData.getAdditionalInfo().get("WSBInventoryIdCorrelatedAcc");

			if (wsbStartingNumber.equals(bpmWsbStartingNumber) && wsbEndingNumber.equals(bpmWsbEndingNumber)
					&& wsbQuantity.equals(bpmWsbQuantity) && wsbInventoryId.equals(bpmWsbInventoryId))
				isValid = true;
			else
				throw new PaySysException(ResponseCode.ERR_CODE_INVALID_WSLIP_BOOK_NUMBER,
						ResponseDesc.ERR_DESC_INVALID_WSLIP_BOOK_NUMBER);

		} else if (corrAccDetails != null && (request.getWorkFlowStatus() == null
				|| !request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_REJECT.toString()))) {

			Map<String, String> wslipBookStartingNum = HelperFunctions
					.separateDigitsAndAlphabets(request.getAdditionalInfo().get("WSBStartingNumCorrelatedAcc"));
			Map<String, String> wslipBookEndingNum = HelperFunctions
					.separateDigitsAndAlphabets(request.getAdditionalInfo().get("WSBEndingNumCorrelatedAcc"));

			wsbInventoryId = this.reserveWSBInventory(wslipBookStartingNum.get("alphabets"),
					wslipBookStartingNum.get("numbers"), wslipBookEndingNum.get("numbers"), request.getUserTypeKey(),
					corrAccDetails.getProductCode());

			request.getAdditionalInfo().put("WSBInventoryIdCorrelatedAcc", wsbInventoryId);

			isValid = true;

		} else if (corrAccDetails != null && request.getWorkFlowStatus() != null
				&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_REJECT.toString())) {

			this.releaseInventory(request.getAdditionalInfo().get("WSBInventoryIdCorrelatedAcc"));
			request.getAdditionalInfo().remove("WSBInventoryIdCorrelatedAcc");
			isValid = true;

		}

		return isValid;
	}

	protected String getInnerRequestType(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getInnerRequestType");

		try {

			HashMap<String, String> additionalInfo = (HashMap<String, String>) request.getAdditionalInfo();
			return additionalInfo.get("requestType");

		} catch (NullPointerException e) {
			logger.debug("Request type not set in additional info");
			return null;
		}
	}

	protected String reserveWSBInventory(String series, String start, String end, String branchCode, String productCode)
			throws Exception {

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVENTORY,
				APIKeyConstants.API_VALIDATE_WSB_REISSUANCE,
				new APICallerDetail().add(branchCode).add(series).add(start).add(end));

		if (response.getCode() == ResponseCode.SUCCESS) {

			List<InventoryResponseDTO> inventory = (List<InventoryResponseDTO>) new MappingUtility<List<InventoryResponseDTO>>()
					.JsonToDto(response.getData(), new TypeToken<List<InventoryResponseDTO>>() {
					}.getType());

			if (inventory.size() > 1)
				throw new PaySysException(ResponseCode.ERR_CODE_INVALID_INVENTORY,
						ResponseDesc.ERR_DESC_INVALID_INVENTORY + " Reason: Multiple entries found in inventory");

			// Validate Leaf Uniqueness
			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVENTORY,
					APIKeyConstants.API_GET_INVENTORY_BY_LEAF_NUMBER,
					new APICallerDetail().add(inventory.get(0).getInventoryDetails().get(0).getLeaveCertNo()));

			List<InventoryDetailResponseDTO> inventoryDetails = (List<InventoryDetailResponseDTO>) new MappingUtility<List<InventoryDetailResponseDTO>>()
					.JsonToDto(response.getData(), new TypeToken<List<InventoryDetailResponseDTO>>() {
					}.getType());

			if (inventoryDetails.stream().anyMatch(det -> det.getInventoryID() != inventory.get(0).getId()))
				throw new PaySysException(ResponseCode.ERR_CODE_INVALID_INVENTORY,
						ResponseDesc.ERR_DESC_INVALID_INVENTORY
								+ " Reason: Multiple entries found in inventory details");

			// Validate Inventory Status
			if (!inventory.get(0).getStatusCode().equalsIgnoreCase("200"))
				throw new PaySysException(ResponseCode.ERR_CODE_INVALID_INVENTORY_STATUS,
						ResponseDesc.ERR_DESC_INVALID_INVENTORY_STATUS + " Status: "
								+ inventory.get(0).getStatusDesc());

			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVENTORY,
					APIKeyConstants.API_KEY_RESERVE_WSB_INVENTORY,
					new APICallerDetail().add(String.valueOf(inventory.get(0).getId())).add(productCode));

			if (response.getCode() == ResponseCode.SUCCESS)
				return String.valueOf(inventory.get(0).getId());
			else
				throw new PaySysException(response.getCode(), response.getMessage());

		} else
			throw new PaySysException(ResponseCode.ERR_CODE_INVENTORY_NOT_FOUND,
					ResponseDesc.ERR_DESC_INVENTORY_NOT_FOUND);

	}

	protected boolean reserveCertificateInventory(List<Long> certInvIds) throws Exception {

		ServiceResponse response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_INVENTORY,
				APIKeyConstants.API_KEY_RESERVE_CERT_INVENTORY, new APICallerDetail().setObj(certInvIds));

		if (response.getCode() == ResponseCode.SUCCESS) {
			return true;
		} else
			throw new PaySysException(ResponseCode.ERR_CODE_CERT_ALREADY_RESERVED,
					ResponseDesc.ERR_DESC_CERT_ALREADY_RESERVED);
	}

	protected boolean discardCertificate(List<Long> invId) throws Exception {

		List<InventoryRequestDTO> inventoryRequestDTOs = new ArrayList<>();

		invId.forEach(inv -> {
			InventoryRequestDTO inventoryRequestDTO = new InventoryRequestDTO();
			inventoryRequestDTO.setId(inv);
			inventoryRequestDTO.setStatusCode(InventoryStatuses.DISCARD.toString());
			inventoryRequestDTO.setStatusDesc("Discard");
			inventoryRequestDTOs.add(inventoryRequestDTO);
		});

		ServiceResponse response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_INVENTORY,
				APIKeyConstants.API_KEY_UPDATE_STATUS_BY_ID,
				new APICallerDetail().setObj(Collections.singleton(inventoryRequestDTOs)));

		if (response.getCode() == ResponseCode.SUCCESS)
			return true;
		else
			throw new PaySysException(ResponseCode.ERR_CODE_CERT_NOT_FOUND, ResponseDesc.ERR_DESC_CERT_NOT_FOUND);

	}

	protected RegistrationRequestDetailDTO getRequestDetailsByKey(RegistrationRequestDTO request,
			BPMTransactionKey transactionKey) {
		logger.debug("Function called: getRequestDetailsByKey");

		return request.getRegistrationRequestDetailDTO().stream()
				.filter(predicate -> predicate.getTransactionKey().equalsIgnoreCase(transactionKey.toString()))
				.findFirst().orElse(null);
	}

	protected BigDecimal validateCustomer(CustomerDTO customer, ProductResponseDTO productResponse,
			RegistrationRequestDTO registrationRequestDTO, BPMTransactionKey bpmTransactionKey)
			throws PaySysException, Exception {

		String customerCode;
		BigDecimal dAmount = BigDecimal.ZERO;
		boolean isJointRequest = this.isJointReq(registrationRequestDTO.getAdditionalInfo().get("customerType"));
		double requestedAmount = Double.parseDouble(registrationRequestDTO.getAmount());

		if (productResponse.getProductCategory().getCategoryCode()
				.equalsIgnoreCase(ProductCategory.CERTIFICATE.toString()) && isJointRequest) {
			requestedAmount /= 2;
		}

		if (customer != null) {

			customerCode = customer.getCustomerCode();

			if (productResponse.getProductCategory().getCategoryCode()
					.equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())) {

				List<CustomerCertificateResponseDTO> customerCertificateResponseDTO = this
						.getCertificateByCustomerCode(customerCode, registrationRequestDTO.getProductCode());

				if (customerCertificateResponseDTO != null) {
					dAmount = this.getCertificateAmount(customerCertificateResponseDTO, CertificateEnum.NORMAL);
				}

			} else if (productResponse.getProductCategory().getCategoryCode()
					.equalsIgnoreCase(ProductCategory.ACCOUNT.toString())
					|| productResponse.getProductCategory().getCategoryCode()
							.equalsIgnoreCase(ProductCategory.TERM_DEPOSIT_ACCOUNT.toString())) {

				if (registrationRequestDTO.getProductCode().equalsIgnoreCase(Products.PBA.toString())
						|| !isJointRequest) {

					if (!bpmTransactionKey.toString().equals(BPMTransactionKey.JOINT_REQUEST.toString())) {

						Double accountCountBranch = this.getAccountCountByCustomerCode(
								registrationRequestDTO.getUserTypeKey(), registrationRequestDTO.getProductCode(),
								customerCode, registrationRequestDTO.getAdditionalInfo().get("customerType"));

						Double accountCountOrg = this.getOrgCountByCustomerCode(registrationRequestDTO.getProductCode(),
								customerCode);

						if (!validateAccountOpeningLimit(productResponse, accountCountBranch, accountCountOrg)) {
							throw new PaySysException(ResponseCode.FAILURE,
									ResponseDesc.ERR_DESC_UNABLE_TO_OPEN_MORE_ACCOUNTS_FOR_IDEN
											+ customer.getCustIdentityValue());
						}

					}
				}
			}
		}

		if (!this.validateAmountByProduct(productResponse, dAmount = dAmount.add(new BigDecimal(requestedAmount)))) {
			throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CUSTOMER_LIMIT,
					ResponseDesc.ERR_DESC_INVALID_CUSTOMER_LIMIT);
		}

		if (!productResponse.getProductCategory().getCategoryCode().equalsIgnoreCase(ProductCategory.ACCOUNT.toString())
				&& !this.isDivisibleOfMinInvestment(productResponse.getMultipleOff(),
						new BigDecimal(registrationRequestDTO.getAmount()))) {
			throw new PaySysException(ResponseCode.ERR_CODE_INVALID_AMOUNT, ResponseDesc.ERR_DESC_INVALID_AMOUNT);
		}

		return dAmount;
	}

	protected boolean validateTransferInLimits(RegistrationRequestDTO request) throws Exception {
		RegistrationRequestDetailDTO c2cDetails = getRequestDetailsByKey(request,
				BPMTransactionKey.C2C_TRANSFER_REQUEST);
		ProductResponseDTO productResponse = this.getProduct(request.getProductCode());
		CustomerDTO primaryCustomer = null;
		CustomerDTO jointCustomer = null;
		CustomerResponseDTO tempCustomer = null;
		List<CustomerResponseDTO> customers = null;
		List<AuditLogResponseDTO> auditLog = null;
		BigDecimal primaryInvestAmount = BigDecimal.ZERO;
		BigDecimal jointInvestAmount = BigDecimal.ZERO;
		boolean isValid = true;

		try {

			auditLog = this.getRequestFromAuditLog(request);

		} catch (Exception e) {
			logger.info("C2CTransferIn: Request not found in audit log. Error: " + e.getMessage());
		}

		if (auditLog != null) {

			AuditLogResponseDTO rejectedRequest = auditLog.stream().filter(
					log -> log.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_APPROVE.toString())
							&& log.getIsCompleted().equalsIgnoreCase(Constants.YES))
					.findFirst().orElse(null);

			if (rejectedRequest != null)
				throw new PaySysException(ResponseCode.ERR_CODE_REQUEST_ALREADY_EXISTS,
						ResponseDesc.ERR_DESC_REQUEST_ALREADY_EXITS);

		}

		try {

			// Check if registration code or account number already exists
			customers = this.getCustomerByIdentificationKey(request,
					CustomerIdentityKey.fromString(request.getCustIdentityKey()));

		} catch (Exception e) {
			logger.info("C2CTransferIn: Registration or account does not exist. Error: " + e.getMessage());
		}

		if (customers != null)
			throw new PaySysException(ResponseCode.ERR_CODE_PRODUCT_IDENTIFICATION_EXISTS,
					ResponseDesc.ERR_DESC_PRODUCT_IDENTIFICATION_EXISTS);

		primaryCustomer = new MappingUtility<CustomerDTO>(false).JsonToDto(c2cDetails.getContent(), CustomerDTO.class);
		try {

			// Check if customer already exists
			tempCustomer = this.getCustomerByKycIdentValue(primaryCustomer.getCustIdentityValue(),
					request.getUserTypeKey());

		} catch (Exception e) {
			logger.info("C2CTransferIn: Primary customer does not exists Error: " + e.getMessage());
		}

		// Validate primary customer
		if (tempCustomer != null) {
			primaryCustomer.setCustomerCode(tempCustomer.getCode());
		}

		primaryInvestAmount = this.validateCustomer(primaryCustomer, productResponse, request,
				BPMTransactionKey.C2C_TRANSFER_REQUEST);

		if (this.isJointReq(request.getAdditionalInfo().get("customerType"))) {
			RegistrationRequestDetailDTO jointDetails = getRequestDetailsByKey(request,
					BPMTransactionKey.JOINT_REQUEST);

			jointCustomer = new MappingUtility<CustomerDTO>(false).JsonToDto(jointDetails.getContent(),
					CustomerDTO.class);

			try {

				// Check if customer already exists
				tempCustomer = this.getCustomerByKycIdentValue(jointCustomer.getCustIdentityValue(),
						request.getUserTypeKey());

			} catch (Exception e) {
				logger.info("C2CTransferIn: Joint customer does not exists Error: " + e.getMessage());
			}

			// Validate joint customer
			if (tempCustomer != null) {
				jointCustomer.setCustomerCode(tempCustomer.getCode());
			}

			jointInvestAmount = this.validateCustomer(jointCustomer, productResponse, request,
					BPMTransactionKey.JOINT_REQUEST);

			if (productResponse.getProductCategory().getCategoryCode()
					.equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())) {
				// Product is Certificate
				if (!this.validateAmountForJointByProduct(productResponse,
						primaryInvestAmount.add(jointInvestAmount))) {
					throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CUSTOMER_LIMIT,
							ResponseDesc.ERR_DESC_INVALID_CUSTOMER_LIMIT);
				}

			} else {

				// Product is Account or TDA
				Double accountCount = this.getAccountCountByCustomerCodesForJoint(request.getUserTypeKey(),
						request.getProductCode(), primaryCustomer.getCustomerCode(), jointCustomer.getCustomerCode());

				if (!validateAccountOpeningLimit(productResponse, accountCount, null)) {
					throw new PaySysException(ResponseCode.FAILURE, ResponseDesc.ERR_DESC_UNABLE_TO_OPEN_MORE_ACCOUNTS);
				}

			}

		}

		return isValid;
	}

	protected CustomerResponseDTO getCustomerByKycIdentValue(String identifier, String branchCode) throws Exception {
		logger.debug("Function called: getCustomerByKycIdentValue");

		ServiceResponse response = null;

		apiCaller = (APICaller) context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
		response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER,
				APIKeyConstants.API_KEY_GET_BY_IDENTIFIER,
				new APICallerDetail().add(identifier.replaceAll("-", "")).add(branchCode));

		if (response != null && response.getCode() == ResponseCode.SUCCESS) {

			return (CustomerResponseDTO) new MappingUtility<>(false).JsonToDto(response.getData(),
					CustomerResponseDTO.class);

		} else {
			throw new PaySysException(ResponseCode.ERR_CODE_CUST_NOT_FOUND, ResponseDesc.ERR_DESC_CUST_NOT_FOUND);
		}
	}

	protected boolean isValidStatusEncash(String statusCode) {

		switch (StatusCodes.fromString(statusCode)) {

		case New:
		case Normal:
		case Lasped:
		case Pledge:
		case Freeze:
		case Dormant:
		case Initiated:
		case Lost:
		case MigrationBlocked:
		case Encashed:
		case PartEncashed:
		case Block:
			return true;
		}

		return false;
	}

	protected boolean isValidStatus(String statusCode) {

		switch (StatusCodes.fromString(statusCode)) {

		case New:
		case Normal:
		case Lasped:
		case Pledge:
		case Freeze:
		case Dormant:
		case Initiated:
		case Lost:
		case MigrationBlocked:
			return true;
		}

		return false;
	}

}
