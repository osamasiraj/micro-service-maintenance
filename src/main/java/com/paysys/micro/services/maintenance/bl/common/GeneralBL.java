package com.paysys.micro.services.maintenance.bl.common;

import com.couchbase.client.deps.com.fasterxml.jackson.core.type.TypeReference;
import com.couchbase.client.deps.com.fasterxml.jackson.databind.ObjectMapper;
import com.couchbase.client.deps.io.netty.util.internal.StringUtil;
import com.paysys.entity.common.CustomerDTO;
import com.paysys.entity.common.PageTableDTO;
import com.paysys.entity.common.RegistrationRequestDTO;
import com.paysys.entity.common.RegistrationRequestDetailDTO;
import com.paysys.requests.customer.CustomerAccountUnitRequestDTO;
import com.paysys.requests.customer.CustomerAssociateDTO;
import com.paysys.requests.customer.CustomerRegistrationAmountRequestDTO;
import com.paysys.requests.inventory.CertificateInventoryDTO;
import com.paysys.requests.product.AccountRequestDTO;
import com.paysys.requests.transaction.TransactionRequestDTO;
import com.paysys.responses.config.DocumentTypeResponseDTO;
import com.paysys.responses.config.RestrictionResponseDTO;
import com.paysys.responses.customer.*;
import com.paysys.responses.entity.BranchResponseDTO;
import com.paysys.responses.entity.RegionResponseDTO;
import com.paysys.responses.entity.TreasuryResponseDTO;
import com.paysys.responses.inventory.InventoryResponseDTO;
import com.paysys.responses.maintenance.BasicInfoResponseDTO;
import com.paysys.responses.product.*;
import com.paysys.responses.profit.CustomerProfitPaymentResponseDTO;
import com.paysys.responses.transaction.TransactionLogResponseDTO;
import com.paysys.service.common.APIKeyConstants;
import com.paysys.service.common.Constants;
import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.common.enm.*;
import com.paysys.service.exception.PaySysException;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import com.paysys.service.response.ServiceResponse;
import com.paysys.service.utility.APICaller;
import com.paysys.service.utility.APICallerDetail;
import com.paysys.service.utility.HelperFunctions;
import com.paysys.service.utility.MappingUtility;
import org.apache.log4j.Logger;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings({ "unchecked", "unused" })
public class GeneralBL extends ValidationBL {

	private final static Logger logger = Logger.getLogger(GeneralBL.class);

	protected static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}

	public List<KycAttributeResponseDTO> mapKyc(List<CustomerKycResponseDTO> customerkycSet,
			List<KycAttributeResponseDTO> productKycSet) {
		logger.debug("Function called: mapKyc");

		try {

			List<KycAttributeResponseDTO> uniqueKyc = productKycSet.stream()
					.filter(distinctByKey(KycAttributeResponseDTO::getAttributeCode)).collect(Collectors.toList());

			customerkycSet.forEach(customer -> {
				uniqueKyc.forEach(kycAttribute -> {
					if (customer.getAttributeCode().equalsIgnoreCase(kycAttribute.getAttributeCode())) {
						kycAttribute.setAttributeValue(customer.getAttributeValue());
					}
				});
			});

			if (customerkycSet.size() > 0) {
				uniqueKyc.forEach(kycAttribute -> {
					if (kycAttribute.getIsMandatory().equalsIgnoreCase(Constants.YES)
							&& (kycAttribute.getAttributeValue() == null
									|| kycAttribute.getAttributeValue().equalsIgnoreCase("-1")))
						kycAttribute.setIsEditable(Constants.YES);
					else
						kycAttribute.setIsEditable(Constants.NO);
				});
			}

			return uniqueKyc;

		} catch (Exception ex) {
			throw ex;
		}
	}

	public List<CustomerDocumentResponseDTO> mapCustomerDocs(List<CustomerDocumentResponseDTO> custDocs,
			List<CustomerDocumentResponseDTO> productDocs) {
		logger.debug("Function called: mapCustomerDocs");

		try {

			List<CustomerDocumentResponseDTO> uniqueDocs = productDocs.stream()
					.filter(distinctByKey(doc -> doc.getDocumentType().getCode())).collect(Collectors.toList());

			if (custDocs != null) {
				custDocs.forEach(custDoc -> {
					uniqueDocs.forEach(uniqueDoc -> {
						if (custDoc.getDocumentType().getCode()
								.equalsIgnoreCase(uniqueDoc.getDocumentType().getCode())) {
							// NO TIME FOR SUTHRA KAAM
							uniqueDoc.setId(custDoc.getId());
							uniqueDoc.setCreatedDate(custDoc.getCreatedDate());
							uniqueDoc.setNature(custDoc.getNature());
							uniqueDoc.setActive(custDoc.getActive());
							uniqueDoc.setDocumentType(custDoc.getDocumentType());
							uniqueDoc.setReceiveDate(custDoc.getReceiveDate());
							uniqueDoc.setRegistrationCode(custDoc.getRegistrationCode());
							uniqueDoc.setDocumentRefNo(custDoc.getDocumentRefNo());
							uniqueDoc.setCustomerDocumentAttachment(custDoc.getCustomerDocumentAttachment());
						}
					});
				});
			}
			return uniqueDocs;

		} catch (Exception ex) {
			throw ex;
		}
	}

	protected List<KycAttributeResponseDTO> disableKycs(List<KycAttributeResponseDTO> kycList) {
		kycList.forEach(kycAttribute -> {
			if (kycAttribute.getIsMandatory().equalsIgnoreCase(Constants.YES)
					&& (kycAttribute.getAttributeValue() == null
							|| kycAttribute.getAttributeValue().equalsIgnoreCase("-1")))
				kycAttribute.setIsEditable(Constants.YES);
			else
				kycAttribute.setIsEditable(Constants.NO);
		});
		return kycList;
	}

	protected List<CustomerDocumentResponseDTO> getCustomerDocumentsByCode(String customerCode, String docCode)
			throws Exception {
		logger.debug("Function called: getCustomerDocumentsByCode");
		ServiceResponse response;

		AttachmentDTO attachmentDTO = new AttachmentDTO();
		attachmentDTO.setCustomerCode(customerCode);
		attachmentDTO.setDocumentCodes(Collections.singletonList(docCode));

		response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_CUSTOMER_DOCUMENT,
				APIKeyConstants.API_KEY_GET_BY_CUST_AND_DOC_CODE, new APICallerDetail().setObj(attachmentDTO));

		if (response.getCode() == ResponseCode.SUCCESS) {

			return new MappingUtility<List<CustomerDocumentResponseDTO>>(false).JsonToDto(response.getData(),
					new TypeToken<List<CustomerDocumentResponseDTO>>() {
					}.getType());

		} else
			throw new PaySysException(ResponseCode.FAILURE, ResponseDesc.DATA_NOT_FOUND);

	}

	protected List<CustomerDocumentResponseDTO> getCustomerDocumentsByIdentifier(RegistrationRequestDTO request,
			String docCode) throws Exception {
		logger.debug("Function called: getCustomerDocumentsByIdentifier");
		ServiceResponse response = null;
		String apiKey = null;
		APICallerDetail params;

		switch (CustomerIdentityKey.fromString(request.getCustIdentityKey())) {

		case REGISTRATION_CODE:
			if (docCode != null) {
				apiKey = APIKeyConstants.API_KEY_GET_BY_REG_CODE_AND_DOC_CODE;
				params = new APICallerDetail().add(request.getCustIdentityValue()).add(docCode);
			} else {
				apiKey = APIKeyConstants.API_KEY_GET_BY_CERT_REG_CODE;
				params = new APICallerDetail().add(request.getCustIdentityValue());
			}
			break;
		case ACCOUNT_NUMBER:
			if (docCode != null) {
				apiKey = APIKeyConstants.API_KEY_GET_BY_ACC_NUM_AND_DOC_CODE;
				params = new APICallerDetail().add(request.getCustIdentityValue()).add(docCode);
			} else {
				apiKey = APIKeyConstants.API_KEY_GET_BY_ACCOUNT_NUMBER;
				params = new APICallerDetail().add(request.getCustIdentityValue());
			}
			break;
		default:
			throw new PaySysException(ResponseCode.ERR_CODE_INVAID_CUST_IDENT_KEY,
					ResponseDesc.ERR_DESC_INVALID_CUST_IDENT_KEY);
		}

		response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_DOCUMENT, apiKey, params);

		if (response != null && response.getCode() == ResponseCode.SUCCESS) {

			return new MappingUtility<List<CustomerDocumentResponseDTO>>(false).JsonToDto(response.getData(),
					new TypeToken<List<CustomerDocumentResponseDTO>>() {
					}.getType());

		} else
			return null;
	}

	protected List<CustomerDocumentResponseDTO> getDocumentByCustomerCode(String customerCode) throws Exception {
		logger.debug("Function called: getDocumentByCustomerCode");

		ServiceResponse response;

		response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_DOCUMENT,
				APIKeyConstants.API_KEY_GET_BY_SCOPE_AND_TYPE,
				new APICallerDetail().add(Constants.GLOBAL).add(Constants.DOC_TYPE_ISSUEABLE));

		if (response.getCode() == ResponseCode.SUCCESS) {

			List<DocumentTypeResponseDTO> documents = new MappingUtility<List<DocumentTypeResponseDTO>>()
					.JsonToDto(response.getData(), new TypeToken<List<DocumentTypeResponseDTO>>() {
					}.getType());

			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_DOCUMENT,
					APIKeyConstants.API_KEY_GET_BY_CUST_TYPE_AND_SCOPE,
					new APICallerDetail().add(customerCode).add(Constants.DOC_TYPE_ISSUEABLE).add(Constants.GLOBAL));

			List<CustomerDocumentResponseDTO> customerDocumentResponseDTOs = new ArrayList<>();

			if (response.getCode() == ResponseCode.SUCCESS) {
				customerDocumentResponseDTOs = new MappingUtility<List<CustomerDocumentResponseDTO>>()
						.JsonToDto(response.getData(), new TypeToken<List<CustomerDocumentResponseDTO>>() {
						}.getType());
			}

			boolean exists = false;
			for (DocumentTypeResponseDTO document : documents) {
				for (CustomerDocumentResponseDTO custDoc : customerDocumentResponseDTOs) {
					if (custDoc.getDocumentType().getCode().equalsIgnoreCase(document.getCode()))
						exists = true;
				}
				if (!exists) {
					CustomerDocumentResponseDTO custDoc = new CustomerDocumentResponseDTO();
					custDoc.setCustomerCode(customerCode);
					custDoc.setDocumentType(document);
					custDoc.setCustomerDocumentAttachment(new CustomerDocAttachmentResponseDTO());
					customerDocumentResponseDTOs.add(custDoc);
				}
			}
			return customerDocumentResponseDTOs;

		} else
			throw new PaySysException(ResponseCode.FAILURE, ResponseDesc.DATA_NOT_FOUND);

	}

	protected List<CustomerDocumentResponseDTO> getCustomersExistingDocs(String customerCode) throws Exception {
		logger.debug("Function called: getCustomersExistingDocs");

		ServiceResponse response;

		response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_DOCUMENT,
				APIKeyConstants.API_KEY_GET_BY_CUST_CODE, new APICallerDetail().add(customerCode));

		List<CustomerDocumentResponseDTO> customerDocumentResponseDTOs = new ArrayList<>();

		if (response.getCode() == ResponseCode.SUCCESS) {
			customerDocumentResponseDTOs = new MappingUtility<List<CustomerDocumentResponseDTO>>(false)
					.JsonToDto(response.getData(), new TypeToken<List<CustomerDocumentResponseDTO>>() {
					}.getType());
		}

		return customerDocumentResponseDTOs;

	}

	protected List<ProductMaintenanceResponseDTO> mapAccountsToProdMaintenace(List<AccountResponseDTO> accounts,
			RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: mapAccountsToProdMaintenace");

		List<ProductMaintenanceResponseDTO> accountMaintenanceList = new ArrayList<>();
		ProductResponseDTO productInfo;

		for (AccountResponseDTO account : accounts) {

			if (!isValidStatus(account.getAccountStatus().getCode())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.nomineePayment.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.lapsedRevival.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.singleToJoint.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.jointToSingle.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.changeNominee.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.changeCertificateStatus.toString()))
				continue;

			// Get Product Info
			productInfo = this.getProduct(account.getProductCode());

			// Get Customer Account Rel
			CustomerAccountRelResponseDTO accountRel = account.getCustomerAccountRels().stream()
					.filter(rel -> rel.getProduct().getCode().equals(account.getProductCode())).findFirst()
					.orElse(null);

			// Map Product Info
			ProductMaintenanceResponseDTO productMaintenance = this.mapProductInfoToProdMaintenance(productInfo,
					request);
			// Map Account
			productMaintenance.setAvaliableBalance(account.getAvaliableBalance());
			productMaintenance.setAccountTitle(account.getAccountTitle());
			productMaintenance.setAccountNumber(account.getAccountNumber());
			productMaintenance.setLinkedAccount(account.getLinkedAccount());
			productMaintenance.setOperatingCode(account.getOperationMode().getCode());
			productMaintenance.setStatus(account.getAccountStatus().getDescription());
			productMaintenance.setStatusCode(account.getAccountStatus().getCode());

			logger.info("account " + account.getBranch());
			productMaintenance.setBranchCode(account.getBranch());
			// Map Customer Account Rel
			if (accountRel != null) {
				productMaintenance.setInvestorTypeCode(accountRel.getInvestorType().getCode());
				productMaintenance.setCorelatedAccountId(String.valueOf(accountRel.getCorrelationAccountId()));
				productMaintenance.setCustomerType(account.getType());
			}
			accountMaintenanceList.add(productMaintenance);

		}

		return accountMaintenanceList;
	}

	protected List<ProductMaintenanceResponseDTO> mapCertsToProdMaintenance(List<CustomerCertificateResponseDTO> certs,
			RegistrationRequestDTO request) throws Exception {

		logger.debug("Function called: mapCertsToProdMaintenance");

		HashMap<String, ProductMaintenanceResponseDTO> uniqueCertificates = new HashMap<>();
		ProductResponseDTO productInfo;

		for (CustomerCertificateResponseDTO cert : certs) {

			if (!isValidStatus(cert.getStatusCode())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.nomineePayment.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.lapsedRevival.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.changeNominee.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.changeCertificateStatus.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.singleToJoint.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.jointToSingle.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.PCBReIssue.toString())
					&& !request.getRequestType().equalsIgnoreCase(BPMProcess.certificateReIssue.toString()))
				continue;

			if (uniqueCertificates.containsKey(cert.getRegistrationCode())) {

				ProductMaintenanceResponseDTO existingCert = uniqueCertificates.get(cert.getRegistrationCode());
				// existingCert.setAvaliableBalance(existingCert.getAvaliableBalance().add(cert.getAvailableBalance()));
				if (cert.getStatusCode().equalsIgnoreCase(StatusCodes.Pledge.toString()))
					existingCert.setHasPledged(true);

				uniqueCertificates.put(cert.getRegistrationCode(), existingCert);

			} else {

				// Get Product Info
				productInfo = this.getProduct(cert.getProductCode());
				// Map Product Info
				ProductMaintenanceResponseDTO productMaintenance = this.mapProductInfoToProdMaintenance(productInfo,
						request);
				// Map Certificate
				productMaintenance
						.setAvaliableBalance(new BigDecimal(this.getTotalAmountByRegCode(cert.getRegistrationCode())));
				productMaintenance.setRegistrationCode(cert.getRegistrationCode());
				productMaintenance.setAccountTitle(cert.getTitle());
				productMaintenance.setOperatingCode(cert.getType());
				productMaintenance.setStatus(cert.getStatus());
				productMaintenance.setStatusCode(cert.getStatusCode());
				if (cert.getCustomerAccountRels() != null && !cert.getCustomerAccountRels().isEmpty()) {
					productMaintenance
							.setInvestorTypeCode(cert.getCustomerAccountRels().get(0).getInvestorType().getCode());
					productMaintenance.setCorelatedAccountId(
							String.valueOf(cert.getCustomerAccountRels().get(0).getCorrelationAccountId()));
					productMaintenance.setCustomerType(cert.getType());
				}
				uniqueCertificates.put(cert.getRegistrationCode(), productMaintenance);
			}

		}

		return new ArrayList<>(uniqueCertificates.values());
	}

	private ProductMaintenanceResponseDTO mapProductInfoToProdMaintenance(ProductResponseDTO productInfo,
			RegistrationRequestDTO request) {
		logger.debug("Function called: mapProductInfoToProdMaintenance");

		ProductMaintenanceResponseDTO productMaintenance = new ProductMaintenanceResponseDTO();
		// Map Product Info
		productMaintenance.setCode(productInfo.getCode());
		productMaintenance.setCategoryCode(productInfo.getProductCategory().getCategoryCode());
		productMaintenance.setName(productInfo.getName());
		productMaintenance.setDescription(productInfo.getProductCategory().getDescription());
		productMaintenance.setActive(productInfo.getActive());
		productMaintenance.setAllowCorrelation(productInfo.getAllowCorrelation());

		return productMaintenance;
	}

	protected CustomerDTO getReInvestmentCustomerProduct(RegistrationRequestDTO requestDTO) throws Exception {
		logger.debug("Function called: getReInvestmentCustomerProduct");

		ProductMaintenanceResponseDTO productMaintenance = new ProductMaintenanceResponseDTO();
		List<ProductMaintenanceResponseDTO> custProducts = null;
		List<CustomerDTO> customerBasicInfo;
		ProductResponseDTO product;
		ServiceResponse response;
		CustomerResponseDTO customer;

		if (requestDTO != null) {
			switch (CustomerIdentityKey.fromString(requestDTO.getCustIdentityKey().toUpperCase())) {
			case ACCOUNT_NUMBER:

				List<CustomerResponseDTO> customers = this.getCustomerByIdentificationKey(requestDTO,
						CustomerIdentityKey.ACCOUNT_NUMBER);

				customerBasicInfo = this.getCustomerWithBasicInfo(customers);

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT,
						APIKeyConstants.API_KEY_GET_REINVESTMENT_ACC_BY_ACC_NUM,
						new APICallerDetail().add(requestDTO.getCustIdentityValue()));

				if (response.getCode() == ResponseCode.SUCCESS) {

					AccountResponseDTO custAccount = new MappingUtility<AccountResponseDTO>(false)
							.JsonToDto(response.getData(), AccountResponseDTO.class);

					boolean isReinvestable = false;

					for (CustomerAccountUnitRequestDTO unit : custAccount.getCustomerAccountUnits()) {
						if (unit.getEffectiveDate().before(
								new SimpleDateFormat("dd-MM-yyyy").parse(Constants.REINVESTMENT_ALLLOWED_BEFORE_TDA))) {
							isReinvestable = true;
						}
					}

					if (!isReinvestable)
						throw new PaySysException(ResponseCode.ERR_CODE_REINVESTMENT_NOT_ALLOWED_TDA,
								ResponseDesc.ERR_DESC_REINVESTMENT_NOT_ALLOWED_TDA);

					if (custAccount.getAccountStatus().getCode().equalsIgnoreCase(StatusCodes.Lasped.toString())) {

						throw new PaySysException(ResponseCode.ERR_CODE_LAPSED_PROD_REINVESTMENT,
								ResponseDesc.ERR_DESC_LAPSED_PROD_REINVESTMENT);

					}

					custProducts = this.mapAccountsToProdMaintenace(Collections.singletonList(custAccount), requestDTO);

				} else
					throw new PaySysException(response.getCode(), response.getMessage());

				break;

			case REGISTRATION_CODE:

				customers = this.getCustomerByIdentificationKey(requestDTO, CustomerIdentityKey.REGISTRATION_CODE);

				customerBasicInfo = this.getCustomerWithBasicInfo(customers);

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
						APIKeyConstants.API_KEY_GET_REINVESTMENT_CERT_BY_REG_CODE,
						new APICallerDetail().add(requestDTO.getCustIdentityValue()));
				if (response.getCode() == ResponseCode.SUCCESS) { // 15 Nov 2010

					List<CustomerCertificateResponseDTO> custCerts = new MappingUtility<List<CustomerCertificateResponseDTO>>(
							false).JsonToDto(response.getData(), new TypeToken<List<CustomerCertificateResponseDTO>>() {
							}.getType());

					if (!custCerts.get(0).getProductCode().equalsIgnoreCase(Products.BSC.toString())
							&& !custCerts.get(0).getProductCode().equalsIgnoreCase(Products.DSC.toString())
							&& !custCerts.get(0).getProductCode().equalsIgnoreCase(Products.SSCR.toString())
							&& !custCerts.get(0).getProductCode().equalsIgnoreCase(Products.RIC.toString()))
						throw new PaySysException(ResponseCode.ERR_CODE_REINVESTMENT_NOT_ALLOWED,
								ResponseDesc.ERR_DESC_REINVESTMENT_NOT_ALLOWED);

					if (custCerts.get(0).getEffectiveDate().after(
							new SimpleDateFormat("dd-MM-yyyy").parse(Constants.REINVESTMENT_ALLLOWED_BEFORE_CERT))) {

						throw new PaySysException(ResponseCode.ERR_CODE_REINVESTMENT_NOT_ALLOWED_CERT,
								ResponseDesc.ERR_DESC_REINVESTMENT_NOT_ALLOWED_CERT);

					} else if (custCerts.get(0).getStatusCode().equalsIgnoreCase(StatusCodes.Lasped.toString())) {

						throw new PaySysException(ResponseCode.ERR_CODE_LAPSED_PROD_REINVESTMENT,
								ResponseDesc.ERR_DESC_LAPSED_PROD_REINVESTMENT);

					}

					custProducts = this.mapCertsToProdMaintenance(custCerts, requestDTO);

				} else
					throw new PaySysException(response.getCode(), response.getMessage());

				break;

			case DEFAULT:
			default:

				custProducts = new ArrayList<>();

				customer = this.getCustomerByIdentifier(requestDTO, CustomerIdentityKey.IDENTIFIER);

				customerBasicInfo = this.getCustomerWithBasicInfo(Collections.singletonList(customer));

				try {
					if (requestDTO.getCategory() == null
							|| requestDTO.getCategory().equalsIgnoreCase(ProductCategory.ACCOUNT.toString())) {

						response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT,
								APIKeyConstants.API_KEY_GET_REINVESTMENT_ACC_BY_CUST_IDENT,
								new APICallerDetail().add(requestDTO.getCustIdentityValue()));

						List<AccountResponseDTO> customerAccounts = new MappingUtility<List<AccountResponseDTO>>()
								.JsonToDto(response.getData(), new TypeToken<List<AccountResponseDTO>>() {
								}.getType());

						custProducts.addAll(this.mapAccountsToProdMaintenace(customerAccounts, requestDTO));

					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {

					if (requestDTO.getCategory() == null
							|| requestDTO.getCategory().equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())) {

						response = apiCaller.execute(RequestType.GET,
								ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
								APIKeyConstants.API_KEY_GET_REINVESTMENT_CERT_BY_CUST_IDENT,
								new APICallerDetail().add(requestDTO.getCustIdentityValue()));

						List<CustomerCertificateResponseDTO> customerCertificates = new MappingUtility<List<CustomerCertificateResponseDTO>>(
								false).JsonToDto(response.getData(),
										new TypeToken<List<CustomerCertificateResponseDTO>>() {
										}.getType());

						custProducts.addAll(this.mapCertsToProdMaintenance(customerCertificates, requestDTO));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				break;

			}
			customerBasicInfo.get(0).setProducts(custProducts);

			return validateSearchResult(customerBasicInfo.get(0), requestDTO);

		} else
			throw new PaySysException(ResponseCode.ERR_CODE_NO_PRODUCT_FOR_REINVESTMENT,
					ResponseDesc.ERR_DESC_NO_PRODUCT_FOR_REINVESTMENT);
	}

	protected RegistrationRequestDTO getLapsedDetails(RegistrationRequestDTO request) throws Exception {
		ServiceResponse response;

		APICaller tranEngine = (APICaller) context.getBean(APICaller.class, ServiceNames.TRANSACTION_ENGINE);
		response = tranEngine.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_TRAN_ENGINE,
				APIKeyConstants.API_KEY_INQUIRE, new APICallerDetail().setObj(request));

		List<LapsedInquiryResponseDTO> lapsedInquiryResponse = new MappingUtility<List<LapsedInquiryResponseDTO>>(false)
				.JsonToDto(response.getData(), new TypeToken<List<LapsedInquiryResponseDTO>>() {
				}.getType());

		if (request.getRequestType().equalsIgnoreCase(BPMProcess.lapsedInquiry.toString())) {

			request = updateRegistrationRequest(request, lapsedInquiryResponse, BPMTransactionKey.LAPSED_INQUIRY,
					Constants.ADD);

		} else if (request.getRequestType().equalsIgnoreCase(BPMProcess.lapsedMarking.toString())) {

			// List<ProductLapsedResponseDTO> lapsedProductsList = new ArrayList<>();
			HashMap<String, ProductLapsedResponseDTO> temp = new HashMap<>();
			lapsedInquiryResponse.forEach(lapsedProduct -> {

				if (temp.containsKey(lapsedProduct.getProductName())) {
					ProductLapsedResponseDTO product = temp.get(lapsedProduct.getProductName());
					product.setLapsedCount(product.getLapsedCount().add(BigDecimal.ONE));
					product.setLapsedSum(product.getLapsedSum().add(lapsedProduct.getCurrentBalance()));
				} else {
					ProductLapsedResponseDTO product = new ProductLapsedResponseDTO();
					product.setProductName(lapsedProduct.getProductName());
					product.setProductCode(lapsedProduct.getProductCode());
					product.setLapsedCount(BigDecimal.ONE);
					product.setLapsedSum(lapsedProduct.getCurrentBalance());
					temp.put(lapsedProduct.getProductName(), product);
				}
			});
			// lapsedInquiryResponse.add();
			request = updateRegistrationRequest(request, temp.values(), BPMTransactionKey.LAPSED_MARKING,
					Constants.ADD);
		}
		return request;
	}

	protected CustomerDTO getLapsedCustomerProduct(RegistrationRequestDTO requestDTO) throws Exception {
		logger.debug("Function called: getLapsedCustomerProduct");

		ProductMaintenanceResponseDTO productMaintenance = new ProductMaintenanceResponseDTO();
		List<ProductMaintenanceResponseDTO> custProducts = null;
		List<CustomerDTO> customerBasicInfo;
		ProductResponseDTO product;
		ServiceResponse response;
		CustomerResponseDTO customer;

		if (requestDTO != null) {
			switch (CustomerIdentityKey.fromString(requestDTO.getCustIdentityKey().toUpperCase())) {
			case ACCOUNT_NUMBER:

				List<CustomerResponseDTO> customers = this.getCustomerByIdentificationKey(requestDTO,
						CustomerIdentityKey.ACCOUNT_NUMBER);

				customerBasicInfo = this.getCustomerWithBasicInfo(customers);

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT,
						APIKeyConstants.API_KEY_GET_LAPSED_ACC_BY_ACC_NUM,
						new APICallerDetail().add(requestDTO.getCustIdentityValue()));

				if (response.getCode() == ResponseCode.SUCCESS) {

					AccountResponseDTO custAccount = new MappingUtility<AccountResponseDTO>(false)
							.JsonToDto(response.getData(), AccountResponseDTO.class);

					custProducts = this.mapAccountsToProdMaintenace(Collections.singletonList(custAccount), requestDTO);

				} else
					throw new PaySysException(response.getCode(), response.getMessage());

				break;
			case REGISTRATION_CODE:

				customers = this.getCustomerByIdentificationKey(requestDTO, CustomerIdentityKey.REGISTRATION_CODE);

				customerBasicInfo = this.getCustomerWithBasicInfo(customers);

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
						APIKeyConstants.API_KEY_GET_LAPSED_CERT_BY_REG_CODE,
						new APICallerDetail().add(requestDTO.getCustIdentityValue()));

				if (response.getCode() == ResponseCode.SUCCESS) {

					List<CustomerCertificateResponseDTO> custCerts = new MappingUtility<List<CustomerCertificateResponseDTO>>(
							false).JsonToDto(response.getData(), new TypeToken<List<CustomerCertificateResponseDTO>>() {
							}.getType());

					custProducts = this.mapCertsToProdMaintenance(custCerts, requestDTO);

				} else
					throw new PaySysException(response.getCode(), response.getMessage());

				break;

			case DEFAULT:
			default:

				custProducts = new ArrayList<>();

				customer = this.getCustomerByIdentifier(requestDTO, CustomerIdentityKey.IDENTIFIER);

				customerBasicInfo = this.getCustomerWithBasicInfo(Collections.singletonList(customer));

				try {
					if (requestDTO.getCategory() == null
							|| requestDTO.getCategory().equalsIgnoreCase(ProductCategory.ACCOUNT.toString())) {

						response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT,
								APIKeyConstants.API_KEY_GET_LAPSED_ACC_BY_CUST_IDENT,
								new APICallerDetail().add(requestDTO.getCustIdentityValue().replaceAll("-", "")));

						List<AccountResponseDTO> customerAccounts = new MappingUtility<List<AccountResponseDTO>>(true)
								.JsonToDto(response.getData(), new TypeToken<List<AccountResponseDTO>>() {
								}.getType());

						if (customerAccounts != null) {
							custProducts.addAll(this.mapAccountsToProdMaintenace(customerAccounts, requestDTO));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {

					if (requestDTO.getCategory() == null
							|| requestDTO.getCategory().equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())) {

						response = apiCaller.execute(RequestType.GET,
								ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
								APIKeyConstants.API_KEY_GET_LAPSED_CERT_BY_CUST_IDENT,
								new APICallerDetail().add(requestDTO.getCustIdentityValue().replaceAll("-", "")));

						List<CustomerCertificateResponseDTO> customerCertificates = new MappingUtility<List<CustomerCertificateResponseDTO>>(
								false).JsonToDto(response.getData(),
										new TypeToken<List<CustomerCertificateResponseDTO>>() {
										}.getType());

						if (customerCertificates != null) {
							custProducts.addAll(this.mapCertsToProdMaintenance(customerCertificates, requestDTO));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				break;

			}
			customerBasicInfo.get(0).setProducts(custProducts);

			return validateSearchResult(customerBasicInfo.get(0), requestDTO);

		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);

	}

	protected CustomerDTO getCustomerProduct(RegistrationRequestDTO requestDTO) throws Exception {
		logger.debug("Function called: getCustomerProduct");

		ProductMaintenanceResponseDTO productMaintenance = new ProductMaintenanceResponseDTO();
		List<ProductMaintenanceResponseDTO> custProducts = null;
		List<CustomerDTO> customerBasicInfo;
		ProductResponseDTO product;
		ServiceResponse response;
		CustomerResponseDTO customer;

		if (requestDTO != null) {
			switch (CustomerIdentityKey.fromString(requestDTO.getCustIdentityKey().toUpperCase())) {
			case ACCOUNT_NUMBER:

				List<CustomerResponseDTO> customers = this.getCustomerByIdentificationKey(requestDTO,
						CustomerIdentityKey.ACCOUNT_NUMBER);

				customerBasicInfo = this.getCustomerWithBasicInfo(customers);

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT,
						APIKeyConstants.API_KEY_GET_BY_ACCOUNT_NUMBER,
						new APICallerDetail().add(requestDTO.getUserTypeKey()).add(requestDTO.getCustIdentityValue()));

				if (response.getCode() == ResponseCode.SUCCESS) {

					AccountResponseDTO custAccount = new MappingUtility<AccountResponseDTO>(false)
							.JsonToDto(response.getData(), AccountResponseDTO.class);

					custProducts = this.mapAccountsToProdMaintenace(Collections.singletonList(custAccount), requestDTO);

				} else
					throw new PaySysException(response.getCode(), response.getMessage());

				break;
			case REGISTRATION_CODE:

				customers = this.getCustomerByIdentificationKey(requestDTO, CustomerIdentityKey.REGISTRATION_CODE);

				customerBasicInfo = this.getCustomerWithBasicInfo(customers);

				CustomerCertificateResponseDTO certificate = null;

				if (requestDTO.getRequestType() != null && (requestDTO.getRequestType()
						.equalsIgnoreCase(BPMProcess.changeCertificateStatus.toString())
						|| requestDTO.getRequestType().equalsIgnoreCase(BPMProcess.singleToJoint.toString())
						|| requestDTO.getRequestType().equalsIgnoreCase(BPMProcess.jointToSingle.toString())
						|| requestDTO.getRequestType().equalsIgnoreCase(BPMProcess.PCBReIssue.toString())
						|| requestDTO.getRequestType().equalsIgnoreCase(BPMProcess.certificateReIssue.toString()))) {
					certificate = this.getOneAllCertificateByRegCodeAndBranch(requestDTO.getCustIdentityValue(),
							requestDTO.getUserTypeKey());

				} else {
					certificate = this.getOneCertificateByRegCodeAndBranch(requestDTO.getCustIdentityValue(),
							requestDTO.getUserTypeKey());
				}
				custProducts = this.mapCertsToProdMaintenance(Collections.singletonList(certificate), requestDTO);

				break;

			case DEFAULT:
			default:

				custProducts = new ArrayList<>();

				customer = this.getCustomerByIdentifier(requestDTO, CustomerIdentityKey.IDENTIFIER);

				customerBasicInfo = this.getCustomerWithBasicInfo(Collections.singletonList(customer));

				// Query and map customer certificates
				try {

					if (requestDTO.getCategory() == null
							|| requestDTO.getCategory().equalsIgnoreCase(ProductCategory.CERTIFICATE.toString())) {

						certificate = this.getOneCertificateByRegCodeAndBranch(requestDTO.getCustIdentityValue(),
								requestDTO.getUserTypeKey());

						custProducts.addAll(
								this.mapCertsToProdMaintenance(Collections.singletonList(certificate), requestDTO));
					}

				} catch (Exception e) {
					logger.info(String.format("No certificates found for customer: %s", customer.getCode()));
					e.printStackTrace();
				}

				// Query and map customer accounts
				try {

					if (requestDTO.getCategory() == null
							|| requestDTO.getCategory().equalsIgnoreCase(ProductCategory.ACCOUNT.toString())) {

						List<AccountResponseDTO> customerAccounts = getAccountsByCustCodeAndBranch(customer.getCode(),
								requestDTO.getUserTypeKey());

						custProducts.addAll(this.mapAccountsToProdMaintenace(customerAccounts, requestDTO));

					}

				} catch (Exception e) {
					logger.info(String.format("No accounts found for customer: %s", customer.getCode()));
					e.printStackTrace();
				}

				break;

			}
			customerBasicInfo.get(0).setProducts(custProducts);

			return validateSearchResult(customerBasicInfo.get(0), requestDTO);

		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);

	}

	protected List<AccountResponseDTO> getAccountsByCustCodeAndBranch(String customerCode, String branchCode)
			throws Exception {
		logger.debug("Function called: getAccountsByCustCodeAndBranch");

		APICallerDetail params;
		String apikey;

		if (branchCode != null) {
			apikey = APIKeyConstants.API_KEY_GET_BY_CUST_AND_BRANCH;
			params = new APICallerDetail().add(customerCode).add(branchCode);
		} else {
			apikey = APIKeyConstants.API_KEY_GET_BY_CUST_CODE;
			params = new APICallerDetail().add(customerCode);
		}

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT,
				apikey, params);

		if (response.getCode() == ResponseCode.SUCCESS) {
			return new MappingUtility<List<AccountResponseDTO>>(true).JsonToDto(response.getData(),
					new TypeToken<List<AccountResponseDTO>>() {
					}.getType());
		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
	}

	protected List<AccountResponseDTO> getCorrelatedAccountsByCustCode(String customerCode) throws Exception {

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT,
				APIKeyConstants.API_KEY_GET_CORR_ACC_BY_CUST_CODE, new APICallerDetail().add(customerCode));
		if (response.getCode() == ResponseCode.SUCCESS) {
			return new MappingUtility<List<AccountResponseDTO>>().JsonToDto(response.getData(),
					new TypeToken<List<AccountResponseDTO>>() {
					}.getType());
		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
	}

	protected List<CustomerCertificateResponseDTO> getCertificatesByCustCodeAndBranch(RegistrationRequestDTO request)
			throws Exception {
		logger.debug("Function called: getCertificatesByCustCodeAndBranch");

		APICallerDetail params;
		String apikey;

		PageTableDTO pageTableDTO = this.getPaginationDetails(request);

		if (pageTableDTO.getUserTypeKey() != null & !request.getRequestType().equalsIgnoreCase("crm")) {
			apikey = APIKeyConstants.API_KEY_GET_BY_CUST_AND_BRANCH;
			params = new APICallerDetail().setObj(pageTableDTO);
		} else {
			apikey = APIKeyConstants.API_KEY_GET_BY_CUST_CODE;
			params = new APICallerDetail().setObj(pageTableDTO);
		}

		ServiceResponse response = apiCaller.execute(RequestType.POST,
				ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE, apikey, params);

		if (response.getCode() == ResponseCode.SUCCESS) {
			pageTableDTO = new MappingUtility<PageTableDTO>(false).JsonToDto(response.getData(), PageTableDTO.class);
			request.getAdditionalInfo().put("totalRows", String.valueOf(pageTableDTO.getTotalRows()));
			return (List<CustomerCertificateResponseDTO>) pageTableDTO.getData();
		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
	}

	protected List<CustomerKycResponseDTO> getCustKycByCode(String customerCode) {
		logger.debug("Function called: getCustKycByCode");

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER,
				"getByCustCode", new APICallerDetail().add(customerCode));

		Type listType = new TypeToken<List<CustomerKycResponseDTO>>() {
		}.getType();

		return (List<CustomerKycResponseDTO>) new MappingUtility<>().JsonToDto(response.getData(), listType);
	}

	protected List<ProductResponseDTO> getCustProductsByCode(String customerCode) {
		logger.debug("Function called: getCustProductsByCode");

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_PRODUCT,
				"getByCustCode", new APICallerDetail().add(customerCode));
		Type listType = new TypeToken<List<ProductResponseDTO>>() {
		}.getType();
		return (List<ProductResponseDTO>) new MappingUtility<>(true).JsonToDto(response.getData(), listType);
	}

	protected List<KycAttributeResponseDTO> getCustProdKycByInvstTypes(CustomerResponseDTO customer,
			List<ProductResponseDTO> prod) {
		logger.debug("Function called: getCustProdKycByInvstTypes");
		String str = "";
		List<KycAttributeResponseDTO> finalList = new ArrayList<>();

		if (customer.getCustomerAccountRels() != null) {
			customer.getCustomerAccountRels().forEach(rel -> {
				finalList.addAll(getProductKycSet(rel.getProduct().getCode(), rel.getInvestorType().getCode()));
			});
		} else {
			finalList.addAll(getKycSetByCustomerCode(customer));
		}
		return finalList;
	}

	protected List<KycAttributeResponseDTO> getKycSetByCustomerCode(CustomerResponseDTO customer) {
		logger.debug("Function called: getProductKycSet");

		Type listType = new TypeToken<List<KycAttributeResponseDTO>>() {
		}.getType();

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_KYC_ATTR_LIST,
				APIKeyConstants.API_KEY_GET_BY_CUST_CODE, new APICallerDetail().add(customer.getCode()));

		if (response.getCode() == ResponseCode.SUCCESS) {
			return (List<KycAttributeResponseDTO>) new MappingUtility<>(false).JsonToDto(response.getData(), listType);

		} else
			return new ArrayList<>();
	}

	protected List<KycAttributeResponseDTO> getProductKycSet(String productCode, String invTypeCode) {
		logger.debug("Function called: getProductKycSet");

		Type listType = new TypeToken<List<KycAttributeResponseDTO>>() {
		}.getType();

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_KYC_ATTR_LIST,
				APIKeyConstants.API_KEY_GET_BY_PRODUCT_INVEST_CODE,
				new APICallerDetail().add(productCode).add(invTypeCode));

		if (response.getCode() == ResponseCode.SUCCESS) {
			return (List<KycAttributeResponseDTO>) new MappingUtility<>(false).JsonToDto(response.getData(), listType);

		} else
			return new ArrayList<>();
	}

	protected List<ProductResponseDTO> getAllCustomerProducts(String customerCode) throws Exception {
		logger.debug("Function called: getAllCustomerProducts");

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_PRODUCT,
				APIKeyConstants.API_KEY_GET_BY_CUST_CODE, new APICallerDetail().add(customerCode));

		if (response.getCode() == ResponseCode.SUCCESS) {
			return new MappingUtility<List<ProductResponseDTO>>().JsonToDto(response.getData(),
					new TypeToken<List<ProductResponseDTO>>() {
					}.getType());
		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
	}

	protected List<ProductRestrictionResponseDTO> getProductRestrictions(List<ProductResponseDTO> products)
			throws Exception {
		logger.debug("Function called: getProductRestrictions");

		List<String> productCodes = products.stream().map(ProductResponseDTO::getCode).collect(Collectors.toList());

		ServiceResponse response = apiCaller.execute(RequestType.POST,
				ModuleKeyConstants.MODULE_KEY_PRODUCT_RESTRICTION, APIKeyConstants.API_KEY_GET_BY_PROD_LIST,
				new APICallerDetail().setObj(productCodes));

		if (response.getCode() == ResponseCode.SUCCESS) {

			return new MappingUtility<List<ProductRestrictionResponseDTO>>().JsonToDto(response.getData(),
					new TypeToken<List<ProductRestrictionResponseDTO>>() {
					}.getType());
		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
	}

	protected List<CustomerRestrictionResponseDTO> mapRestrictions(List<ProductRestrictionResponseDTO> prodRes,
			List<CustomerRestrictionResponseDTO> custRes) {
		logger.debug("Function called: mapRestrictions");

		if (custRes == null)
			custRes = new ArrayList<>();

		List<RestrictionResponseDTO> restrictions = prodRes.stream().map(ProductRestrictionResponseDTO::getRestriction)
				.collect(Collectors.toList());

		List<RestrictionResponseDTO> uniqueRes = restrictions.stream()
				.filter(distinctByKey(RestrictionResponseDTO::getCode)).collect(Collectors.toList());

		boolean exists = false;
		for (RestrictionResponseDTO ur : uniqueRes) {
			for (CustomerRestrictionResponseDTO cr : custRes) {
				if (cr.getRestriction().getCode().equalsIgnoreCase(ur.getCode()))
					exists = true;
			}
			if (!exists) {
				CustomerRestrictionResponseDTO cr = new CustomerRestrictionResponseDTO();
				cr.setRestriction(ur);
				custRes.add(cr);
			}
		}

		return custRes;

	}

	protected List<KycAttributeResponseDTO> getCustomerKycByList(String customerCode, List<String> kycAttrList)
			throws Exception {
		logger.debug("Function called: getCustomerKycByList");

		HashMap<String, Object> data = new HashMap<>();
		data.put(CustomerIdentityKey.CUSTOMER_CODE.toString(), customerCode);
		data.put(BPMTransactionKey.CUSTOMER_KYC.toString(), kycAttrList.toArray());

		ServiceResponse response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_CUSTOMER,
				APIKeyConstants.API_KEY_GET_BY_CUST_ATTR_LIST, new APICallerDetail().setObj(data));

		if (response.getCode() == ResponseCode.SUCCESS) {

			return new MappingUtility<List<KycAttributeResponseDTO>>().JsonToDto(response.getData(),
					new TypeToken<List<KycAttributeResponseDTO>>() {
					}.getType());
		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
	}

	protected List<CustomerAssociateDTO> getProductNominees(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getProductNominees");

		ServiceResponse response = null;
		String apiKey = null;
		List<CustomerAssociateDTO> nomineeList = new ArrayList<>();

		switch (CustomerIdentityKey.fromString(request.getCustIdentityKey().toUpperCase())) {
		case ACCOUNT_NUMBER:
			apiKey = APIKeyConstants.API_KEY_GET_BY_ACCOUNT_NUMBER;

			break;
		case REGISTRATION_CODE:
			apiKey = APIKeyConstants.API_KEY_GET_BY_CERT_REG_CODE;

			break;
		}

		response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ASSOCIATE, apiKey,
				new APICallerDetail().add(request.getCustIdentityValue()));

		if (response != null && response.getCode() == ResponseCode.SUCCESS) {

			List<CustomerAssociateResponseDTO> prodNominee = new MappingUtility<List<CustomerAssociateResponseDTO>>(
					false).JsonToDto(response.getData(), new TypeToken<List<CustomerAssociateResponseDTO>>() {
					}.getType());

			prodNominee.forEach(pn -> {
				CustomerAssociateDTO nominee = new CustomerAssociateDTO();
				nominee.setCode(pn.getCode());
				nominee.setCreatedBy(pn.getCreatedBy());
				nominee.setDisplayName(pn.getDisplayName());
				nominee.setInvestorTypeCode(pn.getTypeName());
				nominee.setProfitPaid(pn.getCustomerAssociateKycInfo().getProfitPaid());
				nominee.setNomineeShare(pn.getCustomerAssociateKycInfo().getShare());

				ServiceResponse res = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_KYC_ATTR_LIST,
						APIKeyConstants.API_KEY_GET_BY_PRODUCT_INVEST_CODE,
						new APICallerDetail().add(request.getProductCode()).add(pn.getTypeName()));

				List<KycAttributeResponseDTO> kycAttributes = (List<KycAttributeResponseDTO>) new MappingUtility<>(
						false).JsonToDto(res.getData(), new TypeToken<List<KycAttributeResponseDTO>>() {
						}.getType());

				pn.getCustomerAssociateKycs().forEach(kyc -> {

					KycAttributeResponseDTO kycAttRes = kycAttributes.stream()
							.filter(ka -> ka.getAttributeCode().equalsIgnoreCase(kyc.getAttributeCode())).findFirst()
							.orElse(null);

					if (kycAttRes != null) {

						if (kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_CUST_IDENT_CODE.toString()))
							nominee.setIdentifierKey(kyc.getAttributeValue());
						else if (kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_CUST_IDENT_VALUE.toString()))
							nominee.setIdentifierValue(kyc.getAttributeValue());

						kycAttRes.setAttributeCode(kyc.getAttributeCode());
						kycAttRes.setAttributeValue(kyc.getAttributeValue());
					}

				});

				nominee.setKycAttributeResponseDTOs(kycAttributes);
				nomineeList.add(nominee);
			});

			return nomineeList;

		} else if (response != null && response.getMessage().equalsIgnoreCase(ResponseDesc.DATA_NOT_FOUND)) {
			return nomineeList;
		} else
			return nomineeList;
		// throw new PaySysException(ResponseCode.DATA_NOT_FOUND,
		// ResponseDesc.DATA_NOT_FOUND);
		// throw new PaySysException(response.getCode(), response.getMessage());

	}

	protected List<CustomerAssociateDTO> getCustomerAssociates(String customerCode) throws Exception {
		logger.debug("Function called: getCustomerAssociates");

		ServiceResponse response = null;
		List<CustomerAssociateDTO> nomineeList = new ArrayList<>();

		response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ASSOCIATE,
				APIKeyConstants.API_KEY_GET_BY_CUST_CODE, new APICallerDetail().add(customerCode));

		if (response != null && response.getCode() == ResponseCode.SUCCESS) {

			List<CustomerAssociateResponseDTO> prodNominee = new MappingUtility<List<CustomerAssociateResponseDTO>>(
					false).JsonToDto(response.getData(), new TypeToken<List<CustomerAssociateResponseDTO>>() {
					}.getType());

			prodNominee.forEach(pn -> {
				CustomerAssociateDTO nominee = new CustomerAssociateDTO();
				nominee.setCode(pn.getCode());
				nominee.setCreatedBy(pn.getCreatedBy());
				nominee.setDisplayName(pn.getDisplayName());
				nominee.setInvestorTypeCode(pn.getTypeName());
				nominee.setNomineeShare(pn.getCustomerAssociateKycInfo().getShare());

				pn.getCustomerAssociateKycs().forEach(kyc -> {

					if (kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_CUST_IDENT_CODE.toString()))
						nominee.setIdentifierKey(kyc.getAttributeValue());
					else if (kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_CUST_IDENT_VALUE.toString()))
						nominee.setIdentifierValue(kyc.getAttributeValue());
				});

				nomineeList.add(nominee);
			});

			return nomineeList;

		} else if (response != null && response.getMessage().equalsIgnoreCase(ResponseDesc.DATA_NOT_FOUND)) {
			return nomineeList;
		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);

	}

	protected List<CustomerResponseDTO> getCustomerByProduct(RegistrationRequestDTO requestDTO) throws Exception {
		logger.debug("Function called: getCustomerByProduct");

		ServiceResponse response = null;

		apiCaller = context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
		switch (CustomerIdentityKey.fromString(requestDTO.getCustIdentityKey().toUpperCase())) {
		case ACCOUNT_NUMBER:
			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER,
					APIKeyConstants.API_KEY_GET_BY_ACCOUNT_NUMBER,
					new APICallerDetail().add(requestDTO.getCustIdentityValue()));
			break;
		case REGISTRATION_CODE:
			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER,
					APIKeyConstants.API_KEY_GET_BY_CERT_REG_CODE,
					new APICallerDetail().add(requestDTO.getCustIdentityValue()));
			break;

		case DEFAULT:
		default:
			throw new PaySysException(ResponseCode.ERR_CODE_INVAID_CUST_IDENT_KEY,
					ResponseDesc.ERR_DESC_INVALID_CUST_IDENT_KEY);

		}

		if (response.getCode() == ResponseCode.SUCCESS) {
			return new MappingUtility<List<CustomerResponseDTO>>(false).JsonToDto(response.getData(),
					new TypeToken<List<CustomerResponseDTO>>() {
					}.getType());
		} else
			throw new PaySysException(ResponseCode.DATA_NOT_FOUND, ResponseDesc.DATA_NOT_FOUND);
	}

	protected String generateCustomerCode(String userTypeKey) throws Exception {
		logger.debug("Function called: generateCustomerCode");

		try {

			APICallerDetail callerDetail = new APICallerDetail();
			callerDetail.add(userTypeKey);

			apiCaller = context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
			ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER,
					APIKeyConstants.API_KEY_GENERATE_CUST_CODE, callerDetail);
			if (response.getCode() == ResponseCode.SUCCESS) {

				return response.getData().toString();
			} else {
				throw new PaySysException(response.getCode(), response.getMessage());
			}

		} catch (Exception ex) {
			throw ex;
		}
	}

	protected List<KycAttributeResponseDTO> getProducAndCustomerKyc(CustomerResponseDTO customer) {
		logger.debug("Function called: getProducAndCustomerKyc");

		List<CustomerKycResponseDTO> customerKycSet = this.getCustKycByCode(customer.getCode());
		// Get all customer products
		List<ProductResponseDTO> customerProducts = this.getCustProductsByCode(customer.getCode());
		// Get all kyc for all customer products
		List<KycAttributeResponseDTO> custProdKycSet = this.getCustProdKycByInvstTypes(customer, customerProducts);
		// Map customer kyc and customer product kyc
		return this.mapKyc(customerKycSet, custProdKycSet);
	}

	protected List<KycAttributeResponseDTO> getUpdatedKycForAuditLog(CustomerResponseDTO customer,
			List<KycAttributeResponseDTO> custKyc) {
		logger.debug("Function called: getUpdatedKycForAuditLog");

		List<KycAttributeResponseDTO> updatedKyc = new ArrayList<>();
		List<KycAttributeResponseDTO> allKyc = getProducAndCustomerKyc(customer);

		for (KycAttributeResponseDTO originalKyc : allKyc) {
			for (KycAttributeResponseDTO newKyc : custKyc) {
				if (originalKyc.getAttributeCode().equals(newKyc.getAttributeCode())) {
					if (!originalKyc.getAttributeValue().equals(newKyc.getAttributeValue()))
						updatedKyc.add(newKyc);
				}
			}
		}

		return updatedKyc;

	}

	protected BasicInfoResponseDTO getCustBasicInfoByProduct(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getCustBasicInfoByProduct");

		List<CustomerResponseDTO> customers = getCustomerByIdentificationKey(request,
				CustomerIdentityKey.fromString(request.getCustIdentityKey().toUpperCase()));

		if (customers != null) {

			List<CustomerDTO> customersBasicInfo;
			String innerRequestType = this.getInnerRequestType(request);
			if (innerRequestType != null && (innerRequestType.equalsIgnoreCase(BPMProcess.C2CTransferOut.toString())
					|| innerRequestType.equalsIgnoreCase(BPMProcess.C2CTransferIn.toString())
					|| innerRequestType.equalsIgnoreCase(BPMProcess.reInvestment.toString())
					|| innerRequestType.equalsIgnoreCase(BPMProcess.nomineePayment.toString())
					|| innerRequestType.equalsIgnoreCase(BPMProcess.nomineePaymentEncashment.toString())
					|| innerRequestType.equalsIgnoreCase(BPMProcess.nomineePaymentReinvestment.toString())
					|| innerRequestType.equalsIgnoreCase(BPMProcess.lapsedRevival.toString())))
				customersBasicInfo = getCustomerWithBasicInfoForTransfer(customers);
			else
				customersBasicInfo = getCustomerWithBasicInfo(customers);

			BasicInfoResponseDTO basicInfo = new BasicInfoResponseDTO();
			basicInfo.setCustomers(customersBasicInfo);
			basicInfo = getProductBasicInfo(request, basicInfo);
			// Override Category Code
			request.setCategory(basicInfo.getCategoryCode());

			return basicInfo;

		} else
			throw new PaySysException(ResponseCode.ERR_CODE_CUST_NOT_FOUND, ResponseDesc.ERR_DESC_CUST_NOT_FOUND);

	}

	protected RegistrationRequestDTO getProductDetails(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getProductDetails");

		String transactionKey = request.getRegistrationRequestDetailDTO().get(0).getTransactionKey();
		String innerRequestType = this.getInnerRequestType(request);

		if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_ACC_INFO) != null) {

			AccountResponseDTO account = this.getAccountByNumber(request.getUserTypeKey(),
					request.getCustIdentityValue());

			ServiceResponse response = apiCaller.execute(RequestType.GET,
					ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT_UNIT,
					APIKeyConstants.API_KEY_GET_UNIT_PROFIT_DET_BY_ACC_NUM,
					new APICallerDetail().add(request.getCustIdentityValue()));

			List<ProfitDetailResponseDTO> profitDetails = new MappingUtility<List<ProfitDetailResponseDTO>>()
					.JsonToDto(response.getData(), new TypeToken<List<ProfitDetailResponseDTO>>() {
					}.getType());

			CustomerAccountDTO accountDTO = new MappingUtility<CustomerAccountDTO>().JsonToDto(account,
					CustomerAccountDTO.class);
			accountDTO.setCustomerAccountUnits(profitDetails);

			request.setProductCode(account.getProductCode());
			request.setAmount(account.getCurrentBalance().toPlainString());
			request.setCategory(
					account.getCustomerAccountRels().get(0).getProduct().getProductCategory().getCategoryCode());

			this.getTransfererWSB(account.getAccountNumber(), request);

			request = this.addRegistrationRequestDetails(request, accountDTO, BPMTransactionKey.TRANSFERER_ACC_INFO);
		}

		if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_RESTRICTION) != null) {

			List<RestrictionResponseDTO> custRes = this.getCustRestrictionOnRegOrAcc(request);

			request = this.addRegistrationRequestDetails(request, custRes, BPMTransactionKey.CUSTOMER_RESTRICTION);
		}

		if (this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO) != null) {

			AccountRequestDTO accountInfo = (AccountRequestDTO) this.parse(request, BPMTransactionKey.ACCOUNT_INFO);

			AccountResponseDTO account = this.getAccountByNumber(request.getUserTypeKey(),
					request.getCustIdentityValue());
			request.setProductCode(account.getProductCode());
			request.setAmount(account.getCurrentBalance().toPlainString());

			if (accountInfo != null && accountInfo.getAccountTitle() != null)
				account.setAccountTitle(accountInfo.getAccountTitle());

			if (innerRequestType == null || (!innerRequestType.equalsIgnoreCase(BPMProcess.WSBReIssue.toString())
					&& !innerRequestType.equalsIgnoreCase(BPMProcess.accountConversion.toString())
					&& innerRequestType.equalsIgnoreCase(BPMProcess.updateAccount.toString())))
				this.getExistingWSlipBookOfCustomer(account.getAccountNumber(), request);

			request = this.addRegistrationRequestDetails(request, account, BPMTransactionKey.ACCOUNT_INFO);
		}

		if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_CERT_INV) != null
				|| this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY) != null) {
			List<CustomerCertificateResponseDTO> certificates = null;
			if (innerRequestType != null && innerRequestType.equalsIgnoreCase(BPMProcess.PCBReIssue.toString())) {

				CustomerCertificateResponseDTO certificate = this
						.getOneCertificateByRegCode(request.getCustIdentityValue());
				request.setAmount(this.getTotalAmountByRegCode(request.getCustIdentityValue()));
				request.setProductCode(certificate.getProductCode());
				if (certificate.getCustomerAccountRels() != null && !certificate.getCustomerAccountRels().isEmpty()) {
					request.setCategory(certificate.getCustomerAccountRels().get(0).getProduct().getProductCategory()
							.getCategoryCode());
				}
				certificates = Collections.singletonList(certificate);

			} else {
				certificates = this.getCertificatesByRegCodeAndBranch(request);
				if (request.getAdditionalInfo() != null && request.getAdditionalInfo().get("updateCertStatus") != null
						&& request.getAdditionalInfo().get("updateCertStatus").equalsIgnoreCase("true")) {
					certificates.removeIf(cert -> !isValidStatusEncash(cert.getStatusCode()));

				} else {
					certificates.removeIf(cert -> !isValidStatus(cert.getStatusCode()));
				}
				BigDecimal certificatesSum = BigDecimal.ZERO;

				for (CustomerCertificateResponseDTO cert : certificates) {
					certificatesSum = certificatesSum.add(cert.getAvailableBalance());
					request.setProductCode(cert.getProductCode());
					request.setCategory(
							cert.getCustomerAccountRels().get(0).getProduct().getProductCategory().getCategoryCode());
				}
				request.setAmount(this.getTotalAmountByRegCode(request.getCustIdentityValue()));
			}
			request = this.addRegistrationRequestDetails(request, certificates,
					BPMTransactionKey.valueOf(transactionKey.toUpperCase()));
		}

		if (this.getRequestDetailsByKey(request, BPMTransactionKey.JOINT_REQUEST) != null) {

			String innerKey = request.getRegistrationRequestDetailDTO().get(1).getTransactionKey();

			// Get Primary Customer
			List<CustomerResponseDTO> customers = this.getCustomerByProduct(request);
			CustomerResponseDTO customer = customers.get(0);
			String custIdentityValue = request.getAdditionalInfo().get("custIdentityValue");
			CustomerAccountRelResponseDTO customerRel = null;
			String investorType;

			switch (CustomerIdentityKey.fromString(request.getCustIdentityKey().toUpperCase())) {
			case ACCOUNT_NUMBER:
				AccountResponseDTO account = this.getAccountByNumber(request.getUserTypeKey(),
						request.getCustIdentityValue());

				customerRel = customer.getCustomerAccountRels().stream()
						.filter(p -> p.getProduct().getCode().equals(account.getProductCode())
								&& p.getAccountId() == account.getId())
						.findFirst().orElse(null);

				request.setCategory(customerRel.getProduct().getProductCategory().getCategoryCode());

				break;

			case REGISTRATION_CODE:
				List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCodeAndBranch(request);
				if (customer.getCustomerAccountRels() != null) {
					customerRel = customer.getCustomerAccountRels().stream()
							.filter(p -> p.getProduct().getCode().equals(certificates.get(0).getProductCode())
									&& p.getCustomerCertificateId() == certificates.get(0).getId())
							.findFirst().orElse(null);

				} else {
					customerRel = certificates.stream().findFirst().get().getCustomerAccountRels().stream().findFirst()
							.orElse(null);
				}
				request.setCategory(customerRel.getProduct().getProductCategory().getCategoryCode());
				break;
			}

			investorType = customerRel.getInvestorType().getCode();

			// Get Joint Customer
			RegistrationRequestDetailDTO registrationRequestDetailDTO = request.getRegistrationRequestDetailDTO()
					.stream().filter(predicate -> predicate.getTransactionKey()
							.equalsIgnoreCase(BPMTransactionKey.JOINT_REQUEST.toString()))
					.findFirst().orElse(null);

			if (registrationRequestDetailDTO != null) {

				// Joint customer detials sent from ui
				CustomerDTO jointCustomer = new MappingUtility<CustomerDTO>(false)
						.RespToReqDto(registrationRequestDetailDTO.getContent(), CustomerDTO.class);
				jointCustomer.setBranchCode(request.getUserTypeKey());

				// Create new dto to check if existing customer
				RegistrationRequestDTO requestDTO = new MappingUtility<RegistrationRequestDTO>(this.mapUserTypeKey())
						.RespToReqDto(jointCustomer, RegistrationRequestDTO.class);

				CustomerResponseDTO existingCustomer = null;
				try {
					// Check is this is an existing customer
					existingCustomer = this.getCustomerByIdentifier(requestDTO, CustomerIdentityKey.IDENTIFIER);
				} catch (Exception ignored) {
				}

				if (innerKey.equalsIgnoreCase(BPMTransactionKey.CUSTOMER_JOINT_KYC.toString())) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (existingCustomer != null) {

						jointCustomer.setCustomerCode(existingCustomer.getCode());
						customerKycSet = this.getCustKycByCode(jointCustomer.getCustomerCode());

					} else {
						jointCustomer.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));
						jointCustomer.setStatus("A");
						jointCustomer.setIsPrimary(Constants.NO);
						jointCustomer.setCreatedDate(new Timestamp(new Date().getTime()));
						jointCustomer.setCreatedBy(request.getUserTypeKey());
					}

					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							jointCustomer.getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);

					finalKycSet.forEach(kyc -> {
						if (kyc.getAttributeCode().equals(KYCEnum.KYC_CUST_IDENT_CODE.toString()))
							kyc.setAttributeValue(jointCustomer.getCustIdentityKey());
						if (kyc.getAttributeCode().equals(KYCEnum.KYC_CUST_IDENT_VALUE.toString()))
							kyc.setAttributeValue(jointCustomer.getCustIdentityValue());
					});

					request = this.addRegistrationRequestDetails(request, finalKycSet,
							BPMTransactionKey.CUSTOMER_JOINT_KYC);

					RegistrationRequestDetailDTO jointCustDet = new RegistrationRequestDetailDTO();
					jointCustDet.setTransactionKey(BPMTransactionKey.JOINT_REQUEST.toString());
					jointCustDet.setContent(jointCustomer);
					request.getRegistrationRequestDetailDTO().add(jointCustDet);

				} else if (innerKey.equalsIgnoreCase(BPMTransactionKey.CUSTOMER_DOCUMENTS.toString())) {

					List<InvestorDocumentDetailResponseDTO> investorDocumentDetailResponseDTOs = getCustomerRecDocuments(
							investorType, customer.getCode(), Constants.DOC_TYPE_ISSUEABLE, custIdentityValue);

					List<InvestorDocumentDetailResponseDTO> jointInvestorDocumentDetailResponseDTOs = getCustomerRecDocuments(
							jointCustomer.getInvestorTypeCode(), jointCustomer.getCustomerCode(),
							Constants.DOC_TYPE_ISSUEABLE, jointCustomer.getCustIdentityValue());

					investorDocumentDetailResponseDTOs.addAll(jointInvestorDocumentDetailResponseDTOs);

					if (investorDocumentDetailResponseDTOs != null && !investorDocumentDetailResponseDTOs.isEmpty()) {

						request = this.addRegistrationRequestDetails(request, investorDocumentDetailResponseDTOs,
								BPMTransactionKey.CUSTOMER_DOCUMENTS);

					} else {
						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
					}

				} else if (innerKey.equalsIgnoreCase(BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT.toString())) {

					List<CustomerDocumentResponseDTO> customerDocumentResponseDTOs = getCustomerDocumentsForAttachments(
							investorType, request.getProductCode(), customer.getCode(), custIdentityValue);

					if (customerDocumentResponseDTOs != null && !customerDocumentResponseDTOs.isEmpty()) {

						List<CustomerDocumentResponseDTO> jointCustomerDocumentResponseDTOs = getCustomerDocumentsForAttachments(
								jointCustomer.getInvestorTypeCode(), request.getProductCode(),
								jointCustomer.getCustomerCode(), jointCustomer.getCustIdentityValue());

						customerDocumentResponseDTOs.addAll(jointCustomerDocumentResponseDTOs.stream().filter(
								document -> document.getDocumentType().getScope().equalsIgnoreCase(Constants.GLOBAL))
								.collect(Collectors.toList()));

						request = this.addRegistrationRequestDetails(request, customerDocumentResponseDTOs,
								BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT);

					} else
						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
				}
			} else
				throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND, ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
		}

		if (this.getRequestDetailsByKey(request, BPMTransactionKey.MINOR_TO_MAJOR) != null) {

			RegistrationRequestDetailDTO registrationRequestDetailDTO = request.getRegistrationRequestDetailDTO()
					.stream().filter(predicate -> predicate.getTransactionKey()
							.equalsIgnoreCase(BPMTransactionKey.MINOR_TO_MAJOR.toString()))
					.findFirst().orElse(null);

			String investorTypeCode;
			String minorIdentityValue;
			String minorIdentityKey;

			if (registrationRequestDetailDTO != null) {

				// Joint customer detials sent from ui
				CustomerDTO majorCustomer = new MappingUtility<CustomerDTO>(false)
						.RespToReqDto(registrationRequestDetailDTO.getContent(), CustomerDTO.class);

				investorTypeCode = majorCustomer.getInvestorTypeCode();
				minorIdentityValue = request.getAdditionalInfo().get("custIdentityValue");
				minorIdentityKey = request.getAdditionalInfo().get("custIdentityKey");

				RegistrationRequestDTO requestDTO = new RegistrationRequestDTO();
				requestDTO.setCustIdentityValue(minorIdentityValue);
				requestDTO.setCustIdentityKey(minorIdentityKey);

				requestDTO.setUserTypeKey(request.getUserTypeKey());

				CustomerResponseDTO minorCustomer = this.getCustomerByIdentifier(requestDTO,
						CustomerIdentityKey.IDENTIFIER);

				requestDTO.setCustIdentityValue(majorCustomer.getCustIdentityValue());

				CustomerResponseDTO existingCustomer = null;
				try {

					// Check is this is an existing customer
					existingCustomer = this.getCustomerByIdentifier(requestDTO, CustomerIdentityKey.IDENTIFIER);

				} catch (Exception ignored) {
				}

				String innerKey = request.getRegistrationRequestDetailDTO().get(1).getTransactionKey();

				switch (BPMTransactionKey.valueOf(innerKey)) {

				case CUSTOMER_KYC:
					List<CustomerKycResponseDTO> custKyc;

					if (existingCustomer != null) {

						majorCustomer = this.getCustomerWithBasicInfo(Collections.singletonList(existingCustomer))
								.get(0);
						majorCustomer.setInvestorTypeCode(investorTypeCode);

						custKyc = this.getCustKycByCode(majorCustomer.getCustomerCode());

						if (existingCustomer.getCode().equalsIgnoreCase(minorCustomer.getCode()))
							majorCustomer.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));

					} else {

						majorCustomer.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));
						majorCustomer.setBranchCode(minorCustomer.getBranch());
						majorCustomer.setInvestorTypeCode(investorTypeCode);
						majorCustomer.setDisplayName(minorCustomer.getDisplayName());

						custKyc = this.getCustKycByCode(minorCustomer.getCode());

						for (CustomerKycResponseDTO kyc : custKyc) {
							if (kyc.getAttributeCode().equals(KYCEnum.KYC_CUST_IDENT_CODE.toString()))
								kyc.setAttributeValue(majorCustomer.getCustIdentityKey());
							if (kyc.getAttributeCode().equals(KYCEnum.KYC_CUST_IDENT_VALUE.toString()))
								kyc.setAttributeValue(majorCustomer.getCustIdentityValue());
						}

					}
					List<KycAttributeResponseDTO> productKyc = this.getProductKycSet(request.getProductCode(),
							investorTypeCode);
					List<KycAttributeResponseDTO> finalKyc = this.mapKyc(custKyc, productKyc);

					request = this.addRegistrationRequestDetails(request, finalKyc, BPMTransactionKey.CUSTOMER_KYC);

					RegistrationRequestDetailDTO majorCustDet = new RegistrationRequestDetailDTO();
					majorCustDet.setTransactionKey(BPMTransactionKey.MINOR_TO_MAJOR.toString());
					majorCustDet.setContent(majorCustomer);
					request.getRegistrationRequestDetailDTO().add(majorCustDet);

					break;

				case CUSTOMER_DOCUMENTS:

					List<InvestorDocumentDetailResponseDTO> minorCustDocs = getCustomerRecDocuments(
							InvestorType.Individual_Minor.toString(), minorCustomer.getCode(),
							Constants.DOC_TYPE_ISSUEABLE, minorIdentityValue);

					minorCustDocs.removeIf(
							doc -> doc.getDocumentType().getCode().equalsIgnoreCase(Constants.DOC_CNIC_OPERATOR));

					List<InvestorDocumentDetailResponseDTO> majorCustDocs = getCustomerRecDocuments(investorTypeCode,
							majorCustomer.getCustomerCode(), Constants.DOC_TYPE_ISSUEABLE,
							majorCustomer.getCustIdentityValue());

					minorCustDocs.addAll(majorCustDocs);

					if (minorCustDocs != null && !minorCustDocs.isEmpty()) {

						request = this.addRegistrationRequestDetails(request, minorCustDocs,
								BPMTransactionKey.CUSTOMER_DOCUMENTS);

					} else {
						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
					}

					break;

				case CUSTOMER_DOCUMENTS_ATTACHMENT:

					List<CustomerDocumentResponseDTO> majorDocs = getCustomerDocumentsForAttachments(investorTypeCode,
							request.getProductCode(), majorCustomer.getCustomerCode(),
							majorCustomer.getCustIdentityValue());

					/*
					 * majorDocs.removeIf(doc -> doc.getDocumentType().getCode()
					 * .equalsIgnoreCase(Constants.DOC_SC1_FORM));
					 */

					if (majorDocs != null && !majorDocs.isEmpty()) {

						request = this.addRegistrationRequestDetails(request, majorDocs,
								BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT);

					} else
						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);

					break;
				}

			}
		}
		if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_ACCOUNT) != null) {

			RegistrationRequestDetailDTO requestDetails = request.getRegistrationRequestDetailDTO().get(0);
			HashMap<String, String> accDetails = ((HashMap<String, String>) requestDetails.getContent());

			AccountResponseDTO account = this.getAccountByNumber(accDetails.get("accountNumber"));

			this.getExistingWSlipBookOfCustomerCorelate(account.getAccountNumber(), request);

			request = this.addRegistrationRequestDetails(request, account, BPMTransactionKey.CUSTOMER_ACCOUNT);

		}

		if (this.getRequestDetailsByKey(request, BPMTransactionKey.PROFIT_DETAILS) != null) {

			List<CustomerProfitPaymentResponseDTO> profitDetails = this.getCustomerProfitDetails(request);

			// Retrurn empty data set
			if (profitDetails == null)
				profitDetails = new ArrayList<>();

			request = this.addRegistrationRequestDetails(request, profitDetails, BPMTransactionKey.PROFIT_DETAILS);

		}

		return request;
	}

	protected CustomerCertificateResponseDTO getOneCertificateByRegCode(String regCode) throws PaySysException {
		ServiceResponse serviceResponse = apiCaller.execute(RequestType.GET,
				ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE, APIKeyConstants.API_KEY_GET_ONE_BY_CODE,
				new APICallerDetail().add(regCode));

		if (serviceResponse.getCode() == ResponseCode.SUCCESS)
			return new MappingUtility<CustomerCertificateResponseDTO>(false).JsonToDto(serviceResponse.getData(),
					CustomerCertificateResponseDTO.class);
		else
			throw new PaySysException(serviceResponse.getCode(), serviceResponse.getMessage());
	}

	protected CustomerCertificateResponseDTO getOneCertificateByRegCodeAndBranch(String regCode, String branchCode)
			throws PaySysException {
		ServiceResponse serviceResponse = apiCaller.execute(RequestType.GET,
				ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
				APIKeyConstants.API_KEY_GET_ONE_BY_REG_CODE_AND_BRANCH,
				new APICallerDetail().add(regCode).add(branchCode));

		if (serviceResponse.getCode() == ResponseCode.SUCCESS)
			return new MappingUtility<CustomerCertificateResponseDTO>(false).JsonToDto(serviceResponse.getData(),
					CustomerCertificateResponseDTO.class);
		else
			throw new PaySysException(serviceResponse.getCode(), serviceResponse.getMessage());
	}

	protected CustomerCertificateResponseDTO getOneAllCertificateByRegCodeAndBranch(String regCode, String branchCode)
			throws PaySysException {
		ServiceResponse serviceResponse = apiCaller.execute(RequestType.GET,
				ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
				APIKeyConstants.API_KEY_GET_ONE_ALL_BY_REG_CODE_AND_BRANCH,
				new APICallerDetail().add(regCode).add(branchCode));

		if (serviceResponse.getCode() == ResponseCode.SUCCESS)
			return new MappingUtility<CustomerCertificateResponseDTO>(false).JsonToDto(serviceResponse.getData(),
					CustomerCertificateResponseDTO.class);
		else
			throw new PaySysException(serviceResponse.getCode(), serviceResponse.getMessage());
	}

	protected String getTotalAmountByRegCode(String regCode) throws PaySysException {

		CustomerRegistrationAmountRequestDTO registrationAmountRequestDTO = new CustomerRegistrationAmountRequestDTO();
		registrationAmountRequestDTO.setRegistrationCode(regCode);
		List<String> validStatuses = new ArrayList<>();
		validStatuses.add(StatusCodes.New.toString());
		validStatuses.add(StatusCodes.Normal.toString());
		validStatuses.add(StatusCodes.Lasped.toString());
		validStatuses.add(StatusCodes.Pledge.toString());
		validStatuses.add(StatusCodes.Freeze.toString());
		validStatuses.add(StatusCodes.Dormant.toString());
		validStatuses.add(StatusCodes.Decesed.toString());
		validStatuses.add(StatusCodes.Initiated.toString());
		validStatuses.add(StatusCodes.MigrationBlocked.toString());
		registrationAmountRequestDTO.setAllowedStatuses(validStatuses);

		ServiceResponse serviceResponse = apiCaller.execute(RequestType.POST,
				ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
				APIKeyConstants.API_KEY_GET_TOTAL_AMOUNT_BY_REG_CODE,
				new APICallerDetail().setObj(registrationAmountRequestDTO));
		if (serviceResponse.getCode() == ResponseCode.SUCCESS)
			return String.valueOf(serviceResponse.getData());
		else
			return "0";
		// throw new PaySysException(serviceResponse.getCode(),
		// serviceResponse.getMessage());
	}

	protected List<String> getReqeustedKycList(RegistrationRequestDTO request) {
		try {

			logger.debug("Function called: getReqeustedKycList");

			RegistrationRequestDetailDTO kycDetails = request.getRegistrationRequestDetailDTO().get(0);
			Type listType = new TypeToken<List<String>>() {
			}.getType();

			List<String> response = (List<String>) new MappingUtility<>(true).JsonToDto(kycDetails.getContent(),
					listType);
			return response;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected List<KycAttributeResponseDTO> mapKycIdentCodeAndValue(List<KycAttributeResponseDTO> kycAttrList,
			CustomerDTO customer) {
		logger.debug("Function called: mapKycIdentCodeAndValue");

		for (KycAttributeResponseDTO kyc : kycAttrList) {

			if (kyc.getAttributeCode().equals(KYCEnum.KYC_CUST_IDENT_CODE.toString())) {
				kyc.setAttributeValue(customer.getCustIdentityKey());
				logger.info("Mapping Customer Ident Key: " + customer.getCustIdentityKey());
			}
			if (kyc.getAttributeCode().equals(KYCEnum.KYC_CUST_IDENT_VALUE.toString())) {
				kyc.setAttributeValue(customer.getCustIdentityValue());
				logger.info("Mapping Customer Ident Value: " + customer.getCustIdentityValue());

			}
			if (kyc.getAttributeValue() == null)
				kyc.setIsEditable(Constants.YES);
			else
				kyc.setIsEditable(Constants.NO);
		}
		return kycAttrList;
	}

	protected RegistrationRequestDTO getC2CTransferOutDetails(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getC2CTransferDetails");

		ServiceResponse response = null;
		String transactionKey = request.getRegistrationRequestDetailDTO().get(0).getTransactionKey();
		String innerRequestType = this.getInnerRequestType(request);

		switch (BPMTransactionKey.valueOf(transactionKey.toUpperCase())) {
		case C2C_TRANSFER_REQUEST:

			RegistrationRequestDetailDTO c2cDetails = getRequestDetailsByKey(request,
					BPMTransactionKey.C2C_TRANSFER_REQUEST);
			BasicInfoResponseDTO transferBasicInfo = new BasicInfoResponseDTO();

			try {

				if (innerRequestType.equalsIgnoreCase(BPMProcess.C2CTransferOut.toString())) {

					transferBasicInfo = this.getCustBasicInfoByProduct(request);

				} else if (innerRequestType.equalsIgnoreCase(BPMProcess.C2CTransferIn.toString())) {

					CustomerDTO primaryCustomer = new MappingUtility<CustomerDTO>(false)
							.JsonToDto(c2cDetails.getContent(), CustomerDTO.class);

					CustomerResponseDTO tempCustomer = this.getCustomerByKycIdentValue(
							primaryCustomer.getCustIdentityValue(), request.getUserTypeKey());

					transferBasicInfo = new BasicInfoResponseDTO();
					transferBasicInfo.setCustomers(
							this.getCustomerWithBasicInfoForTransfer(Collections.singletonList(tempCustomer)));
					transferBasicInfo.getCustomers().get(0).setInvestorTypeCode(primaryCustomer.getInvestorTypeCode());
					transferBasicInfo.getCustomers().get(0).setOperMode(primaryCustomer.getOperMode());
					transferBasicInfo.getCustomers().get(0).setIsPrimary(primaryCustomer.getIsPrimary());

				}

			} catch (Exception e) {

				logger.info(String.format(
						"Customer not found. Request type: %1$s, Identity Key: %2$s, Identity Value: %3$s",
						request.getRequestType(), request.getCustIdentityKey(), request.getCustIdentityValue()));

				CustomerDTO primaryCustomer = new MappingUtility<CustomerDTO>(false).JsonToDto(c2cDetails.getContent(),
						CustomerDTO.class);

				primaryCustomer.setBranchCode(request.getUserTypeKey());

				if (!request.getAdditionalInfo().containsKey("generatedCustCode")) {
					primaryCustomer.setCustomerCode(null);
				}

				transferBasicInfo.setProductCode(request.getProductCode());
				transferBasicInfo.setCategoryCode(request.getCategory());
				ArrayList<CustomerDTO> customers = new ArrayList<CustomerDTO>();
				customers.add(primaryCustomer);
				transferBasicInfo.setCustomers(customers);

			}

			HashMap<String, String> additionalInfo = (HashMap<String, String>) request.getAdditionalInfo();

			if (c2cDetails != null) {
				// Key will be not null if Transfer In request
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.JOINT_REQUEST) != null) {

					RegistrationRequestDetailDTO jointDetails = getRequestDetailsByKey(request,
							BPMTransactionKey.JOINT_REQUEST);

					CustomerDTO jointCustomer = new MappingUtility<CustomerDTO>(false)
							.JsonToDto(jointDetails.getContent(), CustomerDTO.class);

					jointCustomer.setBranchCode(request.getUserTypeKey());

					try {

						CustomerResponseDTO tempCustomer = this.getCustomerByKycIdentValue(
								jointCustomer.getCustIdentityValue(), request.getUserTypeKey());

						transferBasicInfo.getCustomers().addAll(
								this.getCustomerWithBasicInfoForTransfer(Collections.singletonList(tempCustomer)));

						transferBasicInfo.getCustomers().get(1)
								.setInvestorTypeCode(jointCustomer.getInvestorTypeCode());
						transferBasicInfo.getCustomers().get(1).setOperMode(jointCustomer.getOperMode());
						transferBasicInfo.getCustomers().get(1).setIsPrimary(jointCustomer.getIsPrimary());

					} catch (Exception e) {

						logger.info(String.format(
								"Joint Customer not found. Request type: %1$s, Identity Key: %2$s, Identity Value: %3$s",
								request.getRequestType(), jointCustomer.getCustIdentityKey(),
								jointCustomer.getCustIdentityValue()));

						if (!request.getAdditionalInfo().containsKey("generatedJointCustCode")) {
							jointCustomer.setCustomerCode(null);
						}

					}

					transferBasicInfo.getCustomers().add(jointCustomer);
				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferBasicInfo.getCustomers().get(0).getCustomerCode() != null)

						customerKycSet = this
								.getCustKycByCode(transferBasicInfo.getCustomers().get(0).getCustomerCode());

					else {
						transferBasicInfo.getCustomers().get(0)
								.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));
						request.getAdditionalInfo().put("generatedCustCode", "true");
					}

					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferBasicInfo.getCustomers().get(0));

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_KYC);

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_JOINT_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferBasicInfo.getCustomers() != null && transferBasicInfo.getCustomers().size() > 1
							&& transferBasicInfo.getCustomers().get(1).getCustomerCode() != null)

						customerKycSet = this
								.getCustKycByCode(transferBasicInfo.getCustomers().get(1).getCustomerCode());

					else {
						transferBasicInfo.getCustomers().get(1)
								.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));
						request.getAdditionalInfo().put("generatedJointCustCode", "true");

					}
					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferBasicInfo.getCustomers().get(1).getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferBasicInfo.getCustomers().get(1));

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_JOINT_KYC);

				}
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_DOCUMENTS) != null) {

					List<InvestorDocumentDetailResponseDTO> transferDocDetails = new ArrayList<>();

					List<InvestorDocumentDetailResponseDTO> primaryDocs = getCustomerRecDocuments(
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode(), request.getProductCode(),
							transferBasicInfo.getCustomers().get(0).getCustomerCode(),
							transferBasicInfo.getCustomers().get(0).getCustIdentityValue());

					transferDocDetails.addAll(primaryDocs);

					if (transferBasicInfo.getCustomers().size() > 1) {
						List<InvestorDocumentDetailResponseDTO> jointDocs = getCustomerRecDocuments(
								transferBasicInfo.getCustomers().get(1).getInvestorTypeCode(), request.getProductCode(),
								transferBasicInfo.getCustomers().get(1).getCustomerCode(),
								transferBasicInfo.getCustomers().get(1).getCustIdentityValue());

						jointDocs.removeIf(doc -> doc.getDocumentType().getScope().equalsIgnoreCase(Constants.LOCAL));
						transferDocDetails.addAll(jointDocs);
					}

					// GET DOCUMENTS WITH TYPE 05
					List<InvestorDocumentDetailResponseDTO> transferDocs = this.getDocByDocType(
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode(), request.getProductCode(),
							Constants.DOC_TYPE_C2C);

					transferDocDetails.addAll(transferDocs);

					if (!transferDocDetails.isEmpty()) {

						request = addRegistrationRequestDetails(request, transferDocDetails,
								BPMTransactionKey.CUSTOMER_DOCUMENTS);

					} else {

						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
					}

				}

				// For certificate transfer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_CERT_INV) != null) {

					List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCodeAndBranch(request);

					BigDecimal certificatesSum = BigDecimal.ZERO;
					for (CustomerCertificateResponseDTO cert : certificates) {
						certificatesSum = certificatesSum.add(cert.getAvailableBalance());
						request.setProductCode(cert.getProductCode());
					}

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
							APIKeyConstants.API_KEY_GET_CERT_PROFIT_DET_BY_REG_CODE,
							new APICallerDetail().add(request.getCustIdentityValue()));

					ProfitDetailResponseDTO profitDetail = new MappingUtility<ProfitDetailResponseDTO>()
							.JsonToDto(response.getData(), ProfitDetailResponseDTO.class);

					additionalInfo.put("effectiveDate",
							String.valueOf(certificates.get(0).getEffectiveDate().getTime()));
					additionalInfo.put("maturityDate", String.valueOf(certificates.get(0).getMaturityDate().getTime()));
					additionalInfo.put("profitCount", profitDetail.getProfitCount());
					additionalInfo.put("profitsPaid", profitDetail.getProfitsPaid());

					request.setAmount(certificatesSum.toPlainString());
					request.setCategory(certificates.get(0).getCustomerAccountRels().get(0).getProduct()
							.getProductCategory().getCategoryCode());

					request = addRegistrationRequestDetails(request, certificates,
							BPMTransactionKey.TRANSFERER_CERT_INV);

				}

				// For account transfer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_ACC_INFO) != null) {

					AccountResponseDTO account = this.getAccountByNumber(request.getUserTypeKey(),
							request.getCustIdentityValue());

					CustomerAccountDTO customerAccount = new MappingUtility<CustomerAccountDTO>().JsonToDto(account,
							CustomerAccountDTO.class);

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT_UNIT,
							APIKeyConstants.API_KEY_GET_UNIT_PROFIT_DET_BY_ACC_NUM,
							new APICallerDetail().add(request.getCustIdentityValue()));

					List<ProfitDetailResponseDTO> profitDetails = new MappingUtility<List<ProfitDetailResponseDTO>>()
							.JsonToDto(response.getData(), new TypeToken<List<ProfitDetailResponseDTO>>() {
							}.getType());

					customerAccount.setCustomerAccountUnits(profitDetails);

					request.setProductCode(account.getProductCode());
					request.setCategory(account.getCustomerAccountRels().get(0).getProduct().getProductCategory()
							.getCategoryCode());
					request.setAmount(account.getCurrentBalance().toPlainString());
					additionalInfo.put("units", String.valueOf((long) account.getCustomerAccountUnits().size()));
					this.getTransfererWSB(account.getAccountNumber(), request);

					request = addRegistrationRequestDetails(request, customerAccount,
							BPMTransactionKey.TRANSFERER_ACC_INFO);

				}

				if (!additionalInfo.containsKey("customerType")) {
					additionalInfo.put("customerType", transferBasicInfo.getCustomerType());
				} else {

					transferBasicInfo.getCustomers().get(0).setOperMode(additionalInfo.get("customerType"));
					transferBasicInfo.getCustomers().get(0).setCustomerType(additionalInfo.get("customerType"));

					if (transferBasicInfo.getCustomers().size() > 1)
						transferBasicInfo.getCustomers().get(1).setCustomerType(additionalInfo.get("customerType"));

				}

				request.setAdditionalInfo(additionalInfo);

				// Single customer details
				request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(0),
						BPMTransactionKey.C2C_TRANSFER_REQUEST);

				// Joint cusotmer details
				if (transferBasicInfo.getCustomers().size() > 1) {

					request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(1),
							BPMTransactionKey.JOINT_REQUEST);
				}

			} else
				throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
						ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);

			break;

		case CUSTOMER_ACCOUNT:

			RegistrationRequestDetailDTO requestDetails = this.getRequestDetailsByKey(request,
					BPMTransactionKey.CUSTOMER_ACCOUNT);
			AccountResponseDTO accountResponseDTO = new MappingUtility<AccountResponseDTO>()
					.JsonToDto(requestDetails.getContent(), AccountResponseDTO.class);
			AccountResponseDTO account;
			try {

				account = this.getAccountByNumber(accountResponseDTO.getAccountNumber());
				this.getExistingWSlipBookOfCustomerCorelate(account.getAccountNumber(), request);

			} catch (Exception e) {

				logger.debug(String.format("Account number %s does not exist", accountResponseDTO.getAccountNumber()));
				account = accountResponseDTO;
				account.setOpeningDate(new Date());
				AccountStatusResponseDTO accountStatus = new AccountStatusResponseDTO();
				accountStatus.setCode(AccountStatus.NORMAL.toString());
				account.setAccountStatus(accountStatus);

			}

			request = addRegistrationRequestDetails(request, account, BPMTransactionKey.CUSTOMER_ACCOUNT);

			break;

		case ACCOUNT_INFO:
			requestDetails = this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO);

			AccountResponseDTO accountInfo = new MappingUtility<AccountResponseDTO>()
					.JsonToDto(requestDetails.getContent(), AccountResponseDTO.class);

			String transferToBranch = request.getAdditionalInfo().get("to");

			AccountResponseDTO newAccountInfo = this.generateAccountNumber(transferToBranch, request.getProductCode(),
					BPMTransactionKey.ACCOUNT_INFO);

			OperationModeResponseDTO operationMode = new OperationModeResponseDTO();
			operationMode.setActive(Constants.YES);
			operationMode.setCode(request.getAdditionalInfo().get("customerType"));
			newAccountInfo.setType(operationMode.getCode());
			newAccountInfo.setAccountTitle(accountInfo.getAccountTitle());
			newAccountInfo.setOperationMode(operationMode);
			newAccountInfo.setCreatedBy(request.getUserId());

			if (!innerRequestType.equalsIgnoreCase(BPMProcess.C2CTransferIn.toString())) {

				AccountResponseDTO transferAccount = this.getAccountByNumber(request.getAdditionalInfo().get("from"),
						request.getCustIdentityValue());
				newAccountInfo.setCustomerAccountUnits(transferAccount.getCustomerAccountUnits());
				newAccountInfo.setAvaliableBalance(transferAccount.getAvaliableBalance());
				newAccountInfo.setCurrentBalance(transferAccount.getCurrentBalance());

			} else if (innerRequestType.equalsIgnoreCase(BPMProcess.C2CTransferIn.toString())) {

				RegistrationRequestDetailDTO profitDetailRequest = this.getRequestDetailsByKey(request,
						BPMTransactionKey.TRANSFERER_ACC_INFO);

				CustomerAccountDTO accountDTO = new MappingUtility<CustomerAccountDTO>()
						.JsonToDto(profitDetailRequest.getContent(), CustomerAccountDTO.class);

				/*
				 * List<ProfitDetailResponseDTO> profitDetails = new
				 * MappingUtility<List<ProfitDetailResponseDTO>>()
				 * .JsonToDto(profitDetailRequest.getContent(), new
				 * TypeToken<List<ProfitDetailResponseDTO>>() { }.getType());
				 */

				List<CustomerAccountUnitRequestDTO> accountUnits = new ArrayList<>();
				accountDTO.getCustomerAccountUnits()
						.sort(Comparator.comparing(ProfitDetailResponseDTO::getEffectiveDate));

				for (ProfitDetailResponseDTO pUnit : accountDTO.getCustomerAccountUnits()) {
					newAccountInfo.setCurrentBalance(newAccountInfo.getCurrentBalance().add(pUnit.getCurrentBalance()));
					newAccountInfo
							.setAvaliableBalance(newAccountInfo.getAvaliableBalance().add(pUnit.getCurrentBalance()));

					CustomerAccountUnitRequestDTO unit = new CustomerAccountUnitRequestDTO();
					unit.setCreatedBy(request.getUserId());
					unit.setCreatedDate(pUnit.getEffectiveDate());
					unit.setDepositDate(pUnit.getEffectiveDate());
					unit.setEffectiveDate(pUnit.getEffectiveDate());
					unit.setMaturityDate(pUnit.getMaturityDate());
					unit.setCurrentBalance(pUnit.getCurrentBalance());
					unit.setAvailableBalance(pUnit.getCurrentBalance());
					unit.setUnitNo(String.valueOf(accountUnits.size() + 1));

					accountUnits.add(unit);
				}

				newAccountInfo.setCreatedDate(accountInfo.getEffectiveDate());
				newAccountInfo.setCustomerAccountUnits(accountUnits);

			}

			request = addRegistrationRequestDetails(request, newAccountInfo, BPMTransactionKey.ACCOUNT_INFO);
			break;

		}

		return request;
	}

	protected RegistrationRequestDTO getC2CTransferInDetails(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getC2CTransferDetails");

		ServiceResponse response = null;
		String transactionKey = request.getRegistrationRequestDetailDTO().get(0).getTransactionKey();
		String innerRequestType = this.getInnerRequestType(request);

		switch (BPMTransactionKey.valueOf(transactionKey.toUpperCase())) {
		case C2C_TRANSFER_REQUEST:

			RegistrationRequestDetailDTO c2cDetails = getRequestDetailsByKey(request,
					BPMTransactionKey.C2C_TRANSFER_REQUEST);
			BasicInfoResponseDTO transferBasicInfo = new BasicInfoResponseDTO();

			try {

				CustomerDTO primaryCustomer = new MappingUtility<CustomerDTO>(false).JsonToDto(c2cDetails.getContent(),
						CustomerDTO.class);

				CustomerResponseDTO tempCustomer = this
						.getCustomerByKycIdentValue(primaryCustomer.getCustIdentityValue(), request.getUserTypeKey());

				transferBasicInfo = new BasicInfoResponseDTO();
				transferBasicInfo.setCustomers(
						this.getCustomerWithBasicInfoForTransfer(Collections.singletonList(tempCustomer)));
				transferBasicInfo.getCustomers().get(0).setInvestorTypeCode(primaryCustomer.getInvestorTypeCode());
				transferBasicInfo.getCustomers().get(0).setOperMode(primaryCustomer.getOperMode());
				transferBasicInfo.getCustomers().get(0).setIsPrimary(primaryCustomer.getIsPrimary());

			} catch (Exception e) {

				logger.info(String.format(
						"Customer not found. Request type: %1$s, Identity Key: %2$s, Identity Value: %3$s",
						request.getRequestType(), request.getCustIdentityKey(), request.getCustIdentityValue()));

				CustomerDTO primaryCustomer = new MappingUtility<CustomerDTO>(false).JsonToDto(c2cDetails.getContent(),
						CustomerDTO.class);

				primaryCustomer.setBranchCode(request.getUserTypeKey());

				if (!request.getAdditionalInfo().containsKey("generatedCustCode")) {
					primaryCustomer.setCustomerCode(null);
				}

				transferBasicInfo.setProductCode(request.getProductCode());
				transferBasicInfo.setCategoryCode(request.getCategory());
				ArrayList<CustomerDTO> customers = new ArrayList<CustomerDTO>();
				customers.add(primaryCustomer);
				transferBasicInfo.setCustomers(customers);

			}

			HashMap<String, String> additionalInfo = (HashMap<String, String>) request.getAdditionalInfo();

			if (c2cDetails != null) {
				// Key will be not null if Transfer In request
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.JOINT_REQUEST) != null) {

					RegistrationRequestDetailDTO jointDetails = getRequestDetailsByKey(request,
							BPMTransactionKey.JOINT_REQUEST);

					CustomerDTO jointCustomer = new MappingUtility<CustomerDTO>(false)
							.JsonToDto(jointDetails.getContent(), CustomerDTO.class);

					jointCustomer.setBranchCode(request.getUserTypeKey());

					try {

						CustomerResponseDTO tempCustomer = this.getCustomerByKycIdentValue(
								jointCustomer.getCustIdentityValue(), request.getUserTypeKey());

						transferBasicInfo.getCustomers().addAll(
								this.getCustomerWithBasicInfoForTransfer(Collections.singletonList(tempCustomer)));

						transferBasicInfo.getCustomers().get(1)
								.setInvestorTypeCode(jointCustomer.getInvestorTypeCode());
						transferBasicInfo.getCustomers().get(1).setOperMode(jointCustomer.getOperMode());
						transferBasicInfo.getCustomers().get(1).setIsPrimary(jointCustomer.getIsPrimary());

					} catch (Exception e) {

						logger.info(String.format(
								"Joint Customer not found. Request type: %1$s, Identity Key: %2$s, Identity Value: %3$s",
								request.getRequestType(), jointCustomer.getCustIdentityKey(),
								jointCustomer.getCustIdentityValue()));

						if (!request.getAdditionalInfo().containsKey("generatedJointCustCode")) {
							jointCustomer.setCustomerCode(null);
						}

					}

					transferBasicInfo.getCustomers().add(jointCustomer);
				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferBasicInfo.getCustomers().get(0).getCustomerCode() != null)

						customerKycSet = this
								.getCustKycByCode(transferBasicInfo.getCustomers().get(0).getCustomerCode());

					else {
						transferBasicInfo.getCustomers().get(0)
								.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));
						request.getAdditionalInfo().put("generatedCustCode", "true");
					}

					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferBasicInfo.getCustomers().get(0));

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_KYC);

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_JOINT_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferBasicInfo.getCustomers() != null && transferBasicInfo.getCustomers().size() > 1
							&& transferBasicInfo.getCustomers().get(1).getCustomerCode() != null)

						customerKycSet = this
								.getCustKycByCode(transferBasicInfo.getCustomers().get(1).getCustomerCode());

					else {
						transferBasicInfo.getCustomers().get(1)
								.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));
						request.getAdditionalInfo().put("generatedJointCustCode", "true");

					}
					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferBasicInfo.getCustomers().get(1).getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferBasicInfo.getCustomers().get(1));

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_JOINT_KYC);

				}
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_DOCUMENTS) != null) {

					List<InvestorDocumentDetailResponseDTO> transferDocDetails = new ArrayList<>();

					List<InvestorDocumentDetailResponseDTO> primaryDocs = getCustomerRecDocuments(
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode(), request.getProductCode(),
							transferBasicInfo.getCustomers().get(0).getCustomerCode(),
							transferBasicInfo.getCustomers().get(0).getCustIdentityValue());

					transferDocDetails.addAll(primaryDocs);

					if (transferBasicInfo.getCustomers().size() > 1) {
						List<InvestorDocumentDetailResponseDTO> jointDocs = getCustomerRecDocuments(
								transferBasicInfo.getCustomers().get(1).getInvestorTypeCode(), request.getProductCode(),
								transferBasicInfo.getCustomers().get(1).getCustomerCode(),
								transferBasicInfo.getCustomers().get(1).getCustIdentityValue());

						jointDocs.removeIf(doc -> doc.getDocumentType().getScope().equalsIgnoreCase(Constants.LOCAL));
						transferDocDetails.addAll(jointDocs);
					}

					// GET DOCUMENTS WITH TYPE 05
					List<InvestorDocumentDetailResponseDTO> transferDocs = this.getDocByDocType(
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode(), request.getProductCode(),
							Constants.DOC_TYPE_C2C);

					transferDocDetails.addAll(transferDocs);

					if (!transferDocDetails.isEmpty()) {

						request = addRegistrationRequestDetails(request, transferDocDetails,
								BPMTransactionKey.CUSTOMER_DOCUMENTS);

					} else {

						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
					}

				}

				// For certificate transfer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_CERT_INV) != null) {

					List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCodeAndBranch(request);

					BigDecimal certificatesSum = BigDecimal.ZERO;
					for (CustomerCertificateResponseDTO cert : certificates) {
						certificatesSum = certificatesSum.add(cert.getAvailableBalance());
						request.setProductCode(cert.getProductCode());
					}

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
							APIKeyConstants.API_KEY_GET_CERT_PROFIT_DET_BY_REG_CODE,
							new APICallerDetail().add(request.getCustIdentityValue()));

					ProfitDetailResponseDTO profitDetail = new MappingUtility<ProfitDetailResponseDTO>()
							.JsonToDto(response.getData(), ProfitDetailResponseDTO.class);

					additionalInfo.put("effectiveDate",
							String.valueOf(certificates.get(0).getEffectiveDate().getTime()));
					additionalInfo.put("maturityDate", String.valueOf(certificates.get(0).getMaturityDate().getTime()));
					additionalInfo.put("profitCount", profitDetail.getProfitCount());
					additionalInfo.put("profitsPaid", profitDetail.getProfitsPaid());

					request.setAmount(certificatesSum.toPlainString());
					request.setCategory(certificates.get(0).getCustomerAccountRels().get(0).getProduct()
							.getProductCategory().getCategoryCode());

					request = addRegistrationRequestDetails(request, certificates,
							BPMTransactionKey.TRANSFERER_CERT_INV);

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO) != null) {

					AccountResponseDTO account = this.generateAccountNumber(request.getUserTypeKey(),
							request.getProductCode(), BPMTransactionKey.TRANSFERER_ACC_INFO);

					OperationModeResponseDTO oper = new OperationModeResponseDTO();
					oper.setCode(additionalInfo.get("customerType"));
					account.setType(additionalInfo.get("customerType"));
					account.setAccountTitle(additionalInfo.get("displayName"));

					account.setOperationMode(oper);
					request = addRegistrationRequestDetails(request, account, BPMTransactionKey.ACCOUNT_INFO);

				}
				// For account transfer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_ACC_INFO) != null) {

					AccountResponseDTO account = this.getAccountByNumber(request.getUserTypeKey(),
							request.getCustIdentityValue());

					CustomerAccountDTO customerAccount = new MappingUtility<CustomerAccountDTO>().JsonToDto(account,
							CustomerAccountDTO.class);

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT_UNIT,
							APIKeyConstants.API_KEY_GET_UNIT_PROFIT_DET_BY_ACC_NUM,
							new APICallerDetail().add(request.getCustIdentityValue()));

					List<ProfitDetailResponseDTO> profitDetails = new MappingUtility<List<ProfitDetailResponseDTO>>()
							.JsonToDto(response.getData(), new TypeToken<List<ProfitDetailResponseDTO>>() {
							}.getType());

					customerAccount.setCustomerAccountUnits(profitDetails);

					request.setProductCode(account.getProductCode());
					request.setCategory(account.getCustomerAccountRels().get(0).getProduct().getProductCategory()
							.getCategoryCode());
					request.setAmount(account.getCurrentBalance().toPlainString());
					additionalInfo.put("units", String.valueOf((long) account.getCustomerAccountUnits().size()));
					this.getTransfererWSB(account.getAccountNumber(), request);

					request = addRegistrationRequestDetails(request, customerAccount,
							BPMTransactionKey.TRANSFERER_ACC_INFO);

				}

				if (!additionalInfo.containsKey("customerType")) {
					additionalInfo.put("customerType", transferBasicInfo.getCustomerType());
				} else {

					transferBasicInfo.getCustomers().get(0).setOperMode(additionalInfo.get("customerType"));
					transferBasicInfo.getCustomers().get(0).setCustomerType(additionalInfo.get("customerType"));

					if (transferBasicInfo.getCustomers().size() > 1)
						transferBasicInfo.getCustomers().get(1).setCustomerType(additionalInfo.get("customerType"));

				}

				request.setAdditionalInfo(additionalInfo);

				// Single customer details
				request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(0),
						BPMTransactionKey.C2C_TRANSFER_REQUEST);

				// Joint cusotmer details
				if (transferBasicInfo.getCustomers().size() > 1) {

					request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(1),
							BPMTransactionKey.JOINT_REQUEST);
				}

			} else
				throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
						ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);

			break;

		case CUSTOMER_ACCOUNT:

			RegistrationRequestDetailDTO requestDetails = this.getRequestDetailsByKey(request,
					BPMTransactionKey.CUSTOMER_ACCOUNT);
			AccountResponseDTO accountResponseDTO = new MappingUtility<AccountResponseDTO>()
					.JsonToDto(requestDetails.getContent(), AccountResponseDTO.class);
			AccountResponseDTO account;
			try {

				account = this.getAccountByNumber(accountResponseDTO.getAccountNumber());
				this.getExistingWSlipBookOfCustomerCorelate(account.getAccountNumber(), request);

			} catch (Exception e) {

				logger.debug(String.format("Account number %s does not exist", accountResponseDTO.getAccountNumber()));
				account = accountResponseDTO;
				account.setOpeningDate(new Date());
				AccountStatusResponseDTO accountStatus = new AccountStatusResponseDTO();
				accountStatus.setCode(AccountStatus.NORMAL.toString());
				account.setAccountStatus(accountStatus);

			}

			request = addRegistrationRequestDetails(request, account, BPMTransactionKey.CUSTOMER_ACCOUNT);

			break;

		case ACCOUNT_INFO:
			requestDetails = this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO);

			AccountResponseDTO accountInfo = new MappingUtility<AccountResponseDTO>()
					.JsonToDto(requestDetails.getContent(), AccountResponseDTO.class);

			String transferToBranch = request.getAdditionalInfo().get("to");

			AccountResponseDTO newAccountInfo = this.generateAccountNumber(transferToBranch, request.getProductCode(),
					BPMTransactionKey.ACCOUNT_INFO);

			OperationModeResponseDTO operationMode = new OperationModeResponseDTO();
			operationMode.setActive(Constants.YES);
			operationMode.setCode(request.getAdditionalInfo().get("customerType"));
			newAccountInfo.setType(operationMode.getCode());
			newAccountInfo.setAccountTitle(accountInfo.getAccountTitle());
			newAccountInfo.setOperationMode(operationMode);
			newAccountInfo.setCreatedBy(request.getUserId());

			RegistrationRequestDetailDTO profitDetailRequest = this.getRequestDetailsByKey(request,
					BPMTransactionKey.TRANSFERER_ACC_INFO);

			CustomerAccountDTO accountDTO = new MappingUtility<CustomerAccountDTO>()
					.JsonToDto(profitDetailRequest.getContent(), CustomerAccountDTO.class);

			/*
			 * List<ProfitDetailResponseDTO> profitDetails = new
			 * MappingUtility<List<ProfitDetailResponseDTO>>()
			 * .JsonToDto(profitDetailRequest.getContent(), new
			 * TypeToken<List<ProfitDetailResponseDTO>>() { }.getType());
			 */

			List<CustomerAccountUnitRequestDTO> accountUnits = new ArrayList<>();
			accountDTO.getCustomerAccountUnits().sort(Comparator.comparing(ProfitDetailResponseDTO::getEffectiveDate));

			for (ProfitDetailResponseDTO pUnit : accountDTO.getCustomerAccountUnits()) {
				newAccountInfo.setCurrentBalance(newAccountInfo.getCurrentBalance().add(pUnit.getCurrentBalance()));
				newAccountInfo.setAvaliableBalance(newAccountInfo.getAvaliableBalance().add(pUnit.getCurrentBalance()));

				CustomerAccountUnitRequestDTO unit = new CustomerAccountUnitRequestDTO();
				unit.setCreatedBy(request.getUserId());
				unit.setCreatedDate(pUnit.getEffectiveDate());
				unit.setDepositDate(pUnit.getEffectiveDate());
				unit.setEffectiveDate(pUnit.getEffectiveDate());
				unit.setMaturityDate(pUnit.getMaturityDate());
				unit.setCurrentBalance(pUnit.getCurrentBalance());
				unit.setAvailableBalance(pUnit.getCurrentBalance());
				unit.setUnitNo(String.valueOf(accountUnits.size() + 1));

				accountUnits.add(unit);
			}

			newAccountInfo.setCreatedDate(accountInfo.getEffectiveDate());
			newAccountInfo.setCustomerAccountUnits(accountUnits);

			request = addRegistrationRequestDetails(request, newAccountInfo, BPMTransactionKey.ACCOUNT_INFO);
			break;

		}

		return request;
	}

	protected RegistrationRequestDTO getLapsedRevivalDetails(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getLapsedRevivalDetails");

		ServiceResponse response = null;
		String transactionKey = request.getRegistrationRequestDetailDTO().get(0).getTransactionKey();
		String innerRequestType = this.getInnerRequestType(request);

		switch (BPMTransactionKey.valueOf(transactionKey.toUpperCase())) {
		case LAPSED_REVIVAL_REQUEST:

			RegistrationRequestDetailDTO c2cDetails = getRequestDetailsByKey(request,
					BPMTransactionKey.LAPSED_REVIVAL_REQUEST);
			BasicInfoResponseDTO transferBasicInfo = this.getCustBasicInfoByProduct(request);

			HashMap<String, String> additionalInfo = (HashMap<String, String>) request.getAdditionalInfo();

			if (c2cDetails != null) {

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferBasicInfo.getCustomers().get(0).getCustomerCode() != null)
						customerKycSet = this
								.getCustKycByCode(transferBasicInfo.getCustomers().get(0).getCustomerCode());
					else
						transferBasicInfo.getCustomers().get(0)
								.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));

					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferBasicInfo.getCustomers().get(0));

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_KYC);

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.REINVESTMENT_DETAILS) != null) {

					APICaller crmEngine = (APICaller) context.getBean(APICaller.class, ServiceNames.TRANSACTION_ENGINE);
					response = crmEngine.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_CRM_ENGINE,
							APIKeyConstants.API_KEY_PROCESS, new APICallerDetail().setObj(request));

					if (response.getCode() == ResponseCode.SUCCESS) {
						List<ReinvestmentDetailsResponseDTO> staticResponse = new ObjectMapper().readValue(
								(String) response.getData(), new TypeReference<List<ReinvestmentDetailsResponseDTO>>() {
								});

						// staticResponse.sort(Comparator.comparing(ReinvestmentDetailsResponseDTO::getMaturityDate));

						ReinvestmentDetailsResponseDTO reInvDetails = Collections.max(staticResponse,
								Comparator.comparing(ReinvestmentDetailsResponseDTO::getMaturityDate));

						additionalInfo.put("effectiveDate", String.valueOf(reInvDetails.getEffectiveDate().getTime()));
						additionalInfo.put("maturityDate", String.valueOf(reInvDetails.getMaturityDate().getTime()));

						request = addRegistrationRequestDetails(request, staticResponse,
								BPMTransactionKey.REINVESTMENT_DETAILS);
					} else
						throw new PaySysException(response.getCode(), response.getMessage());

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_JOINT_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferBasicInfo.getCustomers() != null && transferBasicInfo.getCustomers().size() > 1)
						customerKycSet = this
								.getCustKycByCode(transferBasicInfo.getCustomers().get(1).getCustomerCode());
					else
						transferBasicInfo.getCustomers().get(1)
								.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));

					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferBasicInfo.getCustomers().get(1).getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferBasicInfo.getCustomers().get(1));

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_JOINT_KYC);

				}
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_DOCUMENTS) != null) {

					List<InvestorDocumentDetailResponseDTO> transferDocDetails = new ArrayList<>();

					// Existing product holders
					for (CustomerDTO transferFrom : transferBasicInfo.getCustomers()) {

						List<InvestorDocumentDetailResponseDTO> transferFromDocDetails = getCustomerRecDocuments(
								transferFrom.getInvestorTypeCode(), request.getProductCode(),
								transferFrom.getCustomerCode(), transferFrom.getCustIdentityValue());

						/*
						 * transferFromDocDetails.removeIf(doc -> doc.getDocumentType().getCode()
						 * .equalsIgnoreCase(Constants.DOC_SC1_FORM) || doc.getDocumentType().getCode()
						 * .equalsIgnoreCase(Constants.DOC_SIG_CARD));
						 */

						transferDocDetails.addAll(transferFromDocDetails);

					}

					// GET DOCUMENTS WITH TYPE 05
					List<InvestorDocumentDetailResponseDTO> transferDocs = this.getDocByDocType(
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode(), request.getProductCode(),
							Constants.DOC_TYPE_C2C);

					transferDocDetails.addAll(transferDocs);

					if (transferDocDetails != null && !transferDocDetails.isEmpty()) {

						request = addRegistrationRequestDetails(request, transferDocDetails,
								BPMTransactionKey.CUSTOMER_DOCUMENTS);

					} else {
						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
					}

				}

				// For certificate transfer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_CERT_INV) != null) {

					List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCodeAndBranch(request);

					BigDecimal certificatesSum = BigDecimal.ZERO;
					for (CustomerCertificateResponseDTO cert : certificates) {
						certificatesSum = certificatesSum.add(cert.getAvailableBalance());
						request.setProductCode(cert.getProductCode());
					}

					request.setAmount(certificatesSum.toPlainString());
					request.setCategory(certificates.get(0).getCustomerAccountRels().get(0).getProduct()
							.getProductCategory().getCategoryCode());

					additionalInfo.put("originalEffectiveDate",
							String.valueOf(certificates.get(0).getEffectiveDate().getTime()));
					additionalInfo.put("originalMaturityDate",
							String.valueOf(certificates.get(0).getMaturityDate().getTime()));

					request = addRegistrationRequestDetails(request, certificates,
							BPMTransactionKey.TRANSFERER_CERT_INV);

				}

				// For account transfer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_ACC_INFO) != null
						|| this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO) != null) {

					AccountResponseDTO account = this.getAccountByNumber(request.getUserTypeKey(),
							request.getCustIdentityValue());

					CustomerAccountDTO customerAccount = new MappingUtility<CustomerAccountDTO>().JsonToDto(account,
							CustomerAccountDTO.class);

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT_UNIT,
							APIKeyConstants.API_KEY_GET_UNIT_PROFIT_DET_BY_ACC_NUM,
							new APICallerDetail().add(customerAccount.getAccountNumber()));

					List<ProfitDetailResponseDTO> profitDetails = new MappingUtility<List<ProfitDetailResponseDTO>>()
							.JsonToDto(response.getData(), new TypeToken<List<ProfitDetailResponseDTO>>() {
							}.getType());

					profitDetails.forEach(unit -> {
						int tenure = account.getCustomerAccountRels().get(0).getProduct().getTenure().intValue();
						Long maturityDateInMillis = unit.getEffectiveDate().getTime()
								+ (Constants.MILLIS_IN_1_MONTH * tenure);
						unit.setMaturityDate(new Date(maturityDateInMillis));
					});

					customerAccount.setCustomerAccountUnits(profitDetails);

					request.setProductCode(account.getProductCode());
					request.setCategory(account.getCustomerAccountRels().get(0).getProduct().getProductCategory()
							.getCategoryCode());
					request.setAmount(account.getCurrentBalance().toPlainString());
					additionalInfo.put("units", String.valueOf((long) account.getCustomerAccountUnits().size()));

					BPMTransactionKey key;
					if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_ACC_INFO) != null) {
						key = BPMTransactionKey.TRANSFERER_ACC_INFO;
					} else {
						key = BPMTransactionKey.ACCOUNT_INFO;
						AccountStatusResponseDTO reInvestStatus = new AccountStatusResponseDTO();
						reInvestStatus.setCode(StatusCodes.Normal.toString());
						customerAccount.setAccountStatus(reInvestStatus);
					}

					request = addRegistrationRequestDetails(request, customerAccount, key);

				}

				additionalInfo.put("customerType", transferBasicInfo.getCustomerType());

				request.setAdditionalInfo(additionalInfo);

				// Single customer details
				request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(0),
						BPMTransactionKey.LAPSED_REVIVAL_REQUEST);

				// Joint cusotmer details
				if (transferBasicInfo.getCustomers().size() > 1) {

					request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(1),
							BPMTransactionKey.JOINT_REQUEST);
				}

			} else
				throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
						ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);

			break;

		}

		return request;
	}

	protected RegistrationRequestDTO getNomineePaymentDetails(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getNomineePaymentDetails");

		ServiceResponse response = null;
		String transactionKey = request.getRegistrationRequestDetailDTO().get(0).getTransactionKey();
		String innerRequestType = this.getInnerRequestType(request);

		switch (BPMTransactionKey.valueOf(transactionKey.toUpperCase())) {
		case NOMINEE_PAYMENT_REQUEST:

			RegistrationRequestDetailDTO nomineePayDetails = getRequestDetailsByKey(request,
					BPMTransactionKey.NOMINEE_PAYMENT_REQUEST);
			BasicInfoResponseDTO transferBasicInfo = this.getCustBasicInfoByProduct(request);
			BigDecimal investmentAmount = BigDecimal.ZERO;
			BigDecimal dueProfit = BigDecimal.ZERO;
			HashMap<String, String> additionalInfo = (HashMap<String, String>) request.getAdditionalInfo();

			switch (CustomerIdentityKey.fromString(request.getCustIdentityKey())) {
			case ACCOUNT_NUMBER:
				AccountResponseDTO account = this.getAccountByNumber(request.getCustIdentityValue());
				investmentAmount = account.getCurrentBalance();
				request.setCategory(
						account.getCustomerAccountRels().get(0).getProduct().getProductCategory().getCategoryCode());

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT_UNIT,
						APIKeyConstants.API_KEY_GET_UNIT_PROFIT_DET_BY_ACC_NUM,
						new APICallerDetail().add(account.getAccountNumber()));

				List<ProfitDetailResponseDTO> profitDetails = new MappingUtility<List<ProfitDetailResponseDTO>>()
						.JsonToDto(response.getData(), new TypeToken<List<ProfitDetailResponseDTO>>() {
						}.getType());

				for (ProfitDetailResponseDTO unit : profitDetails) {
					if (unit.getDueAmount() != null)
						dueProfit = dueProfit.add(unit.getDueAmount());
				}

				break;

			case REGISTRATION_CODE:
				List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCode(request);

				BigDecimal certificatesSum = BigDecimal.ZERO;
				for (CustomerCertificateResponseDTO cert : certificates) {
					certificatesSum = certificatesSum.add(cert.getAvailableBalance());
					request.setProductCode(cert.getProductCode());
					request.setCategory(
							cert.getCustomerAccountRels().get(0).getProduct().getProductCategory().getCategoryCode());
					additionalInfo.put("effectiveDate", String.valueOf(cert.getEffectiveDate().getTime()));
					additionalInfo.put("maturityDate", String.valueOf(cert.getMaturityDate().getTime()));
				}

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
						APIKeyConstants.API_KEY_GET_CERT_PROFIT_DET_BY_REG_CODE,
						new APICallerDetail().add(request.getCustIdentityValue()));

				ProfitDetailResponseDTO profitDetail = new MappingUtility<ProfitDetailResponseDTO>()
						.JsonToDto(response.getData(), ProfitDetailResponseDTO.class);

				dueProfit = new BigDecimal(
						profitDetail.getDueAmount() == null ? "0" : profitDetail.getDueAmount().toPlainString());

				investmentAmount = certificatesSum;
				break;

			default:
				throw new PaySysException(ResponseCode.ERR_CODE_INVAID_CUST_IDENT_KEY,
						ResponseDesc.ERR_DESC_INVALID_CUST_IDENT_KEY);
			}
			additionalInfo.put("dueAmount", dueProfit.toPlainString());
			request.setAmount(investmentAmount.toPlainString());

			if (nomineePayDetails != null) {

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_DOCUMENTS) != null) {

					// GET DOCUMENTS WITH TYPE 05
					List<InvestorDocumentDetailResponseDTO> nomineePayDocs = this.getDocByDocType(
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode(), request.getProductCode(),
							Constants.DOC_TYPE_NOMINEE_PAY_LETTER);

					if (nomineePayDocs != null && !nomineePayDocs.isEmpty()) {

						request = addRegistrationRequestDetails(request, nomineePayDocs,
								BPMTransactionKey.CUSTOMER_DOCUMENTS);

					} else {
						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
					}

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.NOMINEE_PAYMENT_DETAILS) != null) {

					APICaller crmEngine = (APICaller) context.getBean(APICaller.class, ServiceNames.TRANSACTION_ENGINE);
					response = crmEngine.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_CRM_ENGINE,
							APIKeyConstants.API_KEY_PROCESS, new APICallerDetail().setObj(request));

					List<NomineePayDetailsResponseDTO> staticResponse = new ObjectMapper().readValue(
							(String) response.getData(), new TypeReference<List<NomineePayDetailsResponseDTO>>() {
							});

					request = addRegistrationRequestDetails(request, staticResponse,
							BPMTransactionKey.NOMINEE_PAYMENT_DETAILS);

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_ASSOCIATE) != null) {

					List<CustomerAssociateDTO> nominees = this.getProductNominees(request);

					nominees.removeIf(
							n -> n.getProfitPaid() != null && n.getProfitPaid().equalsIgnoreCase(Constants.YES));

					for (CustomerAssociateDTO nominee : nominees) {
						Double nomineeShare = Double.parseDouble(nominee.getNomineeShare());
						Double totalAmount = (investmentAmount.doubleValue() + dueProfit.doubleValue());
						Double nomineeAmount = (nomineeShare / 100) * totalAmount;
						nominee.setNomineeAmount(String.valueOf(nomineeAmount));
						logger.info(String.format("Nominee name: %1$s, Nominee Share: %2$s, Nominee Amount: %3$s",
								nominee.getDisplayName(), nominee.getNomineeShare(), nominee.getNomineeAmount()));
					}

					request = addRegistrationRequestDetails(request, nominees, BPMTransactionKey.CUSTOMER_ASSOCIATE);

				}

				additionalInfo.put("customerType", transferBasicInfo.getCustomerType());

				request.setAdditionalInfo(additionalInfo);

				// Single customer details
				request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(0),
						BPMTransactionKey.NOMINEE_PAYMENT_REQUEST);

				// Joint cusotmer details
				if (transferBasicInfo.getCustomers().size() > 1) {

					request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(1),
							BPMTransactionKey.JOINT_REQUEST);
				}

			} else
				throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
						ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);

			break;

		}

		return request;
	}

	protected RegistrationRequestDTO getReinvestmentDetails(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getReinvestmentDetails");

		ServiceResponse response = null;
		String transactionKey = request.getRegistrationRequestDetailDTO().get(0).getTransactionKey();
		String innerRequestType = this.getInnerRequestType(request);

		switch (BPMTransactionKey.valueOf(transactionKey.toUpperCase())) {
		case REINVESTMENT_REQUEST:

			RegistrationRequestDetailDTO c2cDetails = getRequestDetailsByKey(request,
					BPMTransactionKey.REINVESTMENT_REQUEST);

			BasicInfoResponseDTO transferBasicInfo = this.getCustBasicInfoByProduct(request);

			HashMap<String, String> additionalInfo = (HashMap<String, String>) request.getAdditionalInfo();

			if (c2cDetails != null) {

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferBasicInfo.getCustomers().get(0).getCustomerCode() != null)
						customerKycSet = this
								.getCustKycByCode(transferBasicInfo.getCustomers().get(0).getCustomerCode());
					else
						transferBasicInfo.getCustomers().get(0)
								.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));

					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferBasicInfo.getCustomers().get(0));

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_KYC);

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.REINVESTMENT_DETAILS) != null) {

					APICaller crmEngine = (APICaller) context.getBean(APICaller.class, ServiceNames.TRANSACTION_ENGINE);
					response = crmEngine.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_CRM_ENGINE,
							APIKeyConstants.API_KEY_PROCESS, new APICallerDetail().setObj(request));

					if (response.getCode() == ResponseCode.SUCCESS) {

						List<ReinvestmentDetailsResponseDTO> staticResponse = new ObjectMapper().readValue(
								(String) response.getData(), new TypeReference<List<ReinvestmentDetailsResponseDTO>>() {
								});

						// staticResponse.sort(Comparator.comparing(ReinvestmentDetailsResponseDTO::getMaturityDate));

						ReinvestmentDetailsResponseDTO reInvDetails = Collections.max(staticResponse,
								Comparator.comparing(ReinvestmentDetailsResponseDTO::getMaturityDate));

						additionalInfo.put("effectiveDate", String.valueOf(reInvDetails.getEffectiveDate().getTime()));
						additionalInfo.put("maturityDate", String.valueOf(reInvDetails.getMaturityDate().getTime()));

						request = addRegistrationRequestDetails(request, staticResponse,
								BPMTransactionKey.REINVESTMENT_DETAILS);
					} else
						throw new PaySysException(response.getCode(), response.getMessage());
				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_JOINT_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferBasicInfo.getCustomers() != null && transferBasicInfo.getCustomers().size() > 1)
						customerKycSet = this
								.getCustKycByCode(transferBasicInfo.getCustomers().get(1).getCustomerCode());
					else
						transferBasicInfo.getCustomers().get(1)
								.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));

					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferBasicInfo.getCustomers().get(1).getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferBasicInfo.getCustomers().get(1));

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_JOINT_KYC);

				}
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_DOCUMENTS) != null) {

					List<InvestorDocumentDetailResponseDTO> transferDocDetails = new ArrayList<>();

					List<InvestorDocumentDetailResponseDTO> primaryDocs = getCustomerRecDocuments(
							transferBasicInfo.getCustomers().get(0).getInvestorTypeCode(), request.getProductCode(),
							transferBasicInfo.getCustomers().get(0).getCustomerCode(),
							transferBasicInfo.getCustomers().get(0).getCustIdentityValue());

					transferDocDetails.addAll(primaryDocs);

					if (transferBasicInfo.getCustomers().size() > 1) {
						List<InvestorDocumentDetailResponseDTO> jointDocs = getCustomerRecDocuments(
								transferBasicInfo.getCustomers().get(1).getInvestorTypeCode(), request.getProductCode(),
								transferBasicInfo.getCustomers().get(1).getCustomerCode(),
								transferBasicInfo.getCustomers().get(1).getCustIdentityValue());

						jointDocs.removeIf(
								doc -> doc.getDocumentType().getCode().equalsIgnoreCase(Constants.DOC_SC1_FORM));

						transferDocDetails.addAll(primaryDocs);

					}

					if (!transferDocDetails.isEmpty()) {

						request = addRegistrationRequestDetails(request, transferDocDetails,
								BPMTransactionKey.CUSTOMER_DOCUMENTS);

					} else {
						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
					}

				}

				// For certificate transfer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_CERT_INV) != null) {

					List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCodeAndBranch(request);

					BigDecimal certificatesSum = BigDecimal.ZERO;
					for (CustomerCertificateResponseDTO cert : certificates) {
						certificatesSum = certificatesSum.add(cert.getAvailableBalance());
						request.setProductCode(cert.getProductCode());
					}

					additionalInfo.put("originalEffectiveDate",
							String.valueOf(certificates.get(0).getEffectiveDate().getTime()));
					additionalInfo.put("originalMaturityDate",
							String.valueOf(certificates.get(0).getMaturityDate().getTime()));

					request.setAmount(certificatesSum.toPlainString());
					request.setCategory(certificates.get(0).getCustomerAccountRels().get(0).getProduct()
							.getProductCategory().getCategoryCode());
					request = addRegistrationRequestDetails(request, certificates,
							BPMTransactionKey.TRANSFERER_CERT_INV);

				}

				// For account transfer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_ACC_INFO) != null) {

					AccountResponseDTO account = this.getAccountByNumber(request.getUserTypeKey(),
							request.getCustIdentityValue());

					CustomerAccountDTO customerAccount = new MappingUtility<CustomerAccountDTO>().JsonToDto(account,
							CustomerAccountDTO.class);

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT_UNIT,
							APIKeyConstants.API_KEY_GET_UNIT_PROFIT_DET_BY_ACC_NUM,
							new APICallerDetail().add(customerAccount.getAccountNumber()));

					List<ProfitDetailResponseDTO> profitDetails = new MappingUtility<List<ProfitDetailResponseDTO>>()
							.JsonToDto(response.getData(), new TypeToken<List<ProfitDetailResponseDTO>>() {
							}.getType());

					Date reInvestAllowedBefore = new SimpleDateFormat("dd-MM-yyyy")
							.parse(Constants.REINVESTMENT_ALLLOWED_BEFORE_TDA);

					profitDetails.removeIf(p -> p.getEffectiveDate().after(reInvestAllowedBefore));

					customerAccount.setCustomerAccountUnits(profitDetails);

					request.setProductCode(account.getProductCode());
					request.setCategory(account.getCustomerAccountRels().get(0).getProduct().getProductCategory()
							.getCategoryCode());
					request.setAmount(account.getCurrentBalance().toPlainString());
					additionalInfo.put("units", String.valueOf((long) account.getCustomerAccountUnits().size()));

					if (innerRequestType == null
							|| !innerRequestType.equalsIgnoreCase(BPMProcess.WSBReIssue.toString()))
						this.getExistingWSlipBookOfCustomer(account.getAccountNumber(), request);

					request = addRegistrationRequestDetails(request, customerAccount,
							BPMTransactionKey.TRANSFERER_ACC_INFO);

				}

				if (transferBasicInfo.getCustomers().size() > 1) {
					additionalInfo.put("customerType", Constants.OP_MODE_JOINT);
					transferBasicInfo.getCustomers().get(0).setOperMode(Constants.OP_MODE_JOINT);
				} else {
					additionalInfo.put("customerType", Constants.OP_MODE_SINGLE);
					transferBasicInfo.getCustomers().get(0).setOperMode(Constants.OP_MODE_SINGLE);
				}

				request.setAdditionalInfo(additionalInfo);

				// Single customer details
				request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(0),
						BPMTransactionKey.REINVESTMENT_REQUEST);

				// Joint cusotmer details
				if (transferBasicInfo.getCustomers().size() > 1) {

					request = addRegistrationRequestDetails(request, transferBasicInfo.getCustomers().get(1),
							BPMTransactionKey.JOINT_REQUEST);
				}

			} else
				throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
						ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);

			break;

		}

		return request;
	}

	protected RegistrationRequestDTO getP2PTransferDetails(RegistrationRequestDTO request) throws Exception {
		logger.debug("Function called: getP2PTransferDetails");

		ServiceResponse response = null;
		String transactionKey = request.getRegistrationRequestDetailDTO().get(0).getTransactionKey();
		String innerRequestType = this.getInnerRequestType(request);
		BasicInfoResponseDTO transferFromBasicInfo = this.getCustBasicInfoByProduct(request);

		switch (BPMTransactionKey.valueOf(transactionKey.toUpperCase())) {

		case P2P_TRANSFER_REQUEST:

			CustomerDTO transferTo = null;
			CustomerDTO transferToJoint = null;
			String operMode;
			// transferFromBasicInfo = this.getCustBasicInfoByProduct(request);

			RegistrationRequestDetailDTO p2pDetails = getRequestDetailsByKey(request,
					BPMTransactionKey.P2P_TRANSFER_REQUEST);

			if (p2pDetails != null) {

				transferTo = new MappingUtility<CustomerDTO>(false).JsonToDto(p2pDetails.getContent(),
						CustomerDTO.class);

				operMode = transferTo.getOperMode();
				String transferToInvsterType = transferTo.getInvestorTypeCode();
				transferTo.setBranchCode(request.getUserTypeKey());

				if (!request.getAdditionalInfo().containsKey("generatedCustCode")) {
					transferTo.setCustomerCode(null);
				}

				try {

					CustomerResponseDTO existingCustomer = this
							.getCustomerByKycIdentValue(transferTo.getCustIdentityValue(), transferTo.getBranchCode());

					transferTo = this.getCustomerWithBasicInfoForTransfer(Collections.singletonList(existingCustomer))
							.get(0);

					transferTo.setInvestorTypeCode(transferToInvsterType);
					transferTo.setOperMode(operMode);

					request.getAdditionalInfo().put("generatedCustCode", "true");

				} catch (Exception e) {

					logger.info(String.format("Customer does not exist, generate new customer code, identity value: %s",
							transferTo.getCustIdentityValue()));

				}

				request.setCategory(transferFromBasicInfo.getCategoryCode());
				request = this.addRegistrationRequestDetails(request, transferTo,
						BPMTransactionKey.P2P_TRANSFER_REQUEST);

				// Check for transfer to type (Single, Joint)
				if (transferTo.getOperMode() != null && this.isJointReq(transferTo.getOperMode())
						&& getRequestDetailsByKey(request, BPMTransactionKey.JOINT_REQUEST) != null) {

					RegistrationRequestDetailDTO jointDetails = getRequestDetailsByKey(request,
							BPMTransactionKey.JOINT_REQUEST);

					transferToJoint = new MappingUtility<CustomerDTO>(false).JsonToDto(jointDetails.getContent(),
							CustomerDTO.class);

					String transferToJointInvType = transferToJoint.getInvestorTypeCode();
					transferToJoint.setBranchCode(request.getUserTypeKey());

					if (!request.getAdditionalInfo().containsKey("generatedJointCustCode")) {
						transferToJoint.setCustomerCode(null);
					}

					try {

						CustomerResponseDTO existingJointCustomer = this.getCustomerByKycIdentValue(
								transferToJoint.getCustIdentityValue(), transferToJoint.getBranchCode());
						transferToJoint = this
								.getCustomerWithBasicInfoForTransfer(Collections.singletonList(existingJointCustomer))
								.get(0);
						transferToJoint.setInvestorTypeCode(transferToJointInvType);
						transferToJoint.setOperMode(operMode);
						transferToJoint.setCreatedBy(request.getUserId());
						transferToJoint.setCreatedDate(new Timestamp(new Date().getTime()));
						request.getAdditionalInfo().put("generatedJointCustCode", "true");

					} catch (Exception e) {

						e.printStackTrace();

						logger.info(String.format(
								"Joint customer does not exist, generate new customer code, identity value: %s",
								transferToJoint.getCustIdentityValue()));

					}

					request = this.addRegistrationRequestDetails(request, transferToJoint,
							BPMTransactionKey.JOINT_REQUEST);

				}

				// For transfer to single customer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_KYC) != null) {

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferTo.getCustomerCode() != null)
						customerKycSet = this.getCustKycByCode(transferTo.getCustomerCode());
					else {
						transferTo.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));
						transferTo.setOperMode(operMode);
						transferTo.setCustomerType(operMode);
						transferTo.setInvestorTypeCode(transferToInvsterType);
						transferTo.setCreatedBy(request.getUserId());
						transferTo.setCreatedDate(new Timestamp(new Date().getTime()));
						request.getAdditionalInfo().put("generatedCustCode", "true");
					}
					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferTo.getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);

					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferTo);

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_KYC);

				}

				// For transfer to joint customer
				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_JOINT_KYC) != null) {

					if (transferToJoint == null)
						throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
								String.format("%1$s %2$s", ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING,
										BPMTransactionKey.JOINT_REQUEST.toString()));

					List<CustomerKycResponseDTO> customerKycSet = new ArrayList<>();

					if (transferToJoint.getCustomerCode() != null)
						customerKycSet = this.getCustKycByCode(transferToJoint.getCustomerCode());
					else {
						transferToJoint.setCustomerCode(this.generateCustomerCode(request.getUserTypeKey()));
						transferToJoint.setOperMode(operMode);
						transferToJoint.setCustomerType(operMode);
						transferToJoint.setInvestorTypeCode(transferToInvsterType);
						request.getAdditionalInfo().put("generatedJointCustCode", "true");
					}
					// Get kyc set for the selected product and investor code
					List<KycAttributeResponseDTO> custProdKycSet = this.getProductKycSet(request.getProductCode(),
							transferTo.getInvestorTypeCode());

					// Map customer kyc and customer product kyc
					List<KycAttributeResponseDTO> finalKycSet = this.mapKyc(customerKycSet, custProdKycSet);
					finalKycSet = this.mapKycIdentCodeAndValue(finalKycSet, transferToJoint);

					request = addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_JOINT_KYC);

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_DOCUMENTS) != null) {

					List<InvestorDocumentDetailResponseDTO> transferDocDetails = new ArrayList<>();

					// Existing product holders
					for (CustomerDTO transferFrom : transferFromBasicInfo.getCustomers()) {

						List<InvestorDocumentDetailResponseDTO> transferFromDocDetails = getCustomerRecDocuments(
								transferFrom.getInvestorTypeCode(), request.getProductCode(),
								transferFrom.getCustomerCode(), transferFrom.getCustIdentityValue());

						transferFromDocDetails.removeIf(
								doc -> doc.getDocumentType().getCode().equalsIgnoreCase(Constants.DOC_SC1_FORM)
										|| doc.getDocumentType().getCode().equalsIgnoreCase(Constants.DOC_SIG_CARD));

						transferDocDetails.addAll(transferFromDocDetails);

					}

					// Primary customer for transfer
					List<InvestorDocumentDetailResponseDTO> transferToDocDetails = getCustomerRecDocuments(
							transferTo.getInvestorTypeCode(), request.getProductCode(), transferTo.getCustomerCode(),
							transferTo.getCustIdentityValue());

					transferDocDetails.addAll(transferToDocDetails);

					// Secondary customer for transfer
					if (transferToJoint != null) {
						List<InvestorDocumentDetailResponseDTO> transferToJointDocDetails = getCustomerRecDocuments(
								transferToJoint.getInvestorTypeCode(), request.getProductCode(),
								transferToJoint.getCustomerCode(), transferToJoint.getCustIdentityValue());

						transferDocDetails.addAll(transferToJointDocDetails);

					}

					// GET DOCUMENTS WITH TYPE 04
					List<InvestorDocumentDetailResponseDTO> transferDocs = this.getDocByDocType(
							transferFromBasicInfo.getCustomers().get(0).getInvestorTypeCode(), request.getProductCode(),
							Constants.DOC_TYPE_P2P);

					transferDocDetails.addAll(transferDocs);

					if (!transferDocDetails.isEmpty()) {

						request = addRegistrationRequestDetails(request, transferDocDetails,
								BPMTransactionKey.CUSTOMER_DOCUMENTS);

					} else {

						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);

					}

				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT) != null) {

					List<CustomerDocumentResponseDTO> transferToDocAttach = getCustomerDocumentsForAttachments(
							transferTo.getInvestorTypeCode(), request.getProductCode(), transferTo.getCustomerCode(),
							transferTo.getCustIdentityValue());

					if (transferToJoint != null) {

						List<CustomerDocumentResponseDTO> transferToJointDocAttach = getCustomerDocumentsForAttachments(
								transferToJoint.getInvestorTypeCode(), request.getProductCode(),
								transferToJoint.getCustomerCode(), transferToJoint.getCustIdentityValue());

						/*
						 * customerDocumentResponseDTOs.addAll(jointCustomerDocumentResponseDTOs.stream(
						 * ) .filter(document -> document.getDocumentType().getScope()
						 * .equalsIgnoreCase(Constants.GLOBAL)) .collect(Collectors.toList()));
						 */

						transferToDocAttach.addAll(transferToJointDocAttach);

					}

					if (transferToDocAttach != null && !transferToDocAttach.isEmpty()) {

						request = this.addRegistrationRequestDetails(request, transferToDocAttach,
								BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT);

					} else
						throw new PaySysException(ResponseCode.ERR_CODE_DOCS_NOT_FOUND,
								ResponseDesc.ERR_DESC_DOCS_NOT_FOUND);
				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_RESTRICTION) != null) {

					ProductResponseDTO product = new ProductResponseDTO();
					product.setCode(request.getProductCode());

					List<ProductRestrictionResponseDTO> prodRes = this
							.getProductRestrictions(Collections.singletonList(product));

					List<CustomerRestrictionResponseDTO> custRes = new ArrayList<>();

					List<CustomerRestrictionResponseDTO> finalRes = this.mapRestrictions(prodRes, custRes);

					request = this.addRegistrationRequestDetails(request, finalRes,
							BPMTransactionKey.CUSTOMER_RESTRICTION);
				}

			} else
				throw new PaySysException(ResponseCode.ERR_CODE_TRANSACTION_KEY_MISSING,
						ResponseDesc.ERR_DESC_TRANSACTION_KEY_MISSING);
			break;

		case TRANSFERER_CERT_INV:

			List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCodeAndBranch(request);

			BigDecimal certificatesSum = BigDecimal.ZERO;
			for (CustomerCertificateResponseDTO cert : certificates) {
				certificatesSum = certificatesSum.add(cert.getAvailableBalance());
				request.setProductCode(cert.getProductCode());
			}

			request.getAdditionalInfo().put("effectiveDate",
					String.valueOf(certificates.get(0).getEffectiveDate().getTime()));
			request.getAdditionalInfo().put("maturityDate",
					String.valueOf(certificates.get(0).getMaturityDate().getTime()));

			request.setAmount(certificatesSum.toPlainString());
			request = this.addRegistrationRequestDetails(request, certificates, BPMTransactionKey.TRANSFERER_CERT_INV);
			break;

		case ACCOUNT_INFO:
		case TRANSFERER_ACC_INFO:

			AccountResponseDTO account = this.getAccountByNumber(request.getUserTypeKey(),
					request.getCustIdentityValue());
			request.setProductCode(account.getProductCode());
			request.setAmount(account.getCurrentBalance().toPlainString());

			if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSFERER_ACC_INFO) != null) {
				this.getTransfererWSB(account.getAccountNumber(), request);
				request = this.addRegistrationRequestDetails(request, account, BPMTransactionKey.TRANSFERER_ACC_INFO);
			}

			if (this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO) != null) {
				request = this.addRegistrationRequestDetails(request, account, BPMTransactionKey.ACCOUNT_INFO);
			}

			break;

		case CUSTOMER_ACCOUNT:

			RegistrationRequestDetailDTO requestDetails = request.getRegistrationRequestDetailDTO().get(0);
			AccountResponseDTO accountResponseDTO = new MappingUtility<AccountResponseDTO>()
					.JsonToDto(requestDetails.getContent(), AccountResponseDTO.class);
			try {

				account = this.getAccountByNumber(accountResponseDTO.getAccountNumber());
				this.getExistingWSlipBookOfCustomerCorelate(account.getAccountNumber(), request);

			} catch (Exception e) {
				e.printStackTrace();
				logger.debug(String.format("Account number %s does not exist", accountResponseDTO.getAccountNumber()));
				account = accountResponseDTO;
				account.setOpeningDate(new Date());
				AccountStatusResponseDTO accountStatus = new AccountStatusResponseDTO();
				accountStatus.setCode(AccountStatus.NORMAL.toString());
				account.setAccountStatus(accountStatus);

			}

			request = this.addRegistrationRequestDetails(request, account, BPMTransactionKey.CUSTOMER_ACCOUNT);

			break;

		}

		RegistrationRequestDetailDTO basicDetails = new RegistrationRequestDetailDTO();
		basicDetails.setTransactionKey(BPMTransactionKey.BASIC_INFO.toString());
		basicDetails.setContent(transferFromBasicInfo);
		request.getRegistrationRequestDetailDTO().add(basicDetails);

		return request;
	}

	protected void getTransfererWSB(String accountNumber, RegistrationRequestDTO registrationRequestDTO)
			throws Exception {
		logger.debug("Function Called : getExistingWSlipBookOfCustomerCorelate");

		this.apiCaller = (APICaller) context.getBean(APICaller.class, ServiceNames.DB_SERVICE);

		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVENTORY,
				APIKeyConstants.API_KEY_GET_WSLIP_BOOK_OF_CUST, new APICallerDetail().add(accountNumber));

		if (response.getCode() == ResponseCode.SUCCESS) {

			InventoryResponseDTO inventoryResponseDTO = (InventoryResponseDTO) new MappingUtility<>()
					.JsonToDto(response.getData(), InventoryResponseDTO.class);

			registrationRequestDTO.getAdditionalInfo().put("WSBStartingNumTransfererAcc",
					inventoryResponseDTO.getInstrumentSeries().concat(inventoryResponseDTO.getRangeFrom()));
			registrationRequestDTO.getAdditionalInfo().put("WSBEndingNumTransfererAcc",
					inventoryResponseDTO.getInstrumentSeries().concat(inventoryResponseDTO.getRangeTo()));
			registrationRequestDTO.getAdditionalInfo().put("WSBQuantityTransfererAcc",
					inventoryResponseDTO.getQuantity().toPlainString());
			registrationRequestDTO.getAdditionalInfo().put("WSBInventoryIdTransfererAcc",
					Long.toString(inventoryResponseDTO.getId()));
			registrationRequestDTO.getAdditionalInfo().put("CorelatedAccountExists", "1");

		} else if (response.getCode() == ResponseCode.DATA_NOT_FOUND) {
			throw new PaySysException(ResponseCode.FAILURE, ResponseDesc.ERR_DESC_WSLIP_BOOK_NOT_FOUND);
		} else {
			throw new PaySysException(response.getCode(), response.getMessage());
		}
	}

	protected RegionResponseDTO getRegionByBranchCode(String branchCode) {
		this.apiCaller = context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_REGION,
				APIKeyConstants.API_KEY_GET_BY_BRANCH_CODE, new APICallerDetail().add(branchCode));

		return new MappingUtility<RegionResponseDTO>(false).JsonToDto(response.getData(), RegionResponseDTO.class);
	}

	protected TreasuryResponseDTO getTreasuryByBranchCode(String branchCode) {
		this.apiCaller = context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_TREASURY,
				APIKeyConstants.API_KEY_GET_BY_BRANCH_CODE, new APICallerDetail().add(branchCode));

		return new MappingUtility<TreasuryResponseDTO>(false).JsonToDto(response.getData(), TreasuryResponseDTO.class);
	}

	protected BranchResponseDTO getBranchByCode(String branchCode) {
		this.apiCaller = context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
		ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_BRANCH,
				APIKeyConstants.API_KEY_GET_BY_CODE, new APICallerDetail().add(branchCode));

		return new MappingUtility<BranchResponseDTO>(false).JsonToDto(response.getData(), BranchResponseDTO.class);
	}

	protected RegistrationRequestDTO updateUserTypeKey(RegistrationRequestDTO request) {

		logger.info("Function called: updateUserTypeKey");
		logger.info(String.format("Request type: %1$s, Workflow status: %2$s", request.getRequestType(),
				request.getWorkFlowStatus()));

		HashMap<String, String> additionalInfo = (HashMap<String, String>) request.getAdditionalInfo();
		if (additionalInfo == null)
			additionalInfo = new HashMap<>();

		if ((request.getRequestType().equalsIgnoreCase(BPMProcess.lapsedReInvestment.toString())
				|| request.getRequestType().equalsIgnoreCase(BPMProcess.lapsedEncashment.toString())
				|| request.getRequestType().equalsIgnoreCase(BPMProcess.lapsedEncashmentByCash.toString()))
				&& request.getWorkFlowStatus() == null) {

			RegionResponseDTO region = this.getRegionByBranchCode(request.getUserTypeKey());
			additionalInfo.put("branchCode", request.getUserTypeKey());
			request.setUserTypeKey(region.getCode());
			// request.setUserType(UserTypeEnum.REGION.toString());
			logger.info(String.format("Region code: %s", region.getCode()));

		} else if (request.getWorkFlowStatus() != null
				&& (request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.REGION_INCHARGE_APPROVE.toString())
						|| request.getWorkFlowStatus()
								.equalsIgnoreCase(BPMWorkFlowStatus.TREASURY_INCHARGE_APPROVE.toString()))) {

			request.setUserTypeKey(request.getAdditionalInfo().get("branchCode"));
			// request.setUserType(UserTypeEnum.BRANCH.toString());
			logger.info(String.format("Branch code: %s", request.getUserTypeKey()));
			additionalInfo.put("workflowStatus", BPMWorkFlowStatus.fromString(request.getWorkFlowStatus()).toString());

		} else if ((request.getRequestType().equalsIgnoreCase(BPMProcess.lapsedEncashment.toString())
				|| request.getRequestType().equalsIgnoreCase(BPMProcess.nomineePaymentEncashment.toString())
				|| request.getRequestType().equalsIgnoreCase(BPMProcess.C2CTransferOut.toString()))
				&& request.getWorkFlowStatus() != null
				&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_VERIFY.toString())) {

			TreasuryResponseDTO treasury = this.getTreasuryByBranchCode(request.getUserTypeKey());
			additionalInfo.put("branchCode", request.getUserTypeKey());
			request.setUserTypeKey(treasury.getCode());
			// request.setUserType(UserTypeEnum.TREASURY.toString());
			logger.info(String.format("Treasury code: %s", treasury.getCode()));
		}

//		} else if (request.getRequestType().equalsIgnoreCase(BPMProcess.C2CTransferOut.toString())
//				&& request.getWorkFlowStatus() != null
//				&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_APPROVE.toString())) {
//			// Validate Branch is online or not then switch usertype key
//			BranchResponseDTO branch = this.getBranchByCode(request.getAdditionalInfo().get("to"));
//
//			logger.info(String.format("Branch code: %1$s, online: %2$s", branch.getBranchCode(), branch.getIsOnline()));
//
//			if (branch.getIsOnline().equalsIgnoreCase(Constants.YES)) {
//				request.setWorkFlowStatus(BPMWorkFlowStatus.INCHARGE_APPROVE.toString());
//				request.setUserTypeKey(branch.getBranchCode());
//				additionalInfo.put("workflowStatus", BPMWorkFlowStatus.INCHARGE_APPROVE.toString());
//
//			} else {
//				request.setWorkFlowStatus(BPMWorkFlowStatus.INCHARGE_APPROVED.toString());
//				request.setUserTypeKey(branch.getBranchCode());
//				additionalInfo.put("workflowStatus", BPMWorkFlowStatus.INCHARGE_APPROVE.toString());
//
//			}
//			logger.info(String.format("New WorkflowStatus: %s", request.getWorkFlowStatus()));
//
//		} else if (request.getRequestType().equalsIgnoreCase(BPMProcess.C2CTransferOut.toString())
//				&& request.getWorkFlowStatus() != null
//				&& request.getWorkFlowStatus().equalsIgnoreCase(BPMWorkFlowStatus.INCHARGE_APPROVED.toString())) {
//
//			additionalInfo.put("workflowStatus", BPMWorkFlowStatus.INCHARGE_APPROVED.toString());
//		}

		return request;
	}

	protected List<CustomerProfitPaymentResponseDTO> getCustomerProfitDetails(RegistrationRequestDTO request) {
		try {
			ServiceResponse response;
			String apiKey = null;
			switch (CustomerIdentityKey.fromString(request.getCustIdentityKey())) {
			case ACCOUNT_NUMBER:
				apiKey = APIKeyConstants.API_KEY_ACCOUNT_NUM_AND_PRODUCT_CODE;
				break;
			case REGISTRATION_CODE:
				apiKey = APIKeyConstants.API_KEY_REGISTRATION_PRODUCT_CODE;
				break;
			}

			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_PROFIT, apiKey,
					new APICallerDetail().add(request.getCustIdentityValue()).add(request.getProductCode()));

			return new MappingUtility<List<CustomerProfitPaymentResponseDTO>>(false).JsonToDto(response.getData(),
					new TypeToken<List<CustomerProfitPaymentResponseDTO>>() {
					}.getType());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	protected List<TransactionLogResponseDTO> getTransactionsByCustomerCode(RegistrationRequestDTO request)
			throws Exception {

		TransactionRequestDTO transactionRequest = new MappingUtility<TransactionRequestDTO>()
				.JsonToDto(request.getAdditionalInfo(), TransactionRequestDTO.class);

		transactionRequest.setCustomerCode(request.getCustIdentityValue());

		ServiceResponse response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_TRAN_LOG,
				APIKeyConstants.API_KEY_GET_BY_CUST_CODE_DATE_AND_TYPE,
				new APICallerDetail().setObj(transactionRequest));

		if (response.getCode() == ResponseCode.SUCCESS) {
			return new MappingUtility<List<TransactionLogResponseDTO>>(false).JsonToDto(response.getData(),
					new TypeToken<List<TransactionLogResponseDTO>>() {
					}.getType());
		} else
			throw new PaySysException(ResponseCode.ERR_CODE_NO_TRANSACTIONS_FOUND,
					ResponseDesc.ERR_DESC_NO_TRANSACTIONS_FOUND);

	}

	protected PropertyMap mapUserTypeKey() {
		return new PropertyMap<CustomerDTO, RegistrationRequestDTO>() {
			@Override
			protected void configure() {
				map().setUserTypeKey(source.getBranchCode());
			}
		};
	}

	protected List<RestrictionResponseDTO> getCustRestrictionOnRegOrAcc(RegistrationRequestDTO registrationRequestDTO)
			throws Exception {

		ServiceResponse response = apiCaller.execute(RequestType.GET,
				ModuleKeyConstants.MODULE_KEY_CUSTOMER_RESTRICTION, APIKeyConstants.API_KEY_GET_BY_REG_OR_ACC,
				new APICallerDetail().add(registrationRequestDTO.getCustIdentityValue()));

		if (response.getCode() == ResponseCode.SUCCESS) {
			return (List<RestrictionResponseDTO>) new MappingUtility().JsonToDto(response.getData(),
					new TypeToken<List<RestrictionResponseDTO>>() {
					}.getType());
		} else {
			throw new PaySysException(response.getCode(), response.getMessage());
		}
	}
	
protected void validateInLueGovtChequeNumber(RegistrationRequestDTO data) throws PaySysException {
		
		//data validations 
		if (data.getAdditionalInfo().containsKey("inlue")) {
			
			if(data.getAdditionalInfo().get("inlue") != null) {
				
				if(data.getAdditionalInfo().get("inlue").equalsIgnoreCase("1")) {
					if (!data.getAdditionalInfo().containsKey("oldChequeNumber")) {
						throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CHEQUE_NUMBER,ResponseDesc.ERR_DESC_INVALID_CHEQUE_NUMBER);
					} 
					else if (data.getAdditionalInfo().get("oldChequeNumber") == null) {
						throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CHEQUE_NUMBER,ResponseDesc.ERR_DESC_INVALID_CHEQUE_NUMBER);
					} 
					else if (data.getAdditionalInfo().get("oldChequeNumber").isEmpty()) {
						throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CHEQUE_NUMBER,ResponseDesc.ERR_DESC_INVALID_CHEQUE_NUMBER);
					}
					else
					{
						//checking db response if cheque number does not exist then throw exception
						apiCaller = (APICaller) context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
						ServiceResponse response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CHEQUE_PROCESSING,
								APIKeyConstants.API_KEY_GET_BY_CHEQUE_NUM,
								new APICallerDetail().add(data.getAdditionalInfo().get("oldChequeNumber")));
						if(!(boolean)response.getData())
						{
							throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CHEQUE_NUMBER,ResponseDesc.ERR_DESC_INVALID_CHEQUE_NUMBER);
						}
					}
				}
				else {
					//throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CHEQUE_NUMBER,ResponseDesc.ERR_DESC_INVALID_CHEQUE_NUMBER);
				}
					
				
			}
			else
				throw new PaySysException(ResponseCode.ERR_CODE_INVALID_CHEQUE_NUMBER,ResponseDesc.ERR_DESC_INVALID_CHEQUE_NUMBER);
		}		
	}
}
