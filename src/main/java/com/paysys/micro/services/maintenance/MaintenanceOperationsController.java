package com.paysys.micro.services.maintenance;

import com.paysys.entity.common.RegistrationRequestDTO;
import com.paysys.micro.services.maintenance.bl.APIInterface;
import com.paysys.micro.services.maintenance.bl.MaintenanceFactory;
import com.paysys.requests.customer.KYCFillerRequestDTO;
import com.paysys.requests.customer.ProductProfitDetailRequestDTO;
import com.paysys.requests.product.ProductIdentTypeRequestDTO;
import com.paysys.service.common.APIKeyConstants;
import com.paysys.service.common.BaseController;
import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.exception.PaySysException;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import com.paysys.service.response.ServiceResponse;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@Api(tags = "Maintenance Operations")
@Validated
@RestController
@RequestMapping(BaseController.BASE_PATH_MAINTENANCE + ModuleKeyConstants.MODULE_KEY_OPERATION)
public class MaintenanceOperationsController {

    private final static Logger logger = Logger.getLogger(MaintenanceOperationsController.class);

    @Autowired
    private ApplicationContext context;


    @ApiOperation(value = "Get Identifier Filler")
    @PostMapping(value = "/getIdentifierFiller")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getIdentifierFiller(@ApiParam(name = "productIdentTypeRequestDTO", value = "Product Identification Type", required = true) @RequestBody ProductIdentTypeRequestDTO productIdentTypeRequest) {

        logger.info("Function Called: getIdentifierFiller");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_IDENTIFIER_FILLER);
            return serviceResponse.getBykey(productIdentTypeRequest);

        } catch (Exception ex) {
            logger.info(ex);
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Identifier Filler")
    @PostMapping(value = "/getIdentifierRegex")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getIdentifierRegex(@ApiParam(name = "kycFillerRequestDTO", value = "KYC Filler Request DTO", required = true) @RequestBody KYCFillerRequestDTO kycFillerRequestDTO) {

        logger.info("Function Called: getIdentifierRegex");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_REGEX_BY_IDEN_NAME);
            return serviceResponse.getBykey(kycFillerRequestDTO);

        } catch (Exception ex) {
            logger.info(ex);
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }


    @ApiOperation(value = "Get Identifier Filler")
    @GetMapping(value = "/getIdentifierTypeFiller")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getIdentifierTypeFiller() {

        logger.info("Function Called: getIdentifierTypeFiller");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_IDENTIFIER_TYPE_FILLER);
            return serviceResponse.getAll();

        } catch (Exception ex) {
            logger.info(ex);
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Save In BPM")
    @PostMapping(value = "/add")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse add(@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: add");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_SAVE);
            return serviceResponse.add(registrationRequestDTO);

        } catch (PaySysException e) {
            logger.info(e);
            return new ServiceResponse(e.getErrorCode(), e.getMessage(),null);
        } catch (Exception ex) {
            logger.info(ex);
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Update In BPM")
    @PostMapping(value = "/update")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse update(@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: update");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_UPDATE);
            return serviceResponse.update(registrationRequestDTO);

        } catch (PaySysException e) {
            logger.info(e);
            return new ServiceResponse(e.getErrorCode(), e.getMessage());
        } catch (Exception ex) {
            logger.info(ex);
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Product Transfer Details")
    @PostMapping(value = "/getTransferDetails")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getTransferDetails(@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getTransferDetails");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_PROD_TRANSFER_DETAILS);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            logger.info(ex);
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Reinvestment Details")
    @PostMapping(value = "/getReinvestmentDetails")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getReinvestmentDetails(@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getReinvestmentDetails");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_PROD_REINVESTMENT_DETAILS);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            logger.info(ex);
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Lapsed Revival Details")
    @PostMapping(value = "/getLapsedRevivalDetails")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getLapsedRevivalDetails(@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getLapsedRevivalDetails");

        try {
            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_LAPSED_REVIVAL_DETAILS);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            logger.info(ex);
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get All Customer Accounts For Correlation By Product Code")
    @PostMapping(value = "/getCustAccByProdCode")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getCustAccByProdCode(@RequestBody HashMap<String, Object> data) {

        logger.info("Function Called: getCustAccByProdCode");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_BY_CUST_PROD_CODE);
            return serviceResponse.getBykey(data);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }


    @ApiOperation(value = "Get Lapsed Products Details")
    @PostMapping(value = "/getLapsedDetails")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getLapsedDetails(@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true)
                                            @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getLapsedDetails");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_LAPSED_DETAILS);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Nominee Payment Details")
    @PostMapping(value = "/getNomineePaymentDetails")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse getNomineePaymentDetails(@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: getNomineePaymentDetails");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_NOMINEE_PAYMENT_DETAILS);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Run Lapsed Marking Job")
    @PostMapping(value = "/runLapsedMarkingJob")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})

    public ServiceResponse runLapsedMarkingJob(@RequestBody RegistrationRequestDTO registrationRequestDTO) {

        logger.info("Function Called: runLapsedMarkingJob");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_RUN_LAPSED_MARKING_JOB);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Maturity Date And Profit Count")
    @PostMapping(value = "/getMaturityAndProfitCount")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})
    public ServiceResponse getMaturityAndProfitCount(@ApiParam(name = "profitDetailResponeDTO", value = "Profit Detail Response DTO", required = true) @RequestBody ProductProfitDetailRequestDTO profitDetailResponseDTO) {
        logger.info("Function Called: getMaturityAndProfitCount");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_KEY_GET_MATURITY_AND_PROFIT_COUNT);
            return serviceResponse.getBykey(profitDetailResponseDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    @ApiOperation(value = "Get Maturity Date And Profit Count")
    @PostMapping(value = "/validateTransferInRequest")
    @ApiResponses(value = {@ApiResponse(code = ResponseCode.SUCCESS, message = ResponseDesc.SUCCESS),
            @ApiResponse(code = ResponseCode.FAILURE, message = ResponseDesc.FAILURE),
            @ApiResponse(code = ResponseCode.NOT_ACTIVE, message = ResponseDesc.NOT_ACTIVE),
            @ApiResponse(code = ResponseCode.DATA_NOT_FOUND, message = ResponseDesc.DATA_NOT_FOUND)})
    public ServiceResponse validateTransferInRequest(@ApiParam(name = "registrationRequestDTO", value = "Registration Request DTO", required = true) @RequestBody RegistrationRequestDTO registrationRequestDTO) {
        logger.info("Function Called: validateTransferInRequest");

        try {

            APIInterface serviceResponse = ((MaintenanceFactory) context.getBean(MaintenanceFactory.class,
                    ModuleKeyConstants.MODULE_KEY_MAINTENANCE_OPERATIONS))
                    .processTransaction(APIKeyConstants.API_VALIDATE_TRANSFER_IN_REQUEST);
            return serviceResponse.getBykey(registrationRequestDTO);

        } catch (Exception ex) {
            return new ServiceResponse(ResponseCode.FAILURE, ex);
        }
    }

    public String fallback(Throwable hystrixCommand) {
        logger.info("Function Called: ");
        logger.info("Service is Down.");
        return "Service is Down.";
    }

}
