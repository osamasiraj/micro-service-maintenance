package com.paysys.micro.services.maintenance.bl;

import com.paysys.entity.common.CustomerDTO;
import com.paysys.entity.common.RegistrationRequestDTO;
import com.paysys.entity.common.RegistrationRequestDetailDTO;
import com.paysys.micro.services.maintenance.bl.common.GeneralBL;
import com.paysys.requests.customer.CustomerAssociateDTO;
import com.paysys.requests.inventory.CertificateInventoryDTO;
import com.paysys.responses.customer.CustomerCertificateResponseDTO;
import com.paysys.responses.customer.CustomerDocumentResponseDTO;
import com.paysys.responses.customer.CustomerResponseDTO;
import com.paysys.responses.inventory.InventoryResponseDTO;
import com.paysys.responses.maintenance.BasicInfoResponseDTO;
import com.paysys.responses.product.AccountResponseDTO;
import com.paysys.responses.product.ProductMaintenanceResponseDTO;
import com.paysys.service.common.APIKeyConstants;
import com.paysys.service.common.Constants;
import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.common.enm.*;
import com.paysys.service.exception.PaySysException;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import com.paysys.service.response.ServiceResponse;
import com.paysys.service.utility.APICallerDetail;
import com.paysys.service.utility.MappingUtility;
import org.modelmapper.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductMaintenanceBL extends GeneralBL implements APIInterface {

	private final String apiKey;
	private final String moduleId;

	public ProductMaintenanceBL(String moduleId, String apiKey) {
		this.moduleId = moduleId;
		this.apiKey = apiKey;
	}

	@Override
	public ServiceResponse getAll() throws PaySysException, Exception {
		return new ServiceResponse(ResponseCode.ERR_CODE_NO_IMPLEMENTATION);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponse getBykey(Object key) throws PaySysException, Exception {
		RegistrationRequestDTO request = null;
		BasicInfoResponseDTO basicInfoResponse = null;

		if (key instanceof RegistrationRequestDTO) {
			request = (RegistrationRequestDTO) key;
		}

		ServiceResponse response = null;

		switch (apiKey) {

		case APIKeyConstants.API_KEY_GET_BY_IDENTIFIER:

			CustomerDTO customerProduct;

			if (this.getInnerRequestType(request).equalsIgnoreCase(BPMProcess.lapsedRevival.toString()))
				customerProduct = this.getLapsedCustomerProduct(request);
			else if (this.getInnerRequestType(request).equalsIgnoreCase(BPMProcess.reInvestment.toString()))
				customerProduct = this.getReInvestmentCustomerProduct(request);
			else
				customerProduct = this.getCustomerProduct(request);

			response = new ServiceResponse(ResponseCode.SUCCESS, customerProduct);

			break;

		case APIKeyConstants.API_KEY_GET_PRODUCT_NOMINEES:

			List<CustomerAssociateDTO> nominees = this.getProductNominees(request);

			request = updateRegistrationRequest(request, nominees, BPMTransactionKey.CUSTOMER_ASSOCIATE, Constants.ADD);

			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			break;

		case APIKeyConstants.API_KEY_GET_PRODUCT_ATTACHMENTS:

			basicInfoResponse = this.getCustBasicInfoByProduct(request);
			List<CustomerDocumentResponseDTO> requiredDocs = new ArrayList<>();

			// Get required documents for all customers
			for (CustomerDTO customer : basicInfoResponse.getCustomers()) {

				List<CustomerDocumentResponseDTO> temp = getCustomerDocumentsForAttachments(
						customer.getInvestorTypeCode(), basicInfoResponse.getProductCode(), customer.getCustomerCode(),
						customer.getCustIdentityValue());

				requiredDocs.addAll(temp);
			}

			// Get existing documents
			List<CustomerDocumentResponseDTO> custDocs = this.getCustomerDocumentsByIdentifier(request, null);

			// Map existing documents with required documents
			requiredDocs = this.mapCustomerDocs(custDocs, requiredDocs);

			// Remove global documents (CNIC etc)
			requiredDocs.removeIf(doc -> doc.getDocumentType().getType().equalsIgnoreCase(Constants.GLOBAL)
					|| doc.getDocumentType().getCode().equalsIgnoreCase("0017"));

			request = this.updateRegistrationRequest(request, requiredDocs,
					BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT, Constants.ADD);

			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			break;

		case APIKeyConstants.API_KEY_GET_SIG_CARD_BY_IDENTIFIER:

			custDocs = this.getCustomerDocumentsByIdentifier(request, Constants.DOC_SIG_CARD);

			if (custDocs == null) {

				basicInfoResponse = this.getCustBasicInfoByProduct(request);

				custDocs = getCustomerDocumentsForAttachments(basicInfoResponse.getInvestorTypeCode(),
						basicInfoResponse.getProductCode(), basicInfoResponse.getCustomers().get(0).getCustomerCode(),
						request.getCustIdentityValue());

				custDocs.removeIf(doc -> !doc.getDocumentType().getCode().equalsIgnoreCase(Constants.DOC_SIG_CARD));

			}

			request = this.updateRegistrationRequest(request, custDocs, BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT,
					Constants.ADD);

			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			break;

		case APIKeyConstants.API_KEY_GET_PRODUCT_DETAILS:

			request = this.getProductDetails(request);

			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			break;
		case APIKeyConstants.API_KEY_GET_CERT_FOR_REISSUANCE:

			CertificateInventoryDTO certificateInventoryRequestDTO = (CertificateInventoryDTO) this.parse(request,
					BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY);

			List<CustomerCertificateResponseDTO> certificates = this.getCertificatesByRegCodeAndBranch(request);

			response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_INVENTORY,
					APIKeyConstants.API_KEY_GET_BY_DENOM_FOR_REISSUANCE,
					new APICallerDetail().setObj(certificateInventoryRequestDTO));

			if (response.getCode() == ResponseCode.SUCCESS) {
				List<CustomerCertificateResponseDTO> newCertificates = (List<CustomerCertificateResponseDTO>) new MappingUtility<>(
						false).JsonToDto(response.getData(), new TypeToken<List<CustomerCertificateResponseDTO>>() {
						}.getType());

				for (CustomerCertificateResponseDTO cert : newCertificates) {
					cert.setTitle(certificates.get(0).getTitle());
				}

				request = updateRegistrationRequest(request, newCertificates,
						BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY, Constants.UPDATE);

				response = new ServiceResponse(ResponseCode.SUCCESS, request);
			} else
				throw new PaySysException(ResponseCode.ERR_CODE_INVENTORY_OUT_OF_STOCK,
						ResponseDesc.ERR_DESC_INVENTORY_OUT_OF_STOCK);

			break;
		case APIKeyConstants.API_KEY_GET_BY_CUST_PROD_CODE:

			response = apiCaller.execute(RequestType.POST, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT, apiKey,
					new APICallerDetail().setObj(key));

			if (response.getCode() != ResponseCode.SUCCESS)
				throw new PaySysException(ResponseCode.ERR_CODE_ACCOUNT_NOT_FOUND,
						ResponseDesc.ERR_DESC_ACCOUNT_NOT_FOUND);

			break;
		case APIKeyConstants.API_KEY_GET_ALLOWED_STATUS_FILLER:

			APICallerDetail apiCallerDetail = ((APICallerDetail) key);
			String moduleKey = null;

			if (apiCallerDetail.getParamDetail().get(1).equalsIgnoreCase("WSB_MAINT")
					|| apiCallerDetail.getParamDetail().get(1).equalsIgnoreCase("PCB_MAINT")) {
				moduleKey = ModuleKeyConstants.MODULE_KEY_INVENTORY_STATUS;
			} else
				moduleKey = ModuleKeyConstants.MODULE_KEY_ACCOUNT_STATUS;

			response = apiCaller.execute(RequestType.GET, moduleKey, apiKey, apiCallerDetail);

			break;
		case APIKeyConstants.API_KEY_GET_INVENTORY_BY_IDENTIFIER:

			List<ProductMaintenanceResponseDTO> products = new ArrayList<>();
			CustomerResponseDTO cust = null;
			AccountResponseDTO account = null;
			List<CustomerCertificateResponseDTO> cert = null;

			switch (CustomerIdentityKey.fromString(request.getCustIdentityKey())) {

			case ACCOUNT_NUMBER:
				cust = this.getCustomerByProduct(request).get(0);

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVENTORY,
						APIKeyConstants.API_GET_WSB_BY_ACCOUNT_NUMBER,
						new APICallerDetail().add(request.getCustIdentityValue()));

				account = this.getAccountByNumber(request.getUserTypeKey(), request.getCustIdentityValue());

				if (response.getCode() != ResponseCode.SUCCESS)
					throw new PaySysException(ResponseCode.ERR_CODE_WSB_MISSING_OR_INVALID,
							ResponseDesc.ERR_DESC_WSB_MISSING_OR_INVALID);

				break;

			case REGISTRATION_CODE:
				cust = this.getCustomerByProduct(request).get(0);

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVENTORY,
						APIKeyConstants.API_GET_PCB_BY_REGISTRATION_CODE,
						new APICallerDetail().add(request.getCustIdentityValue()));

				cert = this.getCertificatesByRegCodeAndBranch(request);

				if (response.getCode() != ResponseCode.SUCCESS)
					throw new PaySysException(ResponseCode.ERR_CODE_PCB_MISSING_OR_INVALID,
							ResponseDesc.ERR_DESC_PCB_MISSING_OR_INVALID);

				break;

			}

			List<InventoryResponseDTO> inventoryReponse = (List<InventoryResponseDTO>) new MappingUtility<>()
					.JsonToDto(response.getData(), new TypeToken<List<InventoryResponseDTO>>() {
					}.getType());

			List<CustomerDTO> customerBasicInfo = this.getCustomerWithBasicInfo(Collections.singletonList(cust));

			for (InventoryResponseDTO inv : inventoryReponse) {

				ProductMaintenanceResponseDTO productMaintenance = new ProductMaintenanceResponseDTO();
				if (getInnerRequestType(request).equalsIgnoreCase(BPMProcess.changeWSBStatus.toString())) {
					productMaintenance.setAccountNumber(account.getAccountNumber());
					productMaintenance.setAccountTitle(account.getAccountTitle());
					productMaintenance.setCode(account.getProductCode());
					productMaintenance.setCategoryCode(account.getCustomerAccountRels().get(0).getProduct()
							.getProductCategory().getCategoryCode());

				} else if (getInnerRequestType(request).equalsIgnoreCase(BPMProcess.changePCBStatus.toString())) {
					productMaintenance.setRegistrationCode(cert.get(0).getRegistrationCode());
					productMaintenance.setAccountTitle(cert.get(0).getTitle());
					productMaintenance.setCode(cert.get(0).getProductCode());
					productMaintenance.setCategoryCode(cert.get(0).getCustomerAccountRels().get(0).getProduct()
							.getProductCategory().getCategoryCode());

				}

				inv.setUnusedLeaves(inv.getInventoryDetails().stream()
						.filter(p -> p.getInventoryStatus().getCode().equals(InventoryStatuses.ISSUED.toString()))
						.count());

				inv.setInventoryDetails(null);
				productMaintenance.setInventory(inv);
				products.add(productMaintenance);

			}

			customerBasicInfo.get(0).setProducts(products);

			response = new ServiceResponse(ResponseCode.SUCCESS, customerBasicInfo.get(0));

			break;

		case APIKeyConstants.API_KEY_GET_INVENTORY_DETAILS:

			String inventoryId;
			BPMTransactionKey transactionKey;

			if (getInnerRequestType(request).equalsIgnoreCase(BPMProcess.changeWSBStatus.toString())) {
				inventoryId = request.getAdditionalInfo().get("WSBInventoryId");
				transactionKey = BPMTransactionKey.CUSTOMER_WSB_INVENTORY;
			} else if (getInnerRequestType(request).equalsIgnoreCase(BPMProcess.changePCBStatus.toString())) {
				inventoryId = request.getAdditionalInfo().get("PCBInventoryId");
				transactionKey = BPMTransactionKey.CUSTOMER_PCB_INVENTORY;
			} else
				throw new PaySysException(ResponseCode.ERR_CODE_INVALID_REQEUST_TYPE,
						ResponseDesc.ERR_DESC_INVALID_REQUEST_TYPE);

			response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVENTORY,
					APIKeyConstants.API_KEY_GET_BY_ID, new APICallerDetail().add(inventoryId));

			InventoryResponseDTO wSlipBooks = (InventoryResponseDTO) new MappingUtility<>(false)
					.JsonToDto(response.getData(), InventoryResponseDTO.class);

			request = addRegistrationRequestDetails(request, wSlipBooks, transactionKey);

			response = new ServiceResponse(ResponseCode.SUCCESS, request);

			break;

		}

		// TEMPORARY FIX
		if (!apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_GET_BY_IDENTIFIER)
				&& !apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_GET_INVENTORY_BY_IDENTIFIER)
				&& !apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_GET_BY_CUST_PROD_CODE)
				&& !apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_GET_ALLOWED_STATUS_FILLER)) {
			if (basicInfoResponse == null)
				basicInfoResponse = this.getCustBasicInfoByProduct(request);
			RegistrationRequestDetailDTO basicDetails = new RegistrationRequestDetailDTO();
			basicDetails.setTransactionKey(BPMTransactionKey.BASIC_INFO.toString());
			basicDetails.setContent(basicInfoResponse);
			request.getRegistrationRequestDetailDTO().add(basicDetails);
		}

		return response;
	}

	@Override
	public ServiceResponse add(Object genObj) throws PaySysException, Exception {
		return new ServiceResponse(ResponseCode.ERR_CODE_NO_IMPLEMENTATION);
	}

	@Override
	public ServiceResponse update(Object genObj) throws PaySysException, Exception {
		return new ServiceResponse(ResponseCode.ERR_CODE_NO_IMPLEMENTATION);
	}

	@Override
	public ServiceResponse inquiry(Object genObj) throws PaySysException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}