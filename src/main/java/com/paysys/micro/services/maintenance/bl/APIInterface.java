package com.paysys.micro.services.maintenance.bl;

import com.paysys.service.exception.PaySysException;
import com.paysys.service.response.ServiceResponse;


public interface APIInterface {

	ServiceResponse getAll() throws PaySysException, Exception;
	ServiceResponse getBykey(Object key) throws PaySysException, Exception;
	ServiceResponse add(Object genObj) throws PaySysException, Exception;
	ServiceResponse update(Object genObj) throws PaySysException, Exception;
	ServiceResponse inquiry(Object genObj) throws PaySysException, Exception;
}
