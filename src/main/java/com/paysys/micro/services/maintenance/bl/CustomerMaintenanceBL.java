package com.paysys.micro.services.maintenance.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;

import com.paysys.entity.common.CustomerDTO;
import com.paysys.entity.common.RegistrationRequestDTO;
import com.paysys.entity.common.RegistrationRequestDetailDTO;
import com.paysys.micro.services.maintenance.bl.common.GeneralBL;
import com.paysys.requests.customer.CustomerAssociateDTO;
import com.paysys.responses.customer.CustomerAccountRelResponseDTO;
import com.paysys.responses.customer.CustomerAccountUnitResponseDTO;
import com.paysys.responses.customer.CustomerCertificateResponseDTO;
import com.paysys.responses.customer.CustomerDocumentResponseDTO;
import com.paysys.responses.customer.CustomerResponseDTO;
import com.paysys.responses.customer.CustomerRestrictionResponseDTO;
import com.paysys.responses.inventory.InventoryResponseDTO;
import com.paysys.responses.maintenance.BasicInfoResponseDTO;
import com.paysys.responses.product.AccountResponseDTO;
import com.paysys.responses.product.KycAttributeResponseDTO;
import com.paysys.responses.product.ProductMaintenanceResponseDTO;
import com.paysys.responses.product.ProductResponseDTO;
import com.paysys.responses.product.ProductRestrictionResponseDTO;
import com.paysys.responses.profit.CustomerProfitPaymentResponseDTO;
import com.paysys.responses.transaction.TransactionLogResponseDTO;
import com.paysys.service.common.APIKeyConstants;
import com.paysys.service.common.Constants;
import com.paysys.service.common.ModuleKeyConstants;
import com.paysys.service.common.enm.BPMProcess;
import com.paysys.service.common.enm.BPMTransactionKey;
import com.paysys.service.common.enm.CustomerIdentityKey;
import com.paysys.service.common.enm.KYCEnum;
import com.paysys.service.common.enm.ProductCategory;
import com.paysys.service.common.enm.RequestType;
import com.paysys.service.common.enm.ServiceNames;
import com.paysys.service.exception.PaySysException;
import com.paysys.service.response.ResponseCode;
import com.paysys.service.response.ResponseDesc;
import com.paysys.service.response.ServiceResponse;
import com.paysys.service.utility.APICaller;
import com.paysys.service.utility.APICallerDetail;
import com.paysys.service.utility.MappingUtility;

public class CustomerMaintenanceBL extends GeneralBL implements APIInterface {

	private final static Logger logger = Logger.getLogger(CustomerMaintenanceBL.class);
	private final String apiKey;
	private final String moduleId;

	public CustomerMaintenanceBL(String moduleId, String apiKey) {
		this.moduleId = moduleId;
		this.apiKey = apiKey;
	}

	@Override
	public ServiceResponse inquiry(Object key) throws PaySysException, Exception {

		ServiceResponse response = null;
		RegistrationRequestDTO request = (RegistrationRequestDTO) key;

		if (request.getUserType().equalsIgnoreCase("3")) {
			request.setUserTypeKey(request.getCustIdentityValue().substring(0, 4));
		}

		if ((apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_SUMMARY))) {

			if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.REGISTRATION_CODE.toString())) {
				apiCaller = context.getBean(APICaller.class, ServiceNames.DB_SERVICE);
				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
						APIKeyConstants.API_KEY_GET_CERTIFICATE_SUMMARY,
						new APICallerDetail().add(request.getCustIdentityValue()).add(request.getProductCode())
								.add(request.getUserTypeKey()));
			} else if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.ACCOUNT_NUMBER.toString())) {
			}
		}

		if (apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_INQUIRE)) {

			if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.REGISTRATION_CODE.toString())) {

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY) != null) {

					List<CustomerCertificateResponseDTO> customerCertificates = this
							.getCertificatesByRegCodeProdCodeBranchCode(request);

					request = this.addRegistrationRequestDetails(request, customerCertificates,
							BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY);

					response = new ServiceResponse(ResponseCode.SUCCESS, request);
				}

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.PROFIT_DETAILS) != null) {

					List<CustomerProfitPaymentResponseDTO> profitDetails;

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_PROFIT,
							APIKeyConstants.API_KEY_REGISTRATION_PRODUCT_CODE,
							new APICallerDetail().add(request.getCustIdentityValue()).add(request.getProductCode()));

					if (response.getCode() == ResponseCode.SUCCESS) {

						profitDetails = new MappingUtility<List<CustomerProfitPaymentResponseDTO>>(false)
								.JsonToDto(response.getData(), new TypeToken<List<CustomerProfitPaymentResponseDTO>>() {
								}.getType());

						request = this.addRegistrationRequestDetails(request, profitDetails,
								BPMTransactionKey.PROFIT_DETAILS);
						response = new ServiceResponse(ResponseCode.SUCCESS, request);

					} else {

						throw new PaySysException(ResponseCode.ERR_DESC_PROFIT_NOT_DUE_PAID,
								ResponseDesc.ERR_DESC_PROFIT_NOT_DUE_PAID);

					}

				}

				else if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSACTION_DETAILS) != null) {

					List<TransactionLogResponseDTO> transactionDetails;

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_TRAN_LOG,
							APIKeyConstants.API_KEY_GET_BY_REG_CODE,
							new APICallerDetail().add(request.getCustIdentityValue())
									.add(request.getAdditionalInfo().get("months")));

					if (response.getCode() == ResponseCode.SUCCESS) {

						transactionDetails = new MappingUtility<List<TransactionLogResponseDTO>>(false)
								.JsonToDto(response.getData(), new TypeToken<List<TransactionLogResponseDTO>>() {
								}.getType());

						request = this.addRegistrationRequestDetails(request, transactionDetails,
								BPMTransactionKey.TRANSACTION_DETAILS);
						response = new ServiceResponse(ResponseCode.SUCCESS, request);

					} else {

						throw new PaySysException(ResponseCode.ERR_CODE_NO_TRANSACTIONS_FOUND,
								ResponseDesc.ERR_DESC_NO_TRANSACTIONS_FOUND);
					}

				}

			}

			else if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.ACCOUNT_NUMBER.toString())) {

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_ACCOUNT_UNIT_DETAILS) != null) {

					List<CustomerAccountUnitResponseDTO> customerAccountUnits;

					apiCaller = context.getBean(APICaller.class, ServiceNames.DB_SERVICE);

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT_UNIT,
							APIKeyConstants.API_KEY_GET_BY_ACCOUNT_NUMBER_PROD_CODE_BRANCH_CODE,
							new APICallerDetail().add(request.getCustIdentityValue()).add(request.getProductCode())
									.add(request.getUserTypeKey()));

					if (response.getCode() == ResponseCode.SUCCESS) {

						customerAccountUnits = new MappingUtility<List<CustomerAccountUnitResponseDTO>>(false)
								.JsonToDto(response.getData(), new TypeToken<List<CustomerAccountUnitResponseDTO>>() {
								}.getType());

						request = this.addRegistrationRequestDetails(request, customerAccountUnits,
								BPMTransactionKey.CUSTOMER_ACCOUNT_UNIT_DETAILS);

						response = new ServiceResponse(ResponseCode.SUCCESS, request);

					} else {
						throw new PaySysException(ResponseCode.ERR_CODE_ACCOUNT_NOT_FOUND,
								ResponseDesc.ERR_DESC_ACCOUNT_NOT_FOUND);
					}

				}

			}

			else if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.UNIT_NUMBER.toString())) {

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.PROFIT_DETAILS) != null) {

					List<CustomerProfitPaymentResponseDTO> profitDetails;

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_PROFIT,
							APIKeyConstants.API_KEY_UNIT_ID_PRODUCT_CODE,
							new APICallerDetail().add(request.getCustIdentityValue()).add(request.getProductCode()));

					if (response.getCode() == ResponseCode.SUCCESS) {

						profitDetails = new MappingUtility<List<CustomerProfitPaymentResponseDTO>>(false)
								.JsonToDto(response.getData(), new TypeToken<List<CustomerProfitPaymentResponseDTO>>() {
								}.getType());

						request = this.addRegistrationRequestDetails(request, profitDetails,
								BPMTransactionKey.PROFIT_DETAILS);
						response = new ServiceResponse(ResponseCode.SUCCESS, request);
					} else {

						throw new PaySysException(ResponseCode.ERR_DESC_PROFIT_NOT_DUE_PAID,
								ResponseDesc.ERR_DESC_PROFIT_NOT_DUE_PAID);
					}

				}

				else if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSACTION_DETAILS) != null) {

					List<TransactionLogResponseDTO> transactionDetails;

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_TRAN_LOG,
							APIKeyConstants.API_KEY_GET_BY_UNIT_ID,
							new APICallerDetail().add(request.getCustIdentityValue())
									.add(request.getAdditionalInfo().get("months")));

					if (response.getCode() == ResponseCode.SUCCESS) {

						transactionDetails = new MappingUtility<List<TransactionLogResponseDTO>>(false)
								.JsonToDto(response.getData(), new TypeToken<List<TransactionLogResponseDTO>>() {
								}.getType());

						request = this.addRegistrationRequestDetails(request, transactionDetails,
								BPMTransactionKey.TRANSACTION_DETAILS);
						response = new ServiceResponse(ResponseCode.SUCCESS, request);

					} else {

						throw new PaySysException(ResponseCode.ERR_CODE_NO_TRANSACTIONS_FOUND,
								ResponseDesc.ERR_DESC_NO_TRANSACTIONS_FOUND);
					}
				}

			}

			else if (request.getCustIdentityKey()
					.equalsIgnoreCase(CustomerIdentityKey.INVENTORY_BY_LEAF_NO.toString())) {

				ServiceResponse tempSvcRsp = null;

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_CERTIFICATE,
						APIKeyConstants.API_KEY_GET_BY_LEAVE_NUMBER,
						new APICallerDetail().add(request.getCustIdentityValue()));

				if (response.getCode() == ResponseCode.SUCCESS) {
					List<CustomerCertificateResponseDTO> customerCertificateResponseDTO = new MappingUtility<List<CustomerCertificateResponseDTO>>(
							false).JsonToDto(response.getData(), new TypeToken<List<CustomerCertificateResponseDTO>>() {
							}.getType());

					request = this.addRegistrationRequestDetails(request, customerCertificateResponseDTO,
							BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY);
					response = new ServiceResponse(ResponseCode.SUCCESS, request);
					tempSvcRsp = response;
				}

				response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_CUSTOMER_ACCOUNT,
						APIKeyConstants.API_KEY_GET_BY_LEAVE_NUMBER,
						new APICallerDetail().add(request.getCustIdentityValue()));

				if (response.getCode() == ResponseCode.SUCCESS) {
					List<AccountResponseDTO> accountResponseDTO = new MappingUtility<List<AccountResponseDTO>>(false)
							.JsonToDto(response.getData(), new TypeToken<List<AccountResponseDTO>>() {
							}.getType());
					request = this.addRegistrationRequestDetails(request, accountResponseDTO,
							BPMTransactionKey.CUSTOMER_ACCOUNT_UNIT_DETAILS);
					response = new ServiceResponse(ResponseCode.SUCCESS, request);
					tempSvcRsp = response;
				}

				if (tempSvcRsp != null)
					response = new ServiceResponse(ResponseCode.SUCCESS, request);
			}

			else if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.INVENTORY.toString())) {

				if (this.getRequestDetailsByKey(request, BPMTransactionKey.INVENTORY_INQUIRY) != null) {

					List<InventoryResponseDTO> inventories;
					String leaveNumber;
					if (request.getAdditionalInfo().get("leaveNumber").equalsIgnoreCase("")) {
						leaveNumber = null;
					} else
						leaveNumber = request.getAdditionalInfo().get("leaveNumber");

					response = apiCaller.execute(RequestType.GET, ModuleKeyConstants.MODULE_KEY_INVENTORY,
							APIKeyConstants.API_GET_BY_LEAF_NUMBER_INV_TYPEB_RANCH_CODE_PROD_CODE_REG_CODE_OR_ACC_NUM,
							new APICallerDetail().add(leaveNumber).add(request.getAdditionalInfo().get("inventoryType"))
									.add(request.getUserTypeKey()).add(request.getProductCode())
									.add(request.getCustIdentityValue()));

					if (response.getCode() == ResponseCode.SUCCESS) {

						inventories = new MappingUtility<List<InventoryResponseDTO>>(false)
								.JsonToDto(response.getData(), new TypeToken<List<InventoryResponseDTO>>() {
								}.getType());

						request = this.addRegistrationRequestDetails(request, inventories,
								BPMTransactionKey.INVENTORY_INQUIRY);
						response = new ServiceResponse(ResponseCode.SUCCESS, request);

					} else {

						throw new PaySysException(ResponseCode.ERR_CODE_NO_INVENTORIES_FOUND,
								ResponseDesc.ERR_DESC_NO_INVENTORIES_FOUND);
					}

				}
			}

		}

		return response;

	}

	@Override
	public ServiceResponse getAll() throws PaySysException, Exception {
		return null;
	}

	@Override
	public ServiceResponse getBykey(Object key) throws PaySysException, Exception {
		ServiceResponse response = null;
		RegistrationRequestDTO request = (RegistrationRequestDTO) key;
		List<ProductResponseDTO> customerProducts;
		BasicInfoResponseDTO basicInfoResponse = null;

		if (!apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_GET_BY_IDENTIFIER))
			basicInfoResponse = this.getCustBasicInfo(request, CustomerIdentityKey.CUSTOMER_CODE);

		if (apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_GET_BY_IDENTIFIER)) {

			List<CustomerDTO> customerBasicInfo;

			if (request.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.ACCOUNT_NUMBER.toString()) || request
					.getCustIdentityKey().equalsIgnoreCase(CustomerIdentityKey.REGISTRATION_CODE.toString())) {

				List<CustomerResponseDTO> customers = this.getCustomerByIdentificationKey(request,
						CustomerIdentityKey.fromString(request.getCustIdentityKey()));

				customerBasicInfo = this.getCustomerWithBasicInfo(customers);

			} else {

				CustomerResponseDTO customer = this.getCustomerByIdentifier(request,
						CustomerIdentityKey.fromString(request.getCustIdentityKey()));

				customerBasicInfo = this.getCustomerWithBasicInfo(Collections.singletonList(customer));

			}

			response = new ServiceResponse(ResponseCode.SUCCESS, customerBasicInfo);

		} else if (apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_GET_CUSTOMER_DETAILS)) {

			if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_KYC) != null) {

				CustomerResponseDTO customer = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);

				List<KycAttributeResponseDTO> finalKycSet;
				List<String> kycAttrList = this.getReqeustedKycList(request);

				if (kycAttrList != null && kycAttrList.size() > 0)
					finalKycSet = this.getCustomerKycByList(customer.getCode(), kycAttrList);
				else
					finalKycSet = this.getProducAndCustomerKyc(customer);

				finalKycSet.forEach(kyc -> {
					if (kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_CUST_IDENT_CODE.toString())
							|| kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_CUST_IDENT_VALUE.toString())
							|| kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_DATE_OF_BIRTH.toString())
							|| kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_PARANTAGE_VALUE.toString())
							|| kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_PARANTAGE_CODE.toString())
							|| kyc.getAttributeCode().equalsIgnoreCase(KYCEnum.KYC_MOTHER_MAIDEN_NAME.toString()))
						kyc.setIsEditable(Constants.NO);
					else
						kyc.setIsEditable(Constants.YES);

				});

				request = this.addRegistrationRequestDetails(request, finalKycSet, BPMTransactionKey.CUSTOMER_KYC);

				response = new ServiceResponse(ResponseCode.SUCCESS, request);

			}
			if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT) != null) {

				CustomerResponseDTO customer = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);

				if (getInnerRequestType(request).equalsIgnoreCase(BPMProcess.CRM.toString())) {

					List<CustomerDocumentResponseDTO> custRes = this.getCustomersExistingDocs(customer.getCode());

					request = this.addRegistrationRequestDetails(request, custRes,
							BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT);

				} else {

					// Get customer attachable documents
					List<CustomerDocumentResponseDTO> customerDocs = this.getDocumentByCustomerCode(customer.getCode());

					request = this.addRegistrationRequestDetails(request, customerDocs,
							BPMTransactionKey.CUSTOMER_DOCUMENTS_ATTACHMENT);

				}
				response = new ServiceResponse(ResponseCode.SUCCESS, request);
			}

			if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_RESTRICTION) != null) {

				CustomerResponseDTO customer = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);

				if (getInnerRequestType(request).equalsIgnoreCase(BPMProcess.CRM.toString())) {

					List<CustomerRestrictionResponseDTO> custRes = this
							.getRestrictionByCustomerCode(customer.getCode());

					request = this.addRegistrationRequestDetails(request, custRes,
							BPMTransactionKey.CUSTOMER_RESTRICTION);

				} else {

					customerProducts = this.getAllCustomerProducts(customer.getCode());

					List<ProductRestrictionResponseDTO> prodRes = this.getProductRestrictions(customerProducts);

					List<CustomerRestrictionResponseDTO> custRes = this
							.getRestrictionByCustomerCode(customer.getCode());

					List<CustomerRestrictionResponseDTO> finalRes = this.mapRestrictions(prodRes, custRes);

					// Remove all except zakat restriction
					finalRes.removeIf(
							res -> !res.getRestriction().getCode().equalsIgnoreCase(Constants.ZAKAT_RESTRICTION));

					request = this.addRegistrationRequestDetails(request, finalRes,
							BPMTransactionKey.CUSTOMER_RESTRICTION);
				}

				response = new ServiceResponse(ResponseCode.SUCCESS, request);

			}
			if (this.getRequestDetailsByKey(request, BPMTransactionKey.ACCOUNT_INFO) != null) {

				List<ProductMaintenanceResponseDTO> accounts = null;

				CustomerResponseDTO customer = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);

				if (request.getCategory() == null
						|| request.getCategory().equalsIgnoreCase(ProductCategory.ACCOUNT.toString())) {

					List<AccountResponseDTO> customerAccounts = getAccountsByCustCodeAndBranch(customer.getCode(),
							null);

					accounts = this.mapAccountsToProdMaintenace(customerAccounts, request);

				}

				request = this.addRegistrationRequestDetails(request, accounts, BPMTransactionKey.ACCOUNT_INFO);

				response = new ServiceResponse(ResponseCode.SUCCESS, request);

			}
			if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY) != null) {

				List<ProductMaintenanceResponseDTO> certificates = new ArrayList<>();

				CustomerResponseDTO customer = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);

				List<CustomerCertificateResponseDTO> customerCertificates = getCertificatesByCustCodeAndBranch(request);

				// certificates = this.mapCertsToProdMaintenance(customerCertificates);

				request = this.addRegistrationRequestDetails(request, customerCertificates,
						BPMTransactionKey.CUSTOMER_CERTIFICATE_INVENTORY);

				response = new ServiceResponse(ResponseCode.SUCCESS, request);

			}
			if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_ACCOUNT) != null) {

				List<ProductMaintenanceResponseDTO> corrAccounts = new ArrayList<>();

				CustomerResponseDTO customer = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);

				if (request.getCategory() == null
						|| request.getCategory().equalsIgnoreCase(ProductCategory.ACCOUNT.toString())) {

					List<AccountResponseDTO> customerAccounts = getCorrelatedAccountsByCustCode(customer.getCode());
					corrAccounts = this.mapAccountsToProdMaintenace(customerAccounts, request);

				}

				request = this.addRegistrationRequestDetails(request, corrAccounts, BPMTransactionKey.CUSTOMER_ACCOUNT);

				response = new ServiceResponse(ResponseCode.SUCCESS, request);

			}

			if (this.getRequestDetailsByKey(request, BPMTransactionKey.CUSTOMER_ASSOCIATE) != null) {

				CustomerResponseDTO customer = this.getCustomerByIdentifier(request, CustomerIdentityKey.CUSTOMER_CODE);

				List<CustomerAssociateDTO> associates = this.getCustomerAssociates(customer.getCode());

				request = this.addRegistrationRequestDetails(request, associates, BPMTransactionKey.CUSTOMER_ASSOCIATE);

				response = new ServiceResponse(ResponseCode.SUCCESS, request);

			}
			if (this.getRequestDetailsByKey(request, BPMTransactionKey.TRANSACTION_DETAILS) != null) {

				List<TransactionLogResponseDTO> transactions = this.getTransactionsByCustomerCode(request);

				// Return empty data set
				if (transactions == null)
					transactions = new ArrayList<>();

				request = this.addRegistrationRequestDetails(request, transactions,
						BPMTransactionKey.TRANSACTION_DETAILS);

				response = new ServiceResponse(ResponseCode.SUCCESS, request);

			}

		}

		// TEMPORARY FIX
		if (!apiKey.equalsIgnoreCase(APIKeyConstants.API_KEY_GET_BY_IDENTIFIER)
				&& (request.getRequestType() == null || !request.getRequestType().equalsIgnoreCase("crm"))) {
			RegistrationRequestDetailDTO basicDetails = new RegistrationRequestDetailDTO();
			basicDetails.setTransactionKey(BPMTransactionKey.BASIC_INFO.toString());
			basicDetails.setContent(basicInfoResponse);
			request.getRegistrationRequestDetailDTO().add(basicDetails);
		}

		return response;
	}

	@Override
	public ServiceResponse add(Object genObj) throws PaySysException, Exception {
		return null;
	}

	@Override
	public ServiceResponse update(Object genObj) throws PaySysException, Exception {
		return null;
	}

}